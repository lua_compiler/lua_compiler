define testrun =
	succeed=0 ; \
	failed=0 ; \
	for f in tests/* ; do \
		echo "" ; \
		if bin/luac "$f" > /dev/null ; then \
			echo -e "\u2714 $f" ; \
			succeed=$(($succeed + 1)) ; \
		else \
			echo -e "\u274c $f" ; \
			failed=$(($failed + 1)) ; \
		fi ; \
	done ; \
	echo -e "\nSucceed: $succeed" ; \
	echo "Failed: $failed" ; \
	echo "All: $(($succeed + $failed))" ;
endef

define dot =
	succeed=0 ; \
	failed=0 ; \
	mkdir svg ; \
	mkdir dot ; \
	for f in tests/* ; do \
		echo "" ; \
		base="${f##*/}" ; \
		clear="${base%.*}" ; \
		touch svg/"$clear".svg ; \
		touch dot/"$clear".dot ; \
		if bin/luac --dot "$f" > dot/"$clear".dot && dot -Tsvg -o svg/"$clear".svg dot/"$clear".dot ; then \
			echo -e "\u2714 $f" ; \
			succeed=$(($succeed + 1)) ; \
		else \
			rm svg/"$clear".svg ; \
			echo -e "\u274c $f" ; \
			failed=$(($failed + 1)) ; \
		fi ; \
	done ; \
	echo -e "\nSucceed: $succeed" ; \
	echo "Failed: $failed" ; \
	echo "All: $(($succeed + $failed))" ;
endef

define html =
	succeed=0 ; \
	failed=0 ; \
	mkdir html ; \
	for f in tests/* ; do \
		echo "" ; \
		base="${f##*/}" ; \
		clear="${base%.*}" ; \
		touch html/"$clear".html ; \
		if bin/luac --html "$f" > html/"$clear".html ; then \
			echo -e "\u2714 $f" ; \
			succeed=$(($succeed + 1)) ; \
		else \
			echo -e "\u274c $f" ; \
			failed=$(($failed + 1)) ; \
		fi ; \
	done ; \
	echo -e "\nSucceed: $succeed" ; \
	echo "Failed: $failed" ; \
	echo "All: $(($succeed + $failed))" ;
endef

all: luac rtl

luac: lex.yy.cpp parser.tab.cpp parser.tab.hpp ast.h node-types.h codegen.h commands.h format
		@echo "Making luac..."
		@g++ -std=c++0x -w -o bin/luac parser.tab.cpp lex.yy.cpp build-tables.cpp build-dot.cpp build-html.cpp codegen.cpp main.cpp

rtl: clean-rtl RTL/src/*.java RTL/src/RtlExceptions/*.java
		@echo "Compiling RTL..."
		@javac -d bin/ RTL/src/*.java RTL/src/RtlExceptions/*.java

run: bin/luac bin/*.class bin/RtlExceptions/*.class tests/custom.lua
		@bin/luac tests/custom.lua && java -noverify -cp bin __mainClass

parser.tab.cpp parser.tab.hpp: parser.y
		@echo "Building parser..."
		@bison -d parser.y -o parser.tab.cpp

lex.yy.cpp: lexer.l parser.tab.hpp
		@echo "Building lexer..."
		@flex -olex.yy.cpp lexer.l

test: SHELL:=/bin/bash
test: luac clean
		@$(value testrun)

dot: SHELL:=/bin/bash
dot: luac clean-dot
		@$(value dot)

html: SHELL:=/bin/bash
html: luac clean-html
		@$(value html)

format: *.h *.cpp
		@echo "Formatting..."
		@clang-format -i -style="Google" $^

.PHONY: clean clean-dot clean-html
clean: clean-dot clean-html clean-rtl
		-@rm -f *.yy.cpp *.tab.*
		-@rm -f *.log

clean-dot:
		-@rm -rf svg/
		-@rm -rf dot/

clean-html:
		-@rm -rf html/

clean-rtl:
		-@rm -f bin/*.class
		-@rm -rf bin/RtlExceptions/
