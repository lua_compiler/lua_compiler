public abstract class Function {
  public abstract PolyVar invoke(PolyVar params[]);
  public Function(){
    super();
  }
}
