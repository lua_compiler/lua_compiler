import RtlExceptions.ArithmeticException;
import RtlExceptions.CoercionException;

public class Main {

    public static void main(String[] args) {
        PolyVar i=new PolyVar(0);
        PolyVar tr=new PolyVar(5);
        PolyVar step=new PolyVar(5);
        try {
            while(i.forCheck(tr,step)==1){
                System.out.print(i.intVal);
                i=i.plus(step);
            }
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } catch (CoercionException e) {
            e.printStackTrace();
        }

    }
}
