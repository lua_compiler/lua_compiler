import RtlExceptions.*;
import RtlExceptions.ArithmeticException;

import java.lang.String;
import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;

public class PolyVar {
  private final double eps = 0.00001;
  public varType type = varType.NIL;
  public String strVal = null;
  public int intVal = 0;
  public float floatVal = 0;
  public Function functionVal = null;
  public boolean boolVal = false;
  public Table tableVal = null;

  public PolyVar(varType type) {
    this.type = type;
  }

  public PolyVar(String str) {
    this.type = varType.STRING;
    this.strVal = str;
  }

  public PolyVar(int intVal) {
    this.type = varType.INT;
    this.intVal = intVal;
  }

  public PolyVar(float floatVal) {
    this.type = varType.FLOAT;
    this.floatVal = floatVal;
  }

  public PolyVar(double floatVal) {
    this.type = varType.FLOAT;
    this.floatVal = (float) floatVal;
  }

  public PolyVar(Function func) {
    this.type = varType.FUNCTION;
    this.functionVal = func;
  }

  public PolyVar() {
    this.type = varType.NIL;
  }

  public PolyVar(int dummy0, int dummy1) {
    this.type = varType.TABLE;
    this.tableVal = new Table();
  }

  public PolyVar(boolean b) {
    this.type = varType.BOOL;
    this.boolVal = b;
  }

  public PolyVar(PolyVar copy) {
    this.type = copy.type;
    this.strVal = copy.strVal;
    this.intVal = copy.intVal;
    this.floatVal = copy.floatVal;
    this.functionVal = copy.functionVal;
    this.boolVal = copy.boolVal;
    this.tableVal = copy.tableVal;
  }

  public PolyVar invoke(PolyVar params[]) throws FunctionException {
    if (this.type == varType.FUNCTION) {
      return this.functionVal.invoke(params);
    }
    throw new FunctionException("attempt to call not-a-function");
  }

  public PolyVar plus(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a " +
            "nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar(fVal + other_fVal);
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to " +
                    "perform arithmetic on a string value");
              }
            case INT:
              return new PolyVar(fVal + other.intVal);
            case FLOAT:
              return new PolyVar(fVal + other.floatVal);
            case FUNCTION:
              throw new ArithmeticException("attempt to perform arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.intVal + other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar(this.intVal + other.intVal);
          case FLOAT:
            return new PolyVar((float) (this.intVal + other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.floatVal + other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((float) (this.floatVal + other.intVal));
          case FLOAT:
            return new PolyVar(this.floatVal + other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
    }
    return null;
  }

  public PolyVar minus(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar(fVal - other_fVal);
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to perform arithmetic on a string value");
              }
            case INT:
              return new PolyVar(fVal - other.intVal);
            case FLOAT:
              return new PolyVar(fVal - other.floatVal);
            case FUNCTION:
              throw new ArithmeticException("attempt to perform arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.intVal - other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar(this.intVal - other.intVal);
          case FLOAT:
            return new PolyVar((float) (this.intVal - other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.floatVal - other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((float) (this.floatVal - other.intVal));
          case FLOAT:
            return new PolyVar(this.floatVal - other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
    }
    return null;
  }

  public PolyVar mult(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar(fVal * other_fVal);
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to perform arithmetic on a string value");
              }
            case INT:
              return new PolyVar(fVal * other.intVal);
            case FLOAT:
              return new PolyVar(fVal * other.floatVal);
            case FUNCTION:
              throw new ArithmeticException("attempt to perform arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.intVal * other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar(this.intVal * other.intVal);
          case FLOAT:
            return new PolyVar((float) (this.intVal * other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(this.floatVal * other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((float) (this.floatVal * other.intVal));
          case FLOAT:
            return new PolyVar(this.floatVal * other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
    }
    return null;
  }

  public PolyVar div(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar((float) fVal / other_fVal);
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to perform arithmetic on a string value");
              }
            case INT:
              return new PolyVar((float) fVal / other.intVal);
            case FLOAT:
              return new PolyVar((float) fVal / other.floatVal);
            case FUNCTION:
              throw new ArithmeticException("attempt to perform arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar((float) this.intVal / other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((float) this.intVal / other.intVal);
          case FLOAT:
            return new PolyVar((float) this.intVal / other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar((float) this.floatVal / other_fVal);
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((float) this.floatVal / other.intVal);
          case FLOAT:
            return new PolyVar((float) this.floatVal / other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
    }
    return null;
  }

  public PolyVar floorDiv(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar((int) Math.floor((float) fVal / other_fVal));
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to perform arithmetic on a string value");
              }
            case INT:
              return new PolyVar((int) Math.floor((float) fVal / other.intVal));
            case FLOAT:
              return new PolyVar((int) Math.floor((float) fVal / other.floatVal));
            case FUNCTION:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform " +
              "arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar((int) Math.floor((float) this.intVal / other_fVal));
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a string value");
            }
          case INT:
            return new PolyVar((int) Math.floor((float) this.intVal / other.intVal));
          case FLOAT:
            return new PolyVar((int) Math.floor((float) this.intVal / other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar((int) Math.floor((float) this.floatVal / other_fVal));
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform arithmetic on a string value");
            }
          case INT:
            return new PolyVar((int) Math.floor((float) this.floatVal / other.intVal));
          case FLOAT:
            return new PolyVar((int) Math.floor((float) this.floatVal / other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic on a boolean value");
    }
    return null;
  }

  public PolyVar mod(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                if (other_fVal == 0) {
                  throw new ArithmeticException("attempt to" +
                      " perform 'n%0");
                }
                return new PolyVar(
                    other_fVal > 0
                        ? fVal % other_fVal
                        : (fVal % other_fVal) * -1
                );
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to " +
                    "perform arithmetic on a string" +
                    " value");
              }
            case INT:
              if (other.intVal == 0) {
                throw new ArithmeticException("attempt to" +
                    " perform 'n%0");
              }
              return new PolyVar(
                  other.intVal > 0
                      ? fVal % other.intVal
                      : (fVal % other.intVal) * -1
              );
            case FLOAT:
              if (other.floatVal < eps) {
                throw new ArithmeticException("attempt to" +
                    " perform 'n%0");
              }
              return new PolyVar(
                  other.floatVal > 0
                      ? fVal % other.floatVal
                      : (fVal % other.floatVal) * -1
              );
            case FUNCTION:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform " +
              "arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              if (other_fVal == 0) {
                throw new ArithmeticException("attempt to" +
                    " perform 'n%0");
              }
              return new PolyVar(
                  other_fVal > 0
                      ? this.intVal % other_fVal
                      : (this.intVal % other_fVal) * -1
              );
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a string value");
            }
          case INT:
            if (other.intVal == 0) {
              throw new ArithmeticException("attempt to" +
                  " perform 'n%0");
            }
            return new PolyVar(
                other.intVal > 0
                    ? this.intVal % other.intVal
                    : (this.intVal % other.intVal) * -1
            );
          case FLOAT:
            return new PolyVar((float) (this.intVal % other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              if (other_fVal == 0) {
                throw new ArithmeticException("attempt to" +
                    " perform 'n%0");
              }
              return new PolyVar(
                  other_fVal > 0
                      ? this.floatVal % other_fVal
                      : (this.floatVal % other_fVal) * -1
              );
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a string value");
            }
          case INT:
            if (other.intVal == 0) {
              throw new ArithmeticException("attempt to" +
                  " perform 'n%0");
            }
            return new PolyVar(
                other.intVal > 0
                    ? this.floatVal % other.intVal
                    : (this.floatVal % other.intVal) * -1
            );
          case FLOAT:
            return new PolyVar(this.floatVal % other.floatVal);
          case FUNCTION:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "operation on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "operation on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "operation on a boolean value");
    }
    return null;
  }

  public PolyVar exp(PolyVar other) throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          switch (other.type) {
            case NIL:
              throw new ArithmeticException("attempt to perform arithmetic on a nil value");
            case STRING:
              try {
                float other_fVal = Float.parseFloat(other.strVal);
                return new PolyVar(Math.pow(fVal, other_fVal));
              } catch (NumberFormatException ex) {
                throw new ArithmeticException("attempt to " +
                    "perform arithmetic on a string" +
                    " value");
              }
            case INT:
              return new PolyVar(Math.pow(fVal, other.intVal));
            case FLOAT:
              return new PolyVar(Math.pow(fVal, other.floatVal));
            case FUNCTION:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a function value");
            case TABLE:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a table value");
            case BOOL:
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform " +
              "arithmetic on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(Math.pow(this.intVal, other_fVal));
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a string value");
            }
          case INT:
            return new PolyVar(Math.pow(this.intVal, other.intVal));
          case FLOAT:
            return new PolyVar(Math.pow(this.intVal, other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a boolean value");
        }
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new ArithmeticException("attempt to perform arithmetic on a nil value");
          case STRING:
            try {
              float other_fVal = Float.parseFloat(other.strVal);
              return new PolyVar(Math.pow(this.floatVal, other_fVal));
            } catch (NumberFormatException ex) {
              throw new ArithmeticException("attempt to perform" +
                  " arithmetic on a string value");
            }
          case INT:
            return new PolyVar(Math.pow(this.floatVal, other.intVal));
          case FLOAT:
            return new PolyVar(Math.pow(this.floatVal, other.floatVal));
          case FUNCTION:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a function value");
          case TABLE:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a table value");
          case BOOL:
            throw new ArithmeticException("attempt to perform " +
                "arithmetic on a boolean value");
        }
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic " +
            "on a boolean value");
    }
    return null;
  }

  public PolyVar unm() throws ArithmeticException {
    switch (this.type) {
      case NIL:
        throw new ArithmeticException("attempt to perform arithmetic on a nil value");
      case STRING:
        try {
          float fVal = Float.parseFloat(this.strVal);
          return new PolyVar(-fVal);
        } catch (NumberFormatException ex) {
          throw new ArithmeticException("attempt to perform " +
              "arithmetic on a string value");
        }
      case INT:
        return new PolyVar(-this.intVal);
      case FLOAT:
        return new PolyVar(-this.floatVal);
      case FUNCTION:
        throw new ArithmeticException("attempt to perform arithmetic " +
            " on a function value");
      case TABLE:
        throw new ArithmeticException("attempt to perform arithmetic " +
            " on a table value");
      case BOOL:
        throw new ArithmeticException("attempt to perform arithmetic " +
            " on a boolean value");
    }
    return null;
  }

  public PolyVar band(PolyVar other) throws BitwiseException, CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise operation on a nil " +
            "value");
      case STRING:
        try {
          int iVal = Integer.parseInt(this.strVal);
          switch (other.type) {
            case NIL:
              throw new BitwiseException("attempt to perform bitwise " +
                  "operation on a nil value");
            case STRING:
              try {
                int other_iVal = Integer.parseInt(this.strVal);
                return new PolyVar(iVal & other_iVal);
              } catch (NumberFormatException ex) {
                throw new BitwiseException("attempt to " +
                    "perform bitwise operation on a string " +
                    "value");
              }
            case INT:
              return new PolyVar(iVal & other.intVal);
            case FLOAT:
              throw new CoercionException("number has no integer " +
                  "representation");
            case FUNCTION:
              throw new BitwiseException("attempt to perform bitwise " +
                  "operation on a function value");
            case TABLE:
              throw new BitwiseException("attempt to perform bitwise " +
                  "operation on a table value");
            case BOOL:
              throw new BitwiseException("attempt to perform bitwise " +
                  "operation on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new BitwiseException("attempt to perform bitwise " +
              "operation on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise operation " +
                "on a nil value");
          case STRING:
            try {
              int other_iVal = Integer.parseInt(this.strVal);
              return new PolyVar(this.intVal & other_iVal);
            } catch (NumberFormatException ex) {
              throw new BitwiseException("attempt to perform " +
                  "bitwise operation on a string value");
            }
          case INT:
            return new PolyVar(this.intVal & other.intVal);
          case FLOAT:
            throw new CoercionException("number has no integer representation");
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise operation " +
                "on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise operation " +
                "on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise operation " +
                "on a boolean value");
        }
      case FLOAT:
        throw new CoercionException("number has no integer representation");
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise operation on a function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise operation on a table value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
    }
    return null;
  }

  public PolyVar bor(PolyVar other) throws BitwiseException, CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise operation on a nil value");
      case STRING:
        try {
          int iVal = Integer.parseInt(this.strVal);
          switch (other.type) {
            case NIL:
              throw new BitwiseException("attempt to perform bitwise operation on a nil value");
            case STRING:
              try {
                int other_iVal = Integer.parseInt(this.strVal);
                return new PolyVar(iVal | other_iVal);
              } catch (NumberFormatException ex) {
                throw new BitwiseException("attempt to perform bitwise " +
                    "operation on a string value");
              }
            case INT:
              return new PolyVar(iVal | other.intVal);
            case FLOAT:
              throw new CoercionException("number has no integer " +
                  "representation");
            case FUNCTION:
              throw new BitwiseException("attempt to perform bitwise operation on a function value");
            case TABLE:
              throw new BitwiseException("attempt to perform bitwise operation on a table value");
            case BOOL:
              throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new BitwiseException("attempt to perform bitwise " +
              "operation on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise operation on a nil value");
          case STRING:
            try {
              int other_iVal = Integer.parseInt(this.strVal);
              return new PolyVar(this.intVal | other_iVal);
            } catch (NumberFormatException ex) {
              throw new BitwiseException("attempt to perform bitwise " +
                  "operation on a nil value");
            }
          case INT:
            return new PolyVar(this.intVal | other.intVal);
          case FLOAT:
            throw new CoercionException("number has no integer representation");
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise operation on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise operation on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
        }
      case FLOAT:
        throw new CoercionException("number has no integer representation");
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise operation on a function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise operation on a table value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
    }
    return null;
  }

  public PolyVar bxor(PolyVar other) throws BitwiseException, CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise operation on a " +
            "nil value");
      case STRING:
        try {
          int iVal = Integer.parseInt(this.strVal);
          switch (other.type) {
            case NIL:
              throw new BitwiseException("attempt to perform bitwise operation on a nil value");
            case STRING:
              try {
                int other_iVal = Integer.parseInt(this.strVal);
                return new PolyVar(iVal ^ other_iVal);
              } catch (NumberFormatException ex) {
                throw new BitwiseException("attempt to " +
                    "perform bitwise operation on a string " +
                    "value");
              }
            case INT:
              return new PolyVar(iVal ^ other.intVal);
            case FLOAT:
              throw new CoercionException("number has no integer representation");
            case FUNCTION:
              throw new BitwiseException("attempt to perform bitwise operation on a function value");
            case TABLE:
              throw new BitwiseException("attempt to perform bitwise operation on a table value");
            case BOOL:
              throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
          }
        } catch (NumberFormatException ex) {
          throw new BitwiseException("attempt to perform bitwise " +
              "operation on a string value");
        }
      case INT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise operation on a nil value");
          case STRING:
            try {
              int other_iVal = Integer.parseInt(this.strVal);
              return new PolyVar(this.intVal ^ other_iVal);
            } catch (NumberFormatException ex) {
              throw new BitwiseException("attempt to perform " +
                  "bitwise operation on a string value");
            }
          case INT:
            return new PolyVar(this.intVal ^ other.intVal);
          case FLOAT:
            throw new CoercionException("number has no integer representation");
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise operation on" +
                "a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise operation on" +
                "a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise operation on" +
                "a boolean value");
        }
      case FLOAT:
        throw new CoercionException("number has no integer representation");
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise operation on a " +
            "function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise operation on a table" +
            "value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise operation on a " +
            "boolean value");
    }
    return null;
  }

  public PolyVar rsh(PolyVar other) throws BitwiseException,
      CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a nil value");
      case STRING:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a nil value");
      case INT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case STRING:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case INT:
            if (other.intVal < 0) {
              return this.lsh(new PolyVar(-other.intVal));
            }
            return new PolyVar(this.intVal >> other.intVal);
          case FLOAT:
            if (this.eq(other).boolVal) {
              if ((int) other.floatVal < 0) {
                return this.lsh(new PolyVar(-(int) other.floatVal));
              }
              return new PolyVar(this.intVal >> (int) other
                  .floatVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a boolean value");
        }
        break;
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case STRING:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case INT:
            if (this.eq(other).boolVal) {
              if (other.intVal < 0) {
                return this.lsh(new PolyVar(-other.intVal));
              }
              return new PolyVar((int) this.floatVal >> other
                  .intVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FLOAT:
            if (this.eq(new PolyVar((int) this.floatVal)).boolVal
                && other.eq(new PolyVar((int) other.floatVal)).boolVal) {
              if (other.floatVal < 0) {
                return this.lsh(new PolyVar(-other.floatVal));
              }
              return new PolyVar((int) this.floatVal >> (int)
                  other.floatVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a boolean value");
        }
        break;
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a table value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a boolean value");
    }
    return null;
  }

  public PolyVar lsh(PolyVar other) throws BitwiseException,
      CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a nil value");
      case STRING:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a nil value");
      case INT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case STRING:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case INT:
            if (other.intVal < 0) {
              return this.rsh(new PolyVar(-other.intVal));
            }
            if (other.intVal >= 63) {
              return new PolyVar(0);  // FIXME simplified, improve
            }
            return new PolyVar(this.intVal << other.intVal);
          case FLOAT:
            if (this.eq(other).boolVal) {
              if ((int) other.floatVal < 0) {
                return this.rsh(new PolyVar(-(int) other
                    .floatVal));
              }
              if ((int) other.floatVal >= 63) {
                return new PolyVar(0);  // FIXME simplified, improve
              }
              return new PolyVar(this.intVal << (int) other
                  .floatVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a boolean value");
        }
        break;
      case FLOAT:
        switch (other.type) {
          case NIL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case STRING:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a nil value");
          case INT:
            if (this.eq(other).boolVal) {
              if (other.intVal < 0) {
                return this.rsh(new PolyVar(-other.intVal));
              }
              if (other.intVal >= 63) {
                return new PolyVar(0);  // FIXME simplified, improve
              }
              return new PolyVar((int) this.floatVal << other
                  .intVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FLOAT:
            if (this.eq(new PolyVar((int) this.floatVal)).boolVal
                && other.eq(new PolyVar((int) other.floatVal)).boolVal) {
              if ((int) other.floatVal < 0) {
                return this.rsh(new PolyVar(-(int) other.floatVal));
              }
              if ((int) other.floatVal >= 63) {
                return new PolyVar(0);  // FIXME simplified, improve
              }
              return new PolyVar((int) this.floatVal << (int)
                  other.floatVal);
            } else {
              throw new CoercionException("number has no " +
                  "integer representation");
            }
          case FUNCTION:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a function value");
          case TABLE:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a table value");
          case BOOL:
            throw new BitwiseException("attempt to perform bitwise " +
                "operation on a boolean value");
        }
        break;
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a table value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise " +
            "operation on a boolean value");
    }
    return null;
  }

  public PolyVar ubnot() throws BitwiseException, CoercionException {
    switch (this.type) {
      case NIL:
        throw new BitwiseException("attempt to perform bitwise operation on a nil value");
      case STRING:
        try {
          int iVal = Integer.parseInt(this.strVal);
          return new PolyVar(~iVal);
        } catch (NumberFormatException ex) {
          throw new BitwiseException("attempt to perform bitwise " +
              "operation on a string value");
        }
      case INT:
        return new PolyVar(~this.intVal);
      case FLOAT:
        throw new CoercionException("number has no integer representation");
      case FUNCTION:
        throw new BitwiseException("attempt to perform bitwise operation on a function value");
      case TABLE:
        throw new BitwiseException("attempt to perform bitwise operation on a table value");
      case BOOL:
        throw new BitwiseException("attempt to perform bitwise operation on a boolean value");
    }
    return null;
  }

  public PolyVar eq(PolyVar other) {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.equals(other.strVal));
          case INT:
          case FLOAT:
          case TABLE:
          case BOOL:
          case NIL:
          case FUNCTION:
            return new PolyVar(false);
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal == other.intVal);
          case FLOAT:
            return new PolyVar(Math.abs(this.intVal - other.floatVal) < eps);
          case STRING:
          case FUNCTION:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(false);
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(Math.abs(this.floatVal - other.intVal) < eps);
          case FLOAT:
            return new PolyVar(Math.abs(this.floatVal - other.floatVal) < eps);
          case STRING:
          case FUNCTION:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(false);
        }
      case FUNCTION:
        switch (other.type) {
          case FUNCTION:
            return new PolyVar(this == other); //TODO check
          case INT:
          case FLOAT:
          case STRING:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(false);
        }
      case TABLE:
        switch (other.type) {
          case TABLE:
            return new PolyVar(this == other); //TODO check
          case FUNCTION:
          case INT:
          case FLOAT:
          case STRING:
          case BOOL:
          case NIL:
            return new PolyVar(false);
        }
      case BOOL:
        switch (other.type) {
          case BOOL:
            return new PolyVar(this.boolVal == other.boolVal);
          case TABLE:
          case FUNCTION:
          case INT:
          case FLOAT:
          case STRING:
          case NIL:
            return new PolyVar(false);
        }
      case NIL:
        return new PolyVar(false);
    }
    return null;
  }

  public PolyVar neq(PolyVar other) {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.equals(other.strVal));
          case INT:
          case FLOAT:
          case TABLE:
          case BOOL:
          case NIL:
          case FUNCTION:
            return new PolyVar(true);
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal != other.intVal);
          case FLOAT:
            return new PolyVar(Math.abs(this.intVal - other
                .floatVal) >= eps);
          case STRING:
          case FUNCTION:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(true);
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(Math.abs(this.floatVal - other
                .intVal) >= eps);
          case FLOAT:
            return new PolyVar(Math.abs(this.floatVal - other
                .floatVal) >= eps);
          case STRING:
          case FUNCTION:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(true);
        }
      case FUNCTION:
        switch (other.type) {
          case FUNCTION:
            return new PolyVar(this != other); //TODO check
          case INT:
          case FLOAT:
          case STRING:
          case TABLE:
          case BOOL:
          case NIL:
            return new PolyVar(true);
        }
      case TABLE:
        switch (other.type) {
          case TABLE:
            return new PolyVar(this != other); //TODO check
          case FUNCTION:
          case INT:
          case FLOAT:
          case STRING:
          case BOOL:
          case NIL:
            return new PolyVar(true);
        }
      case BOOL:
        switch (other.type) {
          case BOOL:
            return new PolyVar(this.boolVal != other.boolVal);
          case TABLE:
          case FUNCTION:
          case INT:
          case FLOAT:
          case STRING:
          case NIL:
            return new PolyVar(true);
        }
      case NIL:
        return new PolyVar(true);
    }
    return null;
  }

  public PolyVar lt(PolyVar other) throws CoercionException {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.compareTo(other
                .strVal) < 0);
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "string with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "string with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "string with boolean");
          case FLOAT:
          case INT:
            throw new CoercionException("attempt to compare " +
                "string with number");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "string with nil");
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal < other.intVal);
          case FLOAT:
            return new PolyVar(this.intVal < other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare " +
                "number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.floatVal < other.intVal);
          case FLOAT:
            return new PolyVar(this.floatVal < other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FUNCTION:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "function with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "function with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "two function values");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "function with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "function with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "function with nil");
        }
      case TABLE:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare table" +
                " with number");
          case STRING:
            throw new CoercionException("attempt to compare table" +
                " with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "table with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "two table values");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "table with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "table with nil");
        }
      case BOOL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "boolean with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "boolean with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "boolean with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "boolean with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "two boolean values");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "boolean with nil");
        }
      case NIL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "nil with number");
          case STRING:
            throw new CoercionException("attempt to compare nil " +
                "with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "nil with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "nil with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "nil with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "two nil values");
        }
    }
    return null;
  }

  public PolyVar gt(PolyVar other) throws CoercionException {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.compareTo(other
                .strVal) > 0);
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "string with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "string with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "string with boolean");
          case FLOAT:
          case INT:
            throw new CoercionException("attempt to compare " +
                "string with number");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "string with nil");
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal > other.intVal);
          case FLOAT:
            return new PolyVar(this.intVal > other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare " +
                "number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.floatVal > other.intVal);
          case FLOAT:
            return new PolyVar(this.floatVal > other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FUNCTION:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "function with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "function with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "two function values");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "function with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "function with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "function with nil");
        }
      case TABLE:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare table" +
                " with number");
          case STRING:
            throw new CoercionException("attempt to compare table" +
                " with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "table with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "two table values");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "table with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "table with nil");
        }
      case BOOL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "boolean with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "boolean with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "boolean with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "boolean with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "two boolean values");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "boolean with nil");
        }
      case NIL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "nil with number");
          case STRING:
            throw new CoercionException("attempt to compare nil " +
                "with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "nil with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "nil with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "nil with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "two nil values");
        }
    }
    return null;
  }

  public PolyVar le(PolyVar other) throws CoercionException {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.compareTo(other
                .strVal) <= 0);
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "string with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "string with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "string with boolean");
          case FLOAT:
          case INT:
            throw new CoercionException("attempt to compare " +
                "string with number");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "string with nil");
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal <= other.intVal);
          case FLOAT:
            return new PolyVar(this.intVal <= other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare " +
                "number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.floatVal <= other.intVal);
          case FLOAT:
            return new PolyVar(this.floatVal <= other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FUNCTION:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "function with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "function with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "two function values");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "function with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "function with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "function with nil");
        }
      case TABLE:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare table" +
                " with number");
          case STRING:
            throw new CoercionException("attempt to compare table" +
                " with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "table with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "two table values");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "table with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "table with nil");
        }
      case BOOL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "boolean with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "boolean with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "boolean with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "boolean with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "two boolean values");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "boolean with nil");
        }
      case NIL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "nil with number");
          case STRING:
            throw new CoercionException("attempt to compare nil " +
                "with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "nil with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "nil with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "nil with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "two nil values");
        }
    }
    return null;
  }

  public PolyVar ge(PolyVar other) throws CoercionException {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal.compareTo(other
                .strVal) >= 0);
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "string with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "string with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "string with boolean");
          case FLOAT:
          case INT:
            throw new CoercionException("attempt to compare " +
                "string with number");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "string with nil");
        }
      case INT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.intVal >= other.intVal);
          case FLOAT:
            return new PolyVar(this.intVal >= other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare " +
                "number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FLOAT:
        switch (other.type) {
          case INT:
            return new PolyVar(this.floatVal >= other.intVal);
          case FLOAT:
            return new PolyVar(this.floatVal >= other.floatVal);
          case STRING:
            throw new CoercionException("attempt to compare number with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "number with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "number with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "number with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "number with nil");
        }
      case FUNCTION:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "function with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "function with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "two function values");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "function with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "function with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "function with nil");
        }
      case TABLE:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare table" +
                " with number");
          case STRING:
            throw new CoercionException("attempt to compare table" +
                " with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "table with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "two table values");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "table with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "table with nil");
        }
      case BOOL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "boolean with number");
          case STRING:
            throw new CoercionException("attempt to compare " +
                "boolean with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "boolean with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "boolean with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "two boolean values");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "boolean with nil");
        }
      case NIL:
        switch (other.type) {
          case INT:
          case FLOAT:
            throw new CoercionException("attempt to compare " +
                "nil with number");
          case STRING:
            throw new CoercionException("attempt to compare nil " +
                "with string");
          case FUNCTION:
            throw new CoercionException("attempt to compare " +
                "nil with function");
          case TABLE:
            throw new CoercionException("attempt to compare " +
                "nil with table");
          case BOOL:
            throw new CoercionException("attempt to compare " +
                "nil with boolean");
          case NIL:
            throw new CoercionException("attempt to compare " +
                "two nil values");
        }
    }
    return null;
  }

  public PolyVar and(PolyVar other) {
    switch (this.type) {
      case STRING:
      case INT:
      case FLOAT:
      case FUNCTION:
      case TABLE:
        return new PolyVar(other);
      case BOOL:
        return this.boolVal
            ? new PolyVar(other)
            : new PolyVar(this);
      case NIL:
        return new PolyVar(this);
    }
    return null;
  }

  public PolyVar or(PolyVar other) {
    switch (this.type) {
      case STRING:
      case INT:
      case FLOAT:
      case FUNCTION:
      case TABLE:
        return new PolyVar(this);
      case BOOL:
        return this.boolVal
            ? new PolyVar(this)
            : new PolyVar(other);
      case NIL:
        return new PolyVar(other);
    }
    return null;
  }

  public PolyVar not() {
    return new PolyVar(this.evaluate() == 1 ? false : true);
  }

  public PolyVar concat(PolyVar other) throws CoercionException {
    switch (this.type) {
      case STRING:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.strVal + other.strVal);
          case INT:
            return new PolyVar(this.strVal + other.intVal);
          case FLOAT:
            return new PolyVar(this.strVal + other.floatVal);
          case FUNCTION:
            throw new CoercionException("attempt to concatenate a" +
                " function value");
          case TABLE:
            throw new CoercionException("attempt to concatenate a" +
                " table value");
          case BOOL:
            throw new CoercionException("attempt to concatenate a" +
                " boolean value");
          case NIL:
            throw new CoercionException("attempt to concatenate a" +
                " nil value");
        }
      case INT:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.intVal + other.strVal);
          case INT:
            return new PolyVar(String.valueOf(this.intVal) + other.intVal);
          case FLOAT:
            return new PolyVar(String.valueOf(this.intVal) + other.floatVal);
          case FUNCTION:
            throw new CoercionException("attempt to concatenate a" +
                " function value");
          case TABLE:
            throw new CoercionException("attempt to concatenate a" +
                " table value");
          case BOOL:
            throw new CoercionException("attempt to concatenate a" +
                " boolean value");
          case NIL:
            throw new CoercionException("attempt to concatenate a" +
                " nil value");
        }
      case FLOAT:
        switch (other.type) {
          case STRING:
            return new PolyVar(this.floatVal + other.strVal);
          case INT:
            return new PolyVar(String.valueOf(this.floatVal) + other.intVal);
          case FLOAT:
            return new PolyVar(String.valueOf(this.floatVal) + other.floatVal);
          case FUNCTION:
            throw new CoercionException("attempt to concatenate a" +
                " function value");
          case TABLE:
            throw new CoercionException("attempt to concatenate a" +
                " table value");
          case BOOL:
            throw new CoercionException("attempt to concatenate a" +
                " boolean value");
          case NIL:
            throw new CoercionException("attempt to concatenate a" +
                " nil value");
        }
      case FUNCTION:
        throw new CoercionException("attempt to concatenate a" +
            " function value");
      case TABLE:
        throw new CoercionException("attempt to concatenate a" +
            " table value");
      case BOOL:
        throw new CoercionException("attempt to concatenate a" +
            " boolean value");
      case NIL:
        throw new CoercionException("attempt to concatenate a" +
            " nil value");
    }
    return null;
  }

  public PolyVar len() throws CoercionException {
    switch (this.type) {
      case STRING:
        return new PolyVar(this.strVal.length());
      case TABLE:
        try{
          for (int i = Collections.max(this.tableVal.intMap.keySet()); i >=
              0; --i) {
            if (this.tableVal.intMap.containsKey(i) && this.tableVal.intMap
                .get(i).type != varType.NIL) {
              return new PolyVar(i);
            }
          }
          return new PolyVar(0);
        }
        catch(NoSuchElementException e){
          return new PolyVar(0);
        }
      case INT:
      case FLOAT:
        throw new CoercionException("attempt to get length of a " +
            "number value");
      case FUNCTION:
        throw new CoercionException("attempt to get length of a " +
            "function value");
      case BOOL:
        throw new CoercionException("attempt to get length of a " +
            "boolean value");
      case NIL:
        throw new CoercionException("attempt to get length of a nil " +
            "value");
    }
    return null;
  }

  public int evaluate() {
    if (this.type == varType.NIL ||
        (this.type == varType.BOOL && !this.boolVal)) {
      return 0;
    }
    return 1;
  }

  public PolyVar getTableField(PolyVar index) throws TableException {
    if (this.type != varType.TABLE) {
      PolyVar args[]=new PolyVar[1];
      args[0]=this;
      throw new TableException("attempt to index not-a-table value\n"+this.type.toString());
    }
    if (index.type == varType.INT) {
      PolyVar r = this.tableVal.intMap.get(index.intVal);
      if (r != null) {
        return r;
      } else {
        return new PolyVar();
      }
    } else if (index.type == varType.STRING) {
      PolyVar r = this.tableVal.strMap.get(index.strVal);
      if (r != null) {
        return r;
      } else {
        return new PolyVar();
      }
    } else throw new TableException("this exception is never being thrown");
  }

  public void setTableField(PolyVar index, PolyVar val) throws TableException {
    if (this.type != varType.TABLE) {
      throw new TableException("attempt to index not-a-table value");
    }
    if (index.type == varType.INT) {
      this.tableVal.intMap.put(index.intVal, val);
    } else if (index.type == varType.STRING) {
      this.tableVal.strMap.put(index.strVal, val);
    } else throw new TableException("this exception is never being thrown");
  }

  public PolyVar inc(int val) throws ArithmeticException {
    PolyVar incVal = new PolyVar(val);
    return this.plus(incVal);
  }

  public int forCheck(PolyVar treshold, PolyVar step) throws ArithmeticException, CoercionException {
    if ((this.type == varType.INT || this.type == varType.FLOAT)
        && (treshold.type == varType.INT || treshold.type == varType.FLOAT)
        && (step.type == varType.INT || step.type == varType.FLOAT)) {
      if ((step.ge(new PolyVar(0)).boolVal && this.ge(treshold).boolVal) ||
          (step.lt(new PolyVar(0)).boolVal && this.lt(treshold).boolVal)) {
        return 0;
      } else {
        return 1;
      }
    } else {
      throw new ArithmeticException("for params must be numbers");
    }
  }

  public PolyVar isNil() {
    return new PolyVar(this.type == varType.NIL);
  }

  public PolyVar isBool() {
    return new PolyVar(this.type == varType.BOOL);
  }

  public PolyVar isInt() {
    return new PolyVar(this.type == varType.INT);
  }

  public PolyVar isFloat() {
    return new PolyVar(this.type == varType.FLOAT);
  }

  public PolyVar isString() {
    return new PolyVar(this.type == varType.STRING);
  }

  public PolyVar isTable() {
    return new PolyVar(this.type == varType.TABLE);
  }
}
