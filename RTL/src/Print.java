import java.util.Scanner;

public class Print {
  public static PolyVar print(PolyVar[] params) {
    for (PolyVar arg : params) {
      switch (arg.type) {
        case NIL:
          System.out.print("<Nil>\t");
          break;
        case STRING:
          System.out.print("<String> " + arg.strVal + "\t");
          break;
        case INT:
          System.out.print("<Int> " + arg.intVal + "\t");
          break;
        case FLOAT:
          System.out.print("<Float> " + arg.floatVal + "\t");
          break;
        case FUNCTION:
          System.out.print("<Function>" + arg.toString() + "\t");
          break;
        case TABLE:
          System.out.print("<Table>" + arg.toString() + "\t");
          break;
        case BOOL:
          System.out.print(arg.boolVal ? "<Bool>" + "true" : "<Bool>" + "false" + "\t");
          break;
      }
    }
    System.out.print("\n");
    return new PolyVar(varType.NIL);
  }

  public static PolyVar read() {
    Scanner read = new Scanner(System.in);
    String str = read.next();
    try {
      int intVal = Integer.parseInt(str);
      return new PolyVar(intVal);
    } catch (NumberFormatException e) {
    }
    try {
      float flVal = Float.parseFloat(str);
      return new PolyVar(flVal);
    } catch (NumberFormatException e) {
    }
    return new PolyVar(str);
  }
}
