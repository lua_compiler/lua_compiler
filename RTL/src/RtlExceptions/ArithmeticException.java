package RtlExceptions;

public class ArithmeticException extends RtlException {
  public ArithmeticException(String message) {
    super(message);
  }

  public ArithmeticException(Throwable cause) {
    super(cause);
  }

  public ArithmeticException(String message, Throwable cause) {
    super(message, cause);
  }
}
