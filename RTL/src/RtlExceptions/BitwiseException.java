package RtlExceptions;


public class BitwiseException extends RtlException {

  public BitwiseException(String message) {
    super(message);
  }

  public BitwiseException(Throwable cause) {
    super(cause);
  }

  public BitwiseException(String message, Throwable cause) {
    super(message, cause);
  }
}
