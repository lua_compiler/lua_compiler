package RtlExceptions;


public class CoercionException extends RtlException {
  public CoercionException(String message) {
    super(message);
  }

  public CoercionException(Throwable cause) {
    super(cause);
  }

  public CoercionException(String message, Throwable cause) {
    super(message, cause);
  }
}
