package RtlExceptions;

/**
 * Created by sanis on 11.06.2017.
 */
public class FunctionException extends RtlException {

	public FunctionException(String message) {
		super(message);
	}

	public FunctionException(Throwable cause) {
		super(cause);
	}

	public FunctionException(String message, Throwable cause) {
		super(message, cause);
	}
}
