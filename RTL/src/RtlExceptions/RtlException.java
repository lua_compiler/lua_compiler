package RtlExceptions;


public class RtlException extends Exception {
  public RtlException(String message) {
    super(message);
  }

  public RtlException(Throwable cause) {
    super(cause);
  }

  public RtlException(String message, Throwable cause) {
    super(message, cause);
  }
}
