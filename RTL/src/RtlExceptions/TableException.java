package RtlExceptions;

/**
 * Created by sanis on 12.06.2017.
 */
public class TableException extends RtlException {

	public TableException(String message) {
		super(message);
	}

	public TableException(Throwable cause) {
		super(cause);
	}

	public TableException(String message, Throwable cause) {
		super(message, cause);
	}
}
