#ifndef AST_H
#define AST_H

#include <fstream>
#include <ios>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "node-types.h"

#define MAIN_CLASS_NAME "__mainClass"
#define RESERVE_LOCAL_VARS_FOR_ASSIGNMENT 23
#define MAX_STACK_SIZE 2310

typedef std::vector<std::string *> NameList;

class Stat;
class Var;
class Exp;

typedef std::vector<Stat *> StatList;
typedef std::vector<Var *> VarList;
typedef std::vector<Exp *> ExpList;
typedef std::map<std::string, unsigned int> VarTable;

enum class constTableEntryType {
  dummy = -1,
  utf8 = 1,
  integer = 3,
  floating = 4,
  str = 8,
  nameAndType = 12,
  const_class = 7,
  fieldref = 9,
  methodref = 10
};

class ConstTableEntry {
 public:
  constTableEntryType type;
  std::string *strValue;
  int intVal1 = -1;
  int intVal2 = -1;
  float flVal = -1;
  std::string *varName;
  std::string *className;
  bool localField = true;
  void writeToClassFile(std::ofstream &out);

  ConstTableEntry(constTableEntryType type, std::string *strValue,
                  int intVal1 = -1, int intVal2 = -1,
                  std::string *varName = new std::string(""), float flVal = -1,
                  std::string *className = new std::string(""))
      : type(type),
        strValue(strValue),
        intVal1(intVal1),
        intVal2(intVal2),
        varName(varName),
        flVal(flVal),
        className(className) {}
};

typedef std::vector<ConstTableEntry *> ConstTable;

void buildHtml(ConstTable *);
void buildHtml(VarTable *);
std::string cteType(constTableEntryType type);
unsigned int findUtf(ConstTable *constTable, const std::string &str);
unsigned int findByVarName(std::string &id, ConstTable *constTable);
unsigned int findByClassName(std::__cxx11::string id, ConstTable *constTable);
unsigned int findMethodref(std::__cxx11::string id, ConstTable *constTable);
unsigned int findVar(std::string *id, VarTable *localTable);
int write(std::vector<char> *v, short data);
int write(std::vector<char> *v, int data);
int write(std::vector<char> *v, unsigned short data);
int write(std::vector<char> *v, unsigned int data);
int write(std::vector<char> *v, char data);
void write(std::vector<char> *v, short data, unsigned int index);
void write(std::vector<char> *v, int data, unsigned int index);
void write(std::vector<char> *v, unsigned short data, unsigned int index);
void write(std::vector<char> *v, unsigned int data, unsigned int index);

void fillPolyVarFields(ConstTable *table,
                       std::map<std::string, std::string> *map);
void fillPolyVarMethods(ConstTable *table,
                        std::map<std::string, std::string> *map);
std::map<std::string, std::string> *generatePolyVarFields();
std::map<std::string, std::string> *generatePolyVarMethods();

class Node {  //В каждом новом скоупе создается новая копия переданного
              //локалтейбл и она считается локал тейблом текущего скоупа
 public:
  virtual void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                           VarTable *globalVarTable) {}

  virtual ~Node() {}
};

class Field : public Node {
 public:
  NT_Field type;
  Exp *rExp = nullptr;
  Exp *lExp = nullptr;
  std::string *id = nullptr;
  unsigned int indexInTable = -1;

  Field(Exp *exp1, Exp *exp2)
      : lExp(exp1), rExp(exp2), type(NT_Field::IndexSet) {}

  Field(std::string *id, Exp *exp) : id(id), rExp(exp), type(NT_Field::IdSet) {}

  Field(Exp *exp) : lExp(exp), type(NT_Field::ExpSet) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
};

typedef std::vector<Field *> FieldList;

class RetStat : public Node {
 public:
  NT_RetStat type;
  ExpList *explist = nullptr;

  RetStat() : explist(new ExpList()), type(NT_RetStat::Empty) {}

  RetStat(ExpList *explist) : explist(explist), type(NT_RetStat::NonEmpty) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *constTable,
                   VarTable *varTable);
};

class Block : public Node {
 public:
  NT_Block type;
  StatList *stlist = nullptr;
  RetStat *retstat = nullptr;
  VarTable *localVarTable = nullptr;
  ConstTable *localConstTable = nullptr;
  ConstTable *globalConstTable = nullptr;
  bool inMainChunk = true;
  bool isInLoop = false;

  Block(NT_Block type) : stlist(new StatList()), type(type) {}

  Block(StatList *stlist) : stlist(stlist), type(NT_Block::Stats) {}

  Block(RetStat *retstat) : retstat(retstat), type(NT_Block::Return) {}

  Block(StatList *stlist, RetStat *retstat)
      : stlist(stlist), retstat(retstat), type(NT_Block::StatsReturn) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  void buildHtml() const;
  int generateCode(std::vector<char> *code);
  void generateBreakAddress(std::vector<char> *code);
};

class TableConstructor : public Node {
 public:
  NT_TableConstructor type;
  FieldList *fieldlist = nullptr;

  TableConstructor()
      : fieldlist(new FieldList()), type(NT_TableConstructor::Empty) {}

  TableConstructor(FieldList *fieldlist)
      : fieldlist(fieldlist), type(NT_TableConstructor::List) {}

  TableConstructor(Field *field) {
    fieldlist = new FieldList();
    fieldlist->push_back(field);
    this->type = NT_TableConstructor::Single;
  }

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class Args : public Node {
 public:
  NT_Args type;
  ExpList *explist = nullptr;

  Args() : type(NT_Args::Empty) {}

  Args(ExpList *explist) : explist(explist), type(NT_Args::ExpList) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class FunctionCall;

class PrefixExp : public Node {
 public:
  NT_PrefixExp type;
  Var *var = nullptr;
  FunctionCall *fCall = nullptr;
  Exp *exp = nullptr;

  PrefixExp(Var *var) : var(var), type(NT_PrefixExp::Var) {}

  PrefixExp(FunctionCall *fCall) : fCall(fCall), type(NT_PrefixExp::FuncCall) {}

  PrefixExp(Exp *exp) : exp(exp), type(NT_PrefixExp::Exp) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class FunctionCall : public Node {
 public:
  NT_FunctionCall type;
  PrefixExp *prefixexp = nullptr;
  std::string *id = nullptr;
  Args *args = nullptr;

  FunctionCall(PrefixExp *prefixexp, Args *args)
      : prefixexp(prefixexp), args(args), type(NT_FunctionCall::FuncCall) {}

  FunctionCall(PrefixExp *prefixexp, std::string *id, Args *args)
      : prefixexp(prefixexp),
        id(id),
        args(args),
        type(NT_FunctionCall::MethodCall) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class Var : public Node {
 public:
  NT_Var type;
  std::string *id = nullptr;
  PrefixExp *prefixexp = nullptr;
  Exp *exp = nullptr;
  unsigned int indexInTable = -1;
  bool local = false;

  Var(std::string *id) : id(id), type(NT_Var::Id) {}

  Var(PrefixExp *prefixexp, Exp *exp)
      : prefixexp(prefixexp), exp(exp), type(NT_Var::TableField) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class ElseIf : public Node {
 public:
  NT_ElseIf type;
  ElseIf *elseif = nullptr;
  Exp *exp = nullptr;
  Block *block = nullptr;
  bool isInLoop = false;
  bool inMainChunk = false;
  ElseIf(Exp *exp, Block *block)
      : exp(exp), block(block), type(NT_ElseIf::Single) {}

  ElseIf(ElseIf *elseif, Exp *exp, Block *block)
      : elseif(elseif), exp(exp), block(block), type(NT_ElseIf::Several) {}

  void buildDot(unsigned int *id);
  void buildHtml() const;
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   std::vector<int> &gotos, VarTable *varTable);
  void generateBreakAddress(std::vector<char> *code);
};

class IfElse : public Node {
 public:
  NT_IfElse type;
  Exp *exp = nullptr;
  Block *ifBlock = nullptr;
  Block *elseBlock = nullptr;
  ElseIf *elseif = nullptr;
  bool isInLoop = false;
  bool inMainChunk = false;
  IfElse(Exp *exp, Block *block)
      : exp(exp), ifBlock(block), type(NT_IfElse::IfOnly) {}

  IfElse(Exp *exp, Block *block, ElseIf *elseif)
      : exp(exp), ifBlock(block), elseif(elseif), type(NT_IfElse::IfElseif) {}

  IfElse(Exp *exp, Block *ifBlock, Block *elseBlock)
      : exp(exp),
        ifBlock(ifBlock),
        elseBlock(elseBlock),
        type(NT_IfElse::IfElse) {}

  IfElse(Exp *exp, Block *ifBlock, ElseIf *elseif, Block *elseBlock)
      : exp(exp),
        ifBlock(ifBlock),
        elseif(elseif),
        elseBlock(elseBlock),
        type(NT_IfElse::IfElseifElse) {}

  void buildDot(unsigned int *id);
  void buildHtml() const;
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class ForLoop : public Node {
 public:
  std::string *id = nullptr;
  Exp *initExp = nullptr;
  Exp *endExp = nullptr;
  Exp *stepExp = nullptr;
  Block *block = nullptr;
  VarTable *varTableForI = nullptr;

  ForLoop(std::string *id, Exp *exp1, Exp *exp2, Exp *exp3, Block *block)
      : id(id), initExp(exp1), endExp(exp2), stepExp(exp3), block(block) {}

  void buildDot(unsigned int *id);
  void buildHtml() const;
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *constTable,
                   VarTable *varTable);
};

class VarDecl : public Node {
 public:
  NT_VarDecl type;
  NameList *namelist = nullptr;
  ExpList *explist = nullptr;

  VarDecl(NameList *namelist)
      : namelist(namelist), type(NT_VarDecl::DeclOnly) {}

  VarDecl(NameList *namelist, ExpList *explist)
      : namelist(namelist), explist(explist), type(NT_VarDecl::DeclDef) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *constTable,
                   VarTable *varTable);
};

class ParList : public Node {
 public:
  NT_ParList type;
  NameList *namelist = nullptr;

  ParList() : namelist(new NameList()), type(NT_ParList::Usual) {}

  ParList(NameList *namelist) : namelist(namelist), type(NT_ParList::Usual) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
};

class FuncBody : public Node {
 public:
  NT_FuncBody type;
  ParList *parlist = nullptr;
  Block *block = nullptr;
  unsigned int classIndex = -1;
  unsigned int constructorIndex = -1;
  std::string *className = nullptr;
  ConstTable *newConstTable = nullptr;

  FuncBody(Block *block)
      : parlist(new ParList()),
        block(block),
        type(NT_FuncBody::WithoutParams) {}

  FuncBody(ParList *parlist, Block *block)
      : parlist(parlist), block(block), type(NT_FuncBody::WithParams) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  void buildHtml(std::string *funcName) const;
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class FunctionDef : public Node {
 public:
  FuncBody *funcbody = nullptr;
  unsigned int indexInTable = -1;
  FunctionDef(FuncBody *funcbody) : funcbody(funcbody) {}

  void buildDot(unsigned int *id);
  void buildHtml() const;
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
};

class Stat : public Node {
 public:
  NT_Stat type;
  VarList *varlist = nullptr;
  ExpList *explist = nullptr;
  PrefixExp *prefixexp = nullptr;
  Exp *exp = nullptr;
  Block *block = nullptr;
  IfElse *ifelse = nullptr;
  ForLoop *forloop = nullptr;
  NameList *namelist = nullptr;
  FuncBody *funcbody = nullptr;
  VarDecl *vardecl = nullptr;
  std::string *id = nullptr;
  unsigned int indexInTable = -1;
  bool isInLoop = false;
  int breakAddress = -1;  // bytecode address where to
  bool inMainChunk = false;

  Stat() : type(NT_Stat::Empty) {}

  Stat(VarList *varlist, ExpList *explist)
      : varlist(varlist), explist(explist), type(NT_Stat::VarDef) {}

  Stat(PrefixExp *prefixexp)
      : prefixexp(prefixexp), type(NT_Stat::FuncCallOnly) {}

  Stat(std::string *label) : id(label), type(NT_Stat::Label) {}

  Stat(NT_Stat type) : type(type) {}

  Stat(std::string *id, NT_Stat type) : type(type), id(id) {}

  Stat(Block *block) : block(block), type(NT_Stat::Block) {}

  Stat(Exp *exp, Block *block, NT_Stat type)
      : exp(exp), block(block), type(type) {}

  Stat(IfElse *ifelse) : ifelse(ifelse), type(NT_Stat::IfElse) {}

  Stat(ForLoop *forloop) : forloop(forloop), type(NT_Stat::ForLoop) {}

  Stat(NameList *namelist, ExpList *explist, Block *block)
      : namelist(namelist),
        explist(explist),
        block(block),
        type(NT_Stat::ForEach) {}

  Stat(std::string *funcname, FuncBody *funcbody, NT_Stat type)
      : id(funcname), funcbody(funcbody), type(type) {}

  Stat(VarDecl *vardecl) : vardecl(vardecl), type(NT_Stat::VarDecl) {}

  void buildDot(unsigned int *id);
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

class Exp : public Node {
 public:
  NT_Exp type = NT_Exp::Nil;
  Exp *lExp = nullptr;
  Exp *rExp = nullptr;
  bool b = false;
  int i = 0;
  double d = 0.0;
  std::string *s = nullptr;
  FunctionDef *fDef = nullptr;
  PrefixExp *prefExp = nullptr;
  TableConstructor *tConstr = nullptr;
  unsigned int indexInTable = -1;

  Exp() : type(NT_Exp::Nil) {}

  Exp(bool b) : b(b), type(NT_Exp::Bool) {}

  Exp(int i) : i(i), type(NT_Exp::Int) {}

  Exp(double d) : d(d), type(NT_Exp::Float) {}

  Exp(std::string *s) : s(s), type(NT_Exp::String) {}

  Exp(NT_Exp type) : type(type) {}

  Exp(FunctionDef *fDef) : fDef(fDef), type(NT_Exp::Funcdef) {}

  Exp(PrefixExp *prefExp) : prefExp(prefExp), type(NT_Exp::Prefexp) {}

  Exp(TableConstructor *tConstr) : tConstr(tConstr), type(NT_Exp::Tconstr) {}

  Exp(NT_Exp type, Exp *lExp, Exp *rExp) : type(type), lExp(lExp), rExp(rExp) {}

  Exp(NT_Exp type, Exp *lExp) : type(type), lExp(lExp) {}

  void buildDot(unsigned int *id);
  void buildHtml() const;
  void buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                   VarTable *globalVarTable);
  int generateCode(std::vector<char> *code, ConstTable *table,
                   VarTable *varTable);
};

#endif  // AST_H
