#include "ast.h"

void buildDot(NameList *node, unsigned int *id) {
  unsigned int nodeId = (*id)++;
  printf("\"NameList #%u\"", nodeId);
  for (unsigned long i = 0; i < node->size(); i++) {
    printf("\"NameList #%u\" -> \"%s #%u\"\n", nodeId, node->at(i)->c_str(),
           (*id)++);
  }
}

void buildDot(StatList *node, unsigned int *id) {
  unsigned int nodeId = (*id)++;
  printf("\"Stat List #%u\"", nodeId);
  for (unsigned long i = 0; i < node->size(); i++) {
    printf("\"Stat List #%u\" -> ", nodeId);
    node->at(i)->buildDot(id);
    printf("\n");
  }
}

void buildDot(VarList *node, unsigned int *id) {
  unsigned int nodeId = (*id)++;
  for (unsigned long i = 0; i < node->size(); i++) {
    printf("\"VarList #%u\" -> ", nodeId);
    node->at(i)->buildDot(id);
    printf("\n");
    (*id)++;
  }
}

void buildDot(ExpList *node, unsigned int *id) {
  unsigned int nodeId = (*id)++;
  printf("\"Expression List #%u\"\n", nodeId);
  for (unsigned long i = 0; i < node->size(); i++) {
    printf("\"Expression List #%u\" -> ", nodeId);
    node->at(i)->buildDot(id);
    printf("\n");
  }
}

void buildDot(FieldList *node, unsigned int *id) {
  unsigned int nodeId = (*id)++;
  printf("\"Field List #%u\"", nodeId);
  for (unsigned long i = 0; i < node->size(); i++) {
    printf("\"Field List #%u\" -> ", nodeId);
    node->at(i)->buildDot(id);
    printf("\n");
  }
}

void Field::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Field #%u\"", thisId);

  if (this->type == NT_Field::IndexSet) {
    unsigned int eqId = (*id)++;
    printf("\n\"Field #%u\" -> \"= #%u\"", thisId, eqId);

    printf("\n\"= #%u\" -> \"[] #%u\" -> ", eqId, (*id)++);
    this->lExp->buildDot(id);

    printf("\"= #%u\" -> ", eqId);
    this->rExp->buildDot(id);

  } else if (this->type == NT_Field::IdSet) {
    unsigned int eqId = (*id)++;
    printf("\n\"Field #%u\" -> \"= #%u\"", thisId, eqId);

    printf("\n\"= #%u\" -> \"%s indexInTable=%d #%u\"", eqId, this->id->c_str(),
           this->indexInTable, (*id)++);

    printf("\n\"= #%u\" -> ", eqId);
    this->rExp->buildDot(id);

  } else if (this->type == NT_Field::ExpSet) {
    printf("\n\"Field #%u\" -> ", thisId);
    this->lExp->buildDot(id);
  }
}

void RetStat::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Return statement #%u\" -> ", thisId);
  ::buildDot(this->explist, id);
}

void Block::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  if (this->type == NT_Block::Stats || this->type == NT_Block::StatsReturn) {
    for (unsigned long i = 0; i < this->stlist->size(); i++) {
      printf("\"Block #%u\" -> ", thisId);
      this->stlist->at(i)->buildDot(id);
      (*id)++;
    }
  }
  if (this->type == NT_Block::Return || this->type == NT_Block::StatsReturn) {
    printf("\"Block #%u\" -> ", thisId);
    this->retstat->buildDot(id);
  }
}

void TableConstructor::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Table constructor #%u\" -> ", thisId);
  ::buildDot(this->fieldlist, id);
}

void Args::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Args #%u\"", thisId);

  if (this->type == NT_Args::ExpList) {
    printf("\n\"Args #%u\" -> ", thisId);
    ::buildDot(this->explist, id);
  }
}

void PrefixExp::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Prefix exp #%u\" -> ", thisId);

  if (this->type == NT_PrefixExp::Var) {
    this->var->buildDot(id);
  }

  if (this->type == NT_PrefixExp::FuncCall) {
    this->fCall->buildDot(id);
  }

  if (this->type == NT_PrefixExp::Exp) {
    this->exp->buildDot(id);
  }
}

void FunctionCall::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Function call #%u\"", thisId);

  printf("\n\"Function call #%u\" -> ", thisId);
  this->prefixexp->buildDot(id);

  if (this->type == NT_FunctionCall::MethodCall) {
    printf("\n\"Function call #%u\" -> \"%s #%u\"", thisId, this->id->c_str(),
           (*id)++);
  }

  printf("\n\"Function call #%u\" -> ", thisId);
  this->args->buildDot(id);
}

void Var::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  if (this->type == NT_Var::Id) {
    printf("\"Var %s indexIntable=%d #%u\"", this->id->c_str(),
           this->indexInTable, thisId);

  } else if (this->type == NT_Var::TableField) {
    printf("\"Var [] #%u\" -> ", thisId);
    this->prefixexp->buildDot(id);
    printf("\n\"Var [] #%u\" -> ", thisId);
    this->exp->buildDot(id);
  }
}

void ElseIf::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"ElseIf #%u\"", thisId);

  if (this->type == NT_ElseIf::Several) {
    printf("\"ElseIf #%u\" -> ", thisId);
    this->elseif->buildDot(id);
  }

  printf("\"ElseIf #%u\" -> ", thisId);
  this->exp->buildDot(id);

  printf("\"ElseIf #%u\" -> ", thisId);
  this->block->buildDot(id);
}

void IfElse::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Ifelse #%u\"", thisId);

  printf("\n\"Ifelse #%u\" -> ", thisId);
  this->exp->buildDot(id);

  printf("\n\"Ifelse #%u\" -> ", thisId);
  this->ifBlock->buildDot(id);

  if (this->type == NT_IfElse::IfElseif ||
      this->type == NT_IfElse::IfElseifElse) {
    printf("\n\"Ifelse #%u\" -> ", thisId);
    this->elseif->buildDot(id);
  }

  if (this->type == NT_IfElse::IfElse ||
      this->type == NT_IfElse::IfElseifElse) {
    printf("\n\"Ifelse #%u\" -> ", thisId);
    this->elseBlock->buildDot(id);
  }
}

void ForLoop::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"For loop #%u\"", thisId);

  printf("\n\"For loop #%u\" -> \"%s #%u\"", thisId, this->id->c_str(),
         (*id)++);

  printf("\n\"For loop #%u\" -> ", thisId);
  this->initExp->buildDot(id);

  printf("\n\"For loop #%u\" -> ", thisId);
  this->endExp->buildDot(id);

  printf("\n\"For loop #%u\" -> ", thisId);
  this->stepExp->buildDot(id);

  printf("\n\"For loop #%u\" -> ", thisId);
  this->block->buildDot(id);
}

void VarDecl::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  if (this->type == NT_VarDecl::DeclOnly) {
    for (unsigned long i = 0; i < this->namelist->size(); i++) {
      printf("\"Variables Declaration #%u\" -> \"%s #%u\"\n", thisId,
             this->namelist->at(i)->c_str(), (*id)++);
    }
  } else {
    unsigned int nameListId = (*id)++;
    printf("\"= #%u\" -> \"Name List #%u\"\n", thisId, nameListId);
    for (unsigned long i = 0; i < this->namelist->size(); i++) {
      printf("\"Name List #%u\" -> \"%s #%u\"\n", nameListId,
             this->namelist->at(i)->c_str(), (*id)++);
    }
    printf("\"= #%u\" -> ", thisId);
    ::buildDot(this->explist, id);
  }
}

void ParList::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Parameters List #%u\"\n", thisId);
  for (unsigned long i = 0; i < this->namelist->size(); i++) {
    printf("\"Parameters List #%u\" -> \"%s #%u\"\n", thisId,
           this->namelist->at(i)->c_str(), (*id)++);
  }
}

void FuncBody::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  if (this->type == NT_FuncBody::WithoutParams) {
    printf("\"Function body #%u\" -> ", thisId);
    this->block->buildDot(id);
  } else {
    printf("\"Function body #%u\" -> ", thisId);
    this->parlist->buildDot(id);
    printf("\n\"Function body #%u\" -> ", thisId);
    this->block->buildDot(id);
  }
}

void FunctionDef::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  printf("\"Function definition indexInTable=%d #%u\" -> ", this->indexInTable,
         thisId);
  this->funcbody->buildDot(id);
}

void Stat::buildDot(unsigned int *id) {
  unsigned int thisId = (*id)++;
  if (this->type == NT_Stat::VarDef) {
    printf("\"= #%u\" -> ", thisId);
    ::buildDot(this->varlist, id);

    printf("\n\"= #%u\" -> ", thisId);
    ::buildDot(this->explist, id);

  } else if (this->type == NT_Stat::FuncCallOnly) {
    this->prefixexp->buildDot(id);

  } else if (this->type == NT_Stat::Label) {
    printf("\"Label indexInTable=%d #%u\" -> %s", (*id)++, this->indexInTable,
           this->id->c_str());

  } else if (this->type == NT_Stat::Break) {
    printf("\"Break #%u\"", (*id)++);

  } else if (this->type == NT_Stat::Goto) {
    printf("\"Goto indexInTable=%d #%u\" -> %s", (*id)++, this->indexInTable,
           this->id->c_str());

  } else if (this->type == NT_Stat::Block) {
    this->block->buildDot(id);

  } else if (this->type == NT_Stat::WhileLoop) {
    printf("\"While #%u\" -> ", thisId);
    this->exp->buildDot(id);

    printf("\n\"While #%u\" -> ", thisId);
    this->block->buildDot(id);

  } else if (this->type == NT_Stat::RepUntilLoop) {
    printf("\"Until #%u\" -> ", thisId);
    this->block->buildDot(id);

    printf("\n\"Until #%u\" -> ", thisId);
    this->exp->buildDot(id);

  } else if (this->type == NT_Stat::IfElse) {
    this->ifelse->buildDot(id);

  } else if (this->type == NT_Stat::ForLoop) {
    this->forloop->buildDot(id);

  } else if (this->type == NT_Stat::ForEach) {
    printf("\"For in do #%u\" -> ", thisId);
    ::buildDot(this->namelist, id);

    printf("\n\"For in do #%u\" -> ", thisId);
    ::buildDot(this->explist, id);

    printf("\n\"For in do #%u\" -> ", thisId);
    this->block->buildDot(id);

  } else if (this->type == NT_Stat::FuncDecl) {
    printf("\"Function declaration #%u\" -> \"%s indexInTable=%d\"", (*id),
           this->id->c_str(), this->indexInTable);

    printf("\n\"Function declaration #%u\" -> ", (*id)++);
    this->funcbody->buildDot(id);

  } else if (this->type == NT_Stat::LocalFuncDecl) {
    printf("\"Local function declaration #%u\" -> \"%s indexInTable=%d\"",
           (*id)++, this->id->c_str(), this->indexInTable);

    printf("\n\"Local function declaration #%u\" -> ", (*id)++);
    this->funcbody->buildDot(id);

  } else if (this->type == NT_Stat::VarDecl) {
    this->vardecl->buildDot(id);
  }
}

void Exp::buildDot(unsigned int *id) {
  const char *types[] = {"Nil",     "Bool",     "Int",      "Float",   "String",
                         "Funcdef", "Prefexp",  "Funccall", "Tconstr", "Plus",
                         "Minus",   "Multiply", "Div",      "Divint",  "Pow",
                         "Mod",     "Band",     "Bor",      "Xor",     "Shl",
                         "Shr",     "Concat",   "Lt",       "Lte",     "Gt",
                         "Gte",     "Eq",       "Neq",      "And",     "Or",
                         "Uminus",  "Not",      "Len",      "Ubnot"};

  unsigned int thisId = (*id)++;

  if (this->type == NT_Exp::Bool) {
    printf("\"%s %s #%u indexInTable %d\"", types[static_cast<int>(this->type)],
           (this->b) ? "true" : "false", thisId);
    return;
  }

  if (this->type == NT_Exp::Int) {
    printf("\"%s %d #%u indexInTable %d\"", types[static_cast<int>(this->type)],
           this->i, thisId);
    return;
  }

  if (this->type == NT_Exp::Float) {
    printf("\"%s %lf #%u indexInTable %d\"",
           types[static_cast<int>(this->type)], this->d, thisId);
    return;
  }

  if (this->type == NT_Exp::String) {
    printf("\"%s %s #%u indexInTable %d\"", types[static_cast<int>(this->type)],
           this->s->c_str(), thisId);
    return;
  }

  if (this->type == NT_Exp::Funcdef) {
    this->fDef->buildDot(id);
    return;
  }

  if (this->type == NT_Exp::Prefexp) {
    this->prefExp->buildDot(id);
    return;
  }

  if (this->type == NT_Exp::Tconstr) {
    this->tConstr->buildDot(id);
    return;
  }

  printf("\"%s #%u\"\n", types[static_cast<int>(this->type)], thisId);
  if (this->lExp != nullptr) {
    printf("\"%s #%u\" -> ", types[static_cast<int>(this->type)], thisId);
    this->lExp->buildDot(id);
    printf("\n");
  }

  if (this->rExp != nullptr) {
    printf("\"%s #%u\" -> ", types[static_cast<int>(this->type)], thisId);
    this->rExp->buildDot(id);
    printf("\n");
  }
}
