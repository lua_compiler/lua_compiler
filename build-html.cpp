#include "ast.h"

void buildHtml(VarTable *varTable) {
  printf("<h4>Таблица переменных</h4>\n");
  printf("<table>\n");
  printf("<thead>\n");
  printf("<tr>\n");
  printf("<th>Index</th>\n");
  printf("<th>Var name</th>\n");
  printf("</tr>\n");
  printf("</thead>\n");
  printf("<tbody>\n");
  for (std::map<std::string, unsigned int>::iterator i = varTable->begin();
       i != varTable->end(); ++i) {
    printf("<tr>\n");
    printf("<td> %d </td>\n", i->second);
    std::string sasVarName = std::string(i->first);
    size_t index = 0;
    while (true) {  // spizheno so stackoverflow
      index = sasVarName.find("<", index);

      if (index == std::string::npos) break;

      sasVarName.replace(index, 1, "&lt;");
    }
    index = 0;
    while (true) {  // spizheno so stackoverflow
      index = sasVarName.find(">", index);

      if (index == std::string::npos) break;

      sasVarName.replace(index, 1, "&gt;");
    }
    printf("<td> %s </td>\n", sasVarName.c_str());
    printf("</tr>\n");
  }
  printf("</tbody>\n");
  printf("</table>\n");
  printf("<br>\n");
}

void buildHtml(ConstTable *constTable) {
  printf("<h4>Таблица констант</h4>\n");
  printf("<table>\n");
  printf("<thead>\n");
  printf("<tr>\n");
  printf("<th>Number</th>\n");
  printf("<th>Type</th>\n");
  printf("<th>String value</th>\n");
  printf("<th>Int value 1</th>\n");
  printf("<th>Int value 2</th>\n");
  printf("<th>Float value</th>\n");
  printf("<th>Var name</th>\n");
  printf("<th>Class name</th>\n");
  printf("</tr>\n");
  printf("</thead>\n");
  printf("<tbody>\n");
  for (unsigned int i = 0; i < constTable->size(); ++i) {
    printf("<tr>\n");
    printf("<td> %d </td>\n", i);
    printf("<td> %s </td>\n", cteType(constTable->at(i)->type).c_str());
    printf("<td> %s </td>\n", constTable->at(i)->strValue->c_str());
    printf("<td> %d </td>\n", constTable->at(i)->intVal1);
    printf("<td> %d </td>\n", constTable->at(i)->intVal2);
    printf("<td> %f </td>\n", constTable->at(i)->flVal);

    std::string sasVarName = std::string(*constTable->at(i)->varName);
    size_t index = 0;
    while (true) {  // spizheno so stackoverflow
      index = sasVarName.find("<", index);

      if (index == std::string::npos) break;

      sasVarName.replace(index, 1, "&lt;");
    }
    index = 0;
    while (true) {  // spizheno so stackoverflow
      index = sasVarName.find(">", index);

      if (index == std::string::npos) break;

      sasVarName.replace(index, 1, "&gt;");
    }
    printf("<td> %s </td>\n", sasVarName.c_str());
    printf("<td> %s </td>\n", constTable->at(i)->className->c_str());
    printf("</tr>\n");
  }
  printf("</tbody>\n");
  printf("</table>\n");
  printf("<br>\n");
}

void Block::buildHtml() const {
  ::buildHtml(this->localVarTable);

  if (this->stlist != nullptr) {
    Stat *current;
    for (unsigned int i = 0; i < this->stlist->size(); ++i) {
      current = this->stlist->at(i);
      switch (this->stlist->at(i)->type) {
        case NT_Stat::FuncDecl:
        case NT_Stat::LocalFuncDecl:
          current->funcbody->buildHtml(this->stlist->at(i)->id);
          break;
        case NT_Stat::IfElse:
          current->ifelse->buildHtml();
          break;
        case NT_Stat::ForLoop:
          current->forloop->buildHtml();
          break;
        case NT_Stat::WhileLoop:
        case NT_Stat::RepUntilLoop:
          printf("<h3>(While... / Repeat...Until) Loop:</h3>\n");
          current->block->buildHtml();
      }
    }
    printf("<hr>\n");
  }
}

void FuncBody::buildHtml(std::string *funcName) const {
  printf("<h3> Функция %s: </h3>\n", funcName->c_str());

  ::buildHtml(this->newConstTable);

  this->block->buildHtml();
}

void IfElse::buildHtml() const {
  printf("<h3>If</h3>\n");
  switch (this->type) {
    case NT_IfElse::IfOnly:
      this->ifBlock->buildHtml();
      break;
    case NT_IfElse::IfElseif:
      this->ifBlock->buildHtml();
      this->elseif->buildHtml();
      break;
    case NT_IfElse::IfElse:
      this->ifBlock->buildHtml();
      this->elseBlock->buildHtml();
      break;
    case NT_IfElse::IfElseifElse:
      this->ifBlock->buildHtml();
      this->elseif->buildHtml();
      this->elseBlock->buildHtml();
      break;
  }
}

void ElseIf::buildHtml() const {
  printf("<h3>Else If</h3>\n");
  switch (this->type) {
    case NT_ElseIf::Single:
      this->block->buildHtml();
      break;
    case NT_ElseIf::Several:
      this->block->buildHtml();
      this->elseif->buildHtml();
      break;
  }
}

void ForLoop::buildHtml() const {
  printf("<h3>For Loop</h3>\n");
  this->block->buildHtml();
}

void Exp::buildHtml() const {
  if (this->type == NT_Exp::Funcdef) {
    this->fDef->buildHtml();
  }
}

void FunctionDef::buildHtml() const {
  this->funcbody->buildHtml(new std::string(
      "анонимная"));  // TODO name anon function and avoid memory leak
}
