#include "ast.h"

std::string cteType(constTableEntryType type) {
  std::string out;
  switch (type) {
    case constTableEntryType::utf8:
      out = std::string("UTF-8");
      break;
    case constTableEntryType::integer:
      out = std::string("Int");
      break;
    case constTableEntryType::floating:
      out = std::string("Float");
      break;
    case constTableEntryType::str:
      out = std::string("String");
      break;
    case constTableEntryType::nameAndType:
      out = std::string("NameAndType");
      break;
    case constTableEntryType::const_class:
      out = std::string("Class");
      break;
    case constTableEntryType::fieldref:
      out = std::string("FieldRef");
      break;
    case constTableEntryType::methodref:
      out = std::string("MethodRef");
      break;
  }
  return out;
}

int getFunctionsCount(std::string fn, ConstTable *table) {
  int count = 0;
  for (int i = 1; i < table->size(); i++) {
    if (table->at(i)->type == constTableEntryType::utf8 &&
        table->at(i)->strValue->find(fn) != std::string::npos) {
      count++;
    }
  }
  return count;
}

// looks up an entry in a constTable by a varName and returns its id
unsigned int findByVarName(std::string &id, ConstTable *constTable) {
  for (unsigned int i = 0; i < constTable->size(); i++) {
    ConstTableEntry *entry = constTable->at(i);
    if (entry->type == constTableEntryType::fieldref &&
        *(entry->varName) == id) {
      return i;
    }
  }
  return -1;
}

unsigned int findMethodref(std::string id, ConstTable *constTable) {
  for (unsigned int i = 0; i < constTable->size(); i++) {
    ConstTableEntry *entry = constTable->at(i);
    if (entry->type == constTableEntryType::methodref &&
        *(entry->varName) == id) {
      return i;
    }
  }
  return -1;
}

// looks up an entry in a constTable by a className and returns its id
unsigned int findByClassName(std::string id, ConstTable *constTable) {
  for (unsigned int i = 0; i < constTable->size(); i++) {
    ConstTableEntry *entry = constTable->at(i);
    if (*(entry->className) == id) {
      return i;
    }
  }
  return -1;
}

// looks up an id in a table and returns its index if found. Otherwise returns
// -1
unsigned int findVar(std::string *id, VarTable *localTable) {
  if (localTable->find(*id) != localTable->end()) {
    return localTable->find(*id)->second;
  }
  return -1;
}
// looks up an id in a table and returns its index if found. Otherwise inserts
// new id
unsigned int addVar(std::string *id, VarTable *localTable) {
  if (localTable->find(*id) != localTable->end()) {
    return localTable->find(*id)->second;
  }
  unsigned int tableSize = localTable->size();
  (*localTable)[*id] = tableSize;
  return ((*localTable)[*id]);
}

// inserts new id. If id already present, overwrites
unsigned int addVarForce(std::string *id, VarTable *localTable) {
  unsigned int tableSize = localTable->size() + 10;
  (*localTable)[*id] = tableSize;
  return ((*localTable)[*id]);
}

unsigned int findUtf(ConstTable *constTable, const std::string &str) {
  for (unsigned int i = 0; i < constTable->size(); i++) {
    ConstTableEntry *entry = constTable->at(i);
    if (entry->type == constTableEntryType::utf8 && *entry->strValue == str) {
      return i;
    }
  }
  return -1;
}

std::map<std::string, std::string> *generatePolyVarMethods() {
  std::map<std::string, std::string> *map =
      new std::map<std::string, std::string>();
  map->insert(
      std::pair<std::string, std::string>("plus", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("minus", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("mult", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("div", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("floorDiv", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("mod", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("exp", "(LPolyVar;)LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("unm", "()LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("band", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("bor", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("bxor", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("rsh", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("lsh", "(LPolyVar;)LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("ubnot", "()LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("eq", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("neq", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("lt", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("gt", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("le", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("ge", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("and", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("or", "(LPolyVar;)LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("not", "()LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("concat", "(LPolyVar;)LPolyVar;"));
  map->insert(
      std::pair<std::string, std::string>("invoke", "([LPolyVar;)LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("len", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("getTableField",
                                                  "(LPolyVar;)LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("setTableField",
                                                  "(LPolyVar;LPolyVar;)V"));
  map->insert(std::pair<std::string, std::string>("evaluate", "()I"));
  map->insert(std::pair<std::string, std::string>("inc", "(I)LPolyVar;"));

  map->insert(std::pair<std::string, std::string>("isNil", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("isBool", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("isInt", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("isFloat", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("isString", "()LPolyVar;"));
  map->insert(std::pair<std::string, std::string>("isTable", "()LPolyVar;"));

  map->insert(
      std::pair<std::string, std::string>("forCheck", "(LPolyVar;LPolyVar;)I"));
  map->insert(
      std::pair<std::string, std::string>("<init>S", "(Ljava/lang/String;)V"));
  map->insert(std::pair<std::string, std::string>("<init>I", "(I)V"));
  map->insert(std::pair<std::string, std::string>("<init>F", "(F)V"));
  map->insert(std::pair<std::string, std::string>("<init>Fn", "(LFunction;)V"));
  map->insert(std::pair<std::string, std::string>("<init>", "()V"));
  map->insert(std::pair<std::string, std::string>("<init>II", "(II)V"));
  map->insert(
      std::pair<std::string, std::string>("<init>PolyVar", "(LPolyVar;)V"));
  map->insert(std::pair<std::string, std::string>("<init>Z", "(Z)V"));
  return map;
}

std::map<std::string, std::string> *generatePolyVarFields() {
  std::map<std::string, std::string> *map =
      new std::map<std::string, std::string>();
  map->insert(
      std::pair<std::string, std::string>("strVal", "Ljava/lang/String;"));
  map->insert(std::pair<std::string, std::string>("intVal", "I"));
  map->insert(std::pair<std::string, std::string>("floatVal", "F"));
  map->insert(std::pair<std::string, std::string>("functionVal", "LFunction;"));
  map->insert(std::pair<std::string, std::string>("boolVal", "Z"));
  map->insert(std::pair<std::string, std::string>("tableVal", "LTable;"));

  return map;
}

void fillPolyVarFields(ConstTable *table,
                       std::map<std::string, std::string> *map) {
  int classId = findByClassName(std::string("PolyVar"), table);
  for (std::map<std::string, std::string>::const_iterator it = map->begin();
       it != map->end(); it++) {
    int nameId = table->size();
    table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                         new std::string(it->first)));
    int typeId = table->size();
    table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                         new std::string(it->second)));
    int nameAndTypeId = table->size();
    table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                         new std::string(""), nameId, typeId));
    ConstTableEntry *e = new ConstTableEntry(
        constTableEntryType::fieldref, new std::string(""), classId,
        nameAndTypeId,
        new std::string(
            it->first));  // Put method name in varName to find it later
    e->localField = false;
    table->push_back(e);
  }
}

void fillPolyVarMethods(ConstTable *table,
                        std::map<std::string, std::string> *map) {
  int classNameId = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("PolyVar")));
  int classId = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(""), classNameId, -1,
      new std::string(""), -1, new std::string("PolyVar")));
  for (std::map<std::string, std::string>::const_iterator it = map->begin();
       it != map->end(); it++) {
    int nameId = table->size();
    if (it->first.find("<init>") != std::string::npos) {
      table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                           new std::string("<init>")));
    } else {
      table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                           new std::string(it->first)));
    }
    int typeId = findUtf(table, it->second);
    if (typeId == -1) {
      typeId = table->size();
      table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                           new std::string(it->second)));
    }
    int nameAndTypeId = table->size();
    table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                         new std::string(""), nameId, typeId));
    table->push_back(new ConstTableEntry(
        constTableEntryType::methodref, new std::string(""), classId,
        nameAndTypeId,
        new std::string(
            it->first)));  // Put method name in varName to find it later
  }
}

void Field::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                        VarTable *globalVarTable) {
  int utfIndex = -1;
  switch (this->type) {
    case NT_Field::IndexSet:
      this->lExp->buildTables(globalConstTable, constTable, globalVarTable);
      this->rExp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Field::IdSet:
      utfIndex = constTable->size();
      constTable->push_back(
          new ConstTableEntry(constTableEntryType::utf8, this->id, -1, -1));
      this->indexInTable = constTable->size();
      constTable->push_back(new ConstTableEntry(constTableEntryType::str,
                                                new std::string(""), utfIndex,
                                                -1, this->id));
      this->rExp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Field::ExpSet:
      this->lExp->buildTables(globalConstTable, constTable, globalVarTable);
  }
}

void RetStat::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                          VarTable *globalVarTable) {
  for (unsigned int i = 0; i < this->explist->size(); i++) {
    this->explist->at(i)->buildTables(globalConstTable, constTable,
                                      globalVarTable);
  }
}

void Block::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                        VarTable *globalVarTable) {
  this->localConstTable = constTable;
  this->globalConstTable = globalConstTable;
  this->localVarTable = globalVarTable;
  if (this->stlist != nullptr) {
    for (unsigned int i = 0; i < this->stlist->size(); i++) {
      this->stlist->at(i)->isInLoop = this->isInLoop;
      this->stlist->at(i)->inMainChunk = this->inMainChunk;
      this->stlist->at(i)->buildTables(
          this->globalConstTable, this->localConstTable, this->localVarTable);
    }
  }
  if (this->retstat != nullptr) {
    this->retstat->buildTables(globalConstTable, constTable, globalVarTable);
  }
}

void TableConstructor::buildTables(ConstTable *globalConstTable,
                                   ConstTable *constTable,
                                   VarTable *globalVarTable) {
  if (this->fieldlist != nullptr) {
    for (unsigned int i = 0; i < fieldlist->size(); i++) {
      this->fieldlist->at(i)->buildTables(globalConstTable, constTable,
                                          globalVarTable);
    }
  }
}

void Args::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                       VarTable *globalVarTable) {
  if (this->explist != nullptr) {
    for (int i = 0; i < this->explist->size(); i++) {
      this->explist->at(i)->buildTables(globalConstTable, constTable,
                                        globalVarTable);
    }
  }
}

void PrefixExp::buildTables(ConstTable *globalConstTable,
                            ConstTable *constTable, VarTable *globalVarTable) {
  switch (this->type) {
    case NT_PrefixExp::Var:
      this->var->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_PrefixExp::FuncCall:
      this->fCall->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_PrefixExp::Exp:
      this->exp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
  }
}

void FunctionCall::buildTables(ConstTable *globalConstTable,
                               ConstTable *constTable,
                               VarTable *globalVarTable) {
  this->prefixexp->buildTables(globalConstTable, constTable, globalVarTable);
  this->args->buildTables(globalConstTable, constTable, globalVarTable);
}

void Var::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                      VarTable *globalVarTable) {
  int nameAndTypeId = -1;
  int descriptorId = -1;
  int nameId = -1;
  switch (this->type) {
    case NT_Var::Id:
      // if var is already in vartable
      if (findVar(this->id, globalVarTable) != -1) {
        this->indexInTable = addVar(this->id, globalVarTable);
      } else {
        this->indexInTable = findByVarName(*(this->id), globalConstTable);
        // if var is already in constTable and local constTable is not global
        if (this->indexInTable != -1) {
          if (globalConstTable != constTable) {
            // Add fieldRef to local constTable
            nameId = constTable->size();
            constTable->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                                      this->id, -1, -1));
            descriptorId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::utf8,
                                    new std::string("LPolyVar;"), -1, -1));
            nameAndTypeId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::nameAndType,
                                    new std::string(""), nameId, descriptorId));
            this->indexInTable = constTable->size();
            ConstTableEntry *e = new ConstTableEntry(
                constTableEntryType::fieldref, new std::string(""),
                findByClassName(*(new std::string(MAIN_CLASS_NAME)),
                                constTable),
                nameAndTypeId, this->id);
            e->localField = false;
            constTable->push_back(e);
          }
        }
        // else add to constTable
        else {
          // Add to global constTable
          nameId = globalConstTable->size();
          globalConstTable->push_back(
              new ConstTableEntry(constTableEntryType::utf8, this->id, -1, -1));
          descriptorId = globalConstTable->size();
          globalConstTable->push_back(new ConstTableEntry(
              constTableEntryType::utf8, new std::string("LPolyVar;"), -1, -1));
          nameAndTypeId = globalConstTable->size();
          globalConstTable->push_back(
              new ConstTableEntry(constTableEntryType::nameAndType,
                                  new std::string(""), nameId, descriptorId));
          this->indexInTable = globalConstTable->size();
          globalConstTable->push_back(new ConstTableEntry(
              constTableEntryType::fieldref, new std::string(""),
              findByClassName(*(new std::string(MAIN_CLASS_NAME)), constTable),
              nameAndTypeId, this->id));
          if (globalConstTable != constTable) {
            // Add to local constTable
            nameId = constTable->size();
            constTable->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                                      this->id, -1, -1));
            descriptorId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::utf8,
                                    new std::string("LPolyVar;"), -1, -1));
            nameAndTypeId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::nameAndType,
                                    new std::string(""), nameId, descriptorId));
            this->indexInTable = constTable->size();
            ConstTableEntry *e = new ConstTableEntry(
                constTableEntryType::fieldref, new std::string(""),
                findByClassName(*(new std::string(MAIN_CLASS_NAME)),
                                constTable),
                nameAndTypeId, this->id);
            e->localField = false;
            constTable->push_back(e);
          }
        }
      }
      break;
    case NT_Var::TableField:
      this->prefixexp->buildTables(globalConstTable, constTable,
                                   globalVarTable);
      if (this->exp != nullptr) {
        this->exp->buildTables(globalConstTable, constTable, globalVarTable);
      } else {
        // Add to global constTable
        nameId = globalConstTable->size();
        globalConstTable->push_back(
            new ConstTableEntry(constTableEntryType::utf8, this->id, -1, -1));
        descriptorId = globalConstTable->size();
        globalConstTable->push_back(new ConstTableEntry(
            constTableEntryType::utf8, new std::string("LPolyVar;"), -1, -1));
        nameAndTypeId = globalConstTable->size();
        globalConstTable->push_back(
            new ConstTableEntry(constTableEntryType::nameAndType,
                                new std::string(""), nameId, descriptorId));
        this->indexInTable = globalConstTable->size();
        globalConstTable->push_back(new ConstTableEntry(
            constTableEntryType::fieldref, new std::string(""),
            findByClassName(*(new std::string(MAIN_CLASS_NAME)), constTable),
            nameAndTypeId, this->id));
        if (globalConstTable != constTable) {
          // Add to local constTable
          nameId = constTable->size();
          constTable->push_back(
              new ConstTableEntry(constTableEntryType::utf8, this->id, -1, -1));
          descriptorId = constTable->size();
          constTable->push_back(new ConstTableEntry(
              constTableEntryType::utf8, new std::string("LPolyVar;"), -1, -1));
          nameAndTypeId = constTable->size();
          constTable->push_back(
              new ConstTableEntry(constTableEntryType::nameAndType,
                                  new std::string(""), nameId, descriptorId));
          this->indexInTable = constTable->size();
          ConstTableEntry *e = new ConstTableEntry(
              constTableEntryType::fieldref, new std::string(""),
              findByClassName(*(new std::string(MAIN_CLASS_NAME)), constTable),
              nameAndTypeId, this->id);
          e->localField = false;
          constTable->push_back(e);
        }
      }
      break;
  }
}

void ElseIf::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                         VarTable *globalVarTable) {
  VarTable *localVarTable = new VarTable(*globalVarTable);
  if (this->elseif != nullptr) {
    this->elseif->isInLoop = this->isInLoop;
    this->elseif->inMainChunk = this->inMainChunk;
    this->elseif->buildTables(globalConstTable, constTable, localVarTable);
  }
  this->exp->buildTables(globalConstTable, constTable, localVarTable);
  this->block->isInLoop = this->isInLoop;
  this->block->inMainChunk = this->inMainChunk;
  this->block->buildTables(globalConstTable, constTable, localVarTable);
}

void IfElse::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                         VarTable *globalVarTable) {
  VarTable *localVarTable = new VarTable(*globalVarTable);
  this->exp->buildTables(globalConstTable, constTable, localVarTable);
  this->ifBlock->isInLoop = this->isInLoop;
  this->ifBlock->inMainChunk = this->inMainChunk;
  this->ifBlock->buildTables(globalConstTable, constTable, localVarTable);
  if (this->elseif != nullptr) {
    this->elseif->isInLoop = this->isInLoop;
    this->elseif->inMainChunk = this->inMainChunk;
    this->elseif->buildTables(globalConstTable, constTable, localVarTable);
  }
  if (this->elseBlock != nullptr) {
    this->elseBlock->isInLoop = this->isInLoop;
    this->elseBlock->inMainChunk = this->inMainChunk;
    this->elseBlock->buildTables(globalConstTable, constTable, localVarTable);
  }
}

void ForLoop::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                          VarTable *globalVarTable) {
  VarTable *localVarTable = new VarTable(*globalVarTable);
  addVarForce(this->id, localVarTable);
  this->initExp->buildTables(globalConstTable, constTable, localVarTable);
  this->endExp->buildTables(globalConstTable, constTable, localVarTable);
  this->stepExp->buildTables(globalConstTable, constTable, localVarTable);
  this->block->buildTables(globalConstTable, constTable, localVarTable);
}

void VarDecl::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                          VarTable *globalVarTable) {
  // NOTE all vars defined here will be LOCAL
  for (unsigned int i = 0; i < this->namelist->size(); i++) {
    addVarForce(this->namelist->at(i), globalVarTable);
  }
  if (this->type == NT_VarDecl::DeclDef) {
    for (unsigned int i = 0; i < this->explist->size(); i++) {
      this->explist->at(i)->buildTables(globalConstTable, constTable,
                                        globalVarTable);
    }
  }
}

void ParList::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                          VarTable *globalVarTable) {
  for (int i = 0; i < this->namelist->size(); i++) {
    addVar(this->namelist->at(i), globalVarTable);
  }
}

void FuncBody::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                           VarTable *globalVarTable) {
  // put classref to global constTable
  int n = getFunctionsCount("Anonymous_function_#", globalConstTable);
  this->className = new std::string("Anonymous_function_#" + std::to_string(n));
  int nameIndex = globalConstTable->size();
  globalConstTable->push_back(new ConstTableEntry(
      constTableEntryType::utf8,
      new std::string("Anonymous_function_#" + std::to_string(n))));
  this->classIndex = globalConstTable->size();
  globalConstTable->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(""), nameIndex, -1,
      new std::string(""), -1,
      new std::string("Anonymous_function_#" + std::to_string(n))));

  if (globalConstTable != constTable) {
    // put classref to local constTable
    nameIndex = constTable->size();
    constTable->push_back(new ConstTableEntry(
        constTableEntryType::utf8,
        new std::string("Anonymous_function_#" + std::to_string(n))));
    this->classIndex = constTable->size();
    constTable->push_back(new ConstTableEntry(
        constTableEntryType::const_class, new std::string(""), nameIndex, -1,
        new std::string(""), -1,
        new std::string("Anonymous_function_#" + std::to_string(n))));
  }

  nameIndex = constTable->size();
  constTable->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                            new std::string("<init>")));
  int typeIndex = constTable->size();
  constTable->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("()V")));
  int nameAndTypeIndex = constTable->size();
  constTable->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                            new std::string(""), nameIndex,
                                            typeIndex));
  this->constructorIndex = constTable->size();
  constTable->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""), this->classIndex,
      nameAndTypeIndex, new std::string(*(this->className) + "<init>")));

  ConstTable *table = new ConstTable();
  // init classTable
  table->push_back(
      new ConstTableEntry(constTableEntryType::dummy, new std::string("")));
  // main class ref
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string(MAIN_CLASS_NAME)));
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string(MAIN_CLASS_NAME)));
  // this class ref
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, this->className));
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, this->className));
  // parent class ref
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("Function")));
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string("Function")));
  // parent constructor methodref
  nameIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("<init>")));
  typeIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("()V")));
  nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""),
      findByClassName(*(new std::string("Function")), table), nameAndTypeIndex,
      new std::string("Function<init>")));

  // RTL

  fillPolyVarMethods(table, generatePolyVarMethods());
  fillPolyVarFields(table, generatePolyVarFields());

  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("Print")));
  int printClassIndex = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string("Print")));
  // print()
  nameIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("print")));
  typeIndex = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::utf8, new std::string("([LPolyVar;)LPolyVar;")));
  nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""), printClassIndex,
      nameAndTypeIndex, new std::string("print")));
  // read()
  nameIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("read")));
  typeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("()LPolyVar;")));
  nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""), printClassIndex,
      nameAndTypeIndex, new std::string("read")));
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("Code")));
  // end init classTable

  VarTable *newVarTable = new VarTable();
  (*newVarTable)[std::string("self")] = 0;
  (*newVarTable)[std::string("<reserved for PolyVar array>")] = 1;
  this->newConstTable = table;
  this->parlist->buildTables(globalConstTable, table, newVarTable);
  this->block->inMainChunk = false;
  this->block->buildTables(globalConstTable, table, newVarTable);
}

void FunctionDef::buildTables(ConstTable *globalConstTable,
                              ConstTable *constTable,
                              VarTable *globalVarTable) {
  // NOTE on code generating: create new class derived from FUNCTION_CLASS (how
  // to name it?), build invoke() method, put obj on stack
  this->funcbody->buildTables(globalConstTable, constTable,
                              globalVarTable);  // TODO check if tables are ok.
}

void Stat::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                       VarTable *globalVarTable) {
  int descriptorId;
  int nameId;
  int nameAndTypeId;
  ConstTable *table;
  VarTable *newVarTable;

  switch (this->type) {
    case NT_Stat::VarDef:  // TODO check, add constTable entries
      for (int i = 0; i < this->varlist->size(); i++) {
        this->varlist->at(i)->buildTables(globalConstTable, constTable,
                                          globalVarTable);
        if (this->explist->size() <= i) {  // align explist to fit varlist
          this->explist->push_back(new Exp());
        }
        this->explist->at(i)->buildTables(globalConstTable, constTable,
                                          globalVarTable);
      }
      break;
    case NT_Stat::FuncCallOnly:
      if (this->prefixexp->type != NT_PrefixExp::FuncCall) {
        // NOTE should be handled on parsing
      } else {
        this->prefixexp->buildTables(globalConstTable, constTable,
                                     globalVarTable);
      }
      break;
    case NT_Stat::Label:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::Break:
      if (!this->isInLoop) {
        throw "syntax error: break not inside a loop";
      }
      break;
    case NT_Stat::Goto:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::Block:
      this->block->inMainChunk = this->inMainChunk;
      this->block->isInLoop = this->isInLoop;
      this->block->buildTables(globalConstTable, constTable,
                               new VarTable(*globalVarTable));
      break;
    case NT_Stat::WhileLoop:
      this->block->isInLoop = true;
      this->block->inMainChunk = this->inMainChunk;
      this->exp->buildTables(globalConstTable, constTable, globalVarTable);
      this->block->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::RepUntilLoop:
      this->block->inMainChunk = this->inMainChunk;
      this->block->isInLoop = true;
      this->exp->buildTables(globalConstTable, constTable, globalVarTable);
      this->block->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::IfElse:
      this->ifelse->inMainChunk = this->inMainChunk;
      this->ifelse->isInLoop = this->isInLoop;
      this->ifelse->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::ForLoop:
      this->forloop->block->inMainChunk = this->inMainChunk;
      this->forloop->block->isInLoop = true;
      this->forloop->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::ForEach:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::FuncDecl:
      // if var is already in vartable
      if (findVar(this->id, globalVarTable) != -1) {
        this->indexInTable = addVar(this->id, globalVarTable);
      } else {
        findByVarName(*(this->id), globalConstTable);
        // if var is already in constTable
        if (this->indexInTable != -1) {
          if (globalConstTable != constTable) {
            // Add fieldRef to local constTable
            nameId = constTable->size();
            constTable->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                                      this->id, -1, -1));
            descriptorId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::utf8,
                                    new std::string("LPolyVar;"), -1, -1));
            nameAndTypeId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::nameAndType,
                                    new std::string(""), nameId, descriptorId));
            this->indexInTable = constTable->size();
            ConstTableEntry *e = new ConstTableEntry(
                constTableEntryType::fieldref, new std::string(""),
                findByClassName(*(new std::string(MAIN_CLASS_NAME)),
                                constTable),
                nameAndTypeId, this->id);
            e->localField = false;
            constTable->push_back(e);
          }
        }
        // else add to constTable
        else {
          // Add to global constTable
          nameId = globalConstTable->size();
          globalConstTable->push_back(
              new ConstTableEntry(constTableEntryType::utf8, this->id, -1, -1));
          descriptorId = globalConstTable->size();
          globalConstTable->push_back(new ConstTableEntry(
              constTableEntryType::utf8, new std::string("LPolyVar;"), -1, -1));
          nameAndTypeId = globalConstTable->size();
          globalConstTable->push_back(
              new ConstTableEntry(constTableEntryType::nameAndType,
                                  new std::string(""), nameId, descriptorId));
          this->indexInTable = globalConstTable->size();
          globalConstTable->push_back(new ConstTableEntry(
              constTableEntryType::fieldref, new std::string(""),
              findByClassName(*(new std::string(MAIN_CLASS_NAME)), constTable),
              nameAndTypeId, this->id));
          if (globalConstTable != constTable) {
            // Add to local constTable
            nameId = constTable->size();
            constTable->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                                      this->id, -1, -1));
            descriptorId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::utf8,
                                    new std::string("LPolyVar;"), -1, -1));
            nameAndTypeId = constTable->size();
            constTable->push_back(
                new ConstTableEntry(constTableEntryType::nameAndType,
                                    new std::string(""), nameId, descriptorId));
            this->indexInTable = constTable->size();
            ConstTableEntry *e = new ConstTableEntry(
                constTableEntryType::fieldref, new std::string(""),
                findByClassName(*(new std::string(MAIN_CLASS_NAME)),
                                constTable),
                nameAndTypeId, this->id);
            e->localField = false;
            constTable->push_back(e);
          }
        }
      }

      this->funcbody->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::LocalFuncDecl:
      this->indexInTable = addVarForce(this->id, globalVarTable);
      this->funcbody->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Stat::VarDecl:
      // NOTE working with LOCAL vars here
      this->vardecl->buildTables(globalConstTable, constTable, globalVarTable);
      break;
  }
}

void Exp::buildTables(ConstTable *globalConstTable, ConstTable *constTable,
                      VarTable *globalVarTable) {
  int utfIndex = -1;
  switch (this->type) {
    case NT_Exp::Int:
      this->indexInTable = constTable->size();
      constTable->push_back(new ConstTableEntry(
          constTableEntryType::integer, new std::string(""), this->i, -1));
      break;
    case NT_Exp::Float:
      this->indexInTable = constTable->size();
      constTable->push_back(new ConstTableEntry(constTableEntryType::floating,
                                                new std::string(""), -1, -1,
                                                new std::string(""), this->d));
      break;
    case NT_Exp::String:
      utfIndex = constTable->size();
      constTable->push_back(
          new ConstTableEntry(constTableEntryType::utf8, this->s, -1, -1));
      this->indexInTable = constTable->size();
      constTable->push_back(new ConstTableEntry(constTableEntryType::str,
                                                new std::string(""), utfIndex,
                                                -1, this->s));
      break;
    case NT_Exp::Funcdef:
      this->fDef->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Exp::Prefexp:
      this->prefExp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Exp::Tconstr:
      this->tConstr->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Exp::Plus:
    case NT_Exp::Minus:
    case NT_Exp::Multiply:
    case NT_Exp::Div:
    case NT_Exp::Divint:
    case NT_Exp::Pow:
    case NT_Exp::Mod:
    case NT_Exp::Band:
    case NT_Exp::Xor:
    case NT_Exp::Bor:
    case NT_Exp::Shr:
    case NT_Exp::Shl:
    case NT_Exp::Concat:
    case NT_Exp::Lt:
    case NT_Exp::Lte:
    case NT_Exp::Gt:
    case NT_Exp::Gte:
    case NT_Exp::Eq:
    case NT_Exp::Neq:
    case NT_Exp::And:
    case NT_Exp::Or:
      this->lExp->buildTables(globalConstTable, constTable, globalVarTable);
      this->rExp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
    case NT_Exp::Uminus:
    case NT_Exp::Not:
    case NT_Exp::Len:
    case NT_Exp::Ubnot:
      this->lExp->buildTables(globalConstTable, constTable, globalVarTable);
      break;
  }
}
