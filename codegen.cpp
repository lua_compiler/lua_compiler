#include "codegen.h"

unsigned short reverse2(unsigned short num) {
  union {
    unsigned short n;
    unsigned char b[2];
  } u2;
  u2.n = num;
  std::reverse(std::begin(u2.b), std::end(u2.b));
  return u2.n;
}

short reverse2s(short num) {
  union {
    short n;
    unsigned char b[2];
  } u2;
  u2.n = num;
  std::reverse(std::begin(u2.b), std::end(u2.b));
  return u2.n;
}

long reverse4s(long num) {
  union {
    long n;
    unsigned char b[4];
  } u4;
  u4.n = num;
  std::reverse(std::begin(u4.b), std::end(u4.b));
  return u4.n;
}

unsigned long reverse4(unsigned long num) {
  union {
    unsigned long n;
    unsigned char b[4];
  } u4;
  u4.n = num;
  std::reverse(std::begin(u4.b), std::end(u4.b));
  return u4.n;
}

float reverse4f(float num) {
  union {
    float n;
    unsigned char b[4];
  } u4;
  u4.n = num;
  std::reverse(std::begin(u4.b), std::end(u4.b));
  return u4.n;
}

int write(std::vector<char> *v, int data) {
  const char *arr = reinterpret_cast<const char *>(&data);
  std::copy(arr, &arr[4], std::back_inserter(*v));
  return 4;
}

int write(std::vector<char> *v, unsigned int data) {
  const char *arr = reinterpret_cast<const char *>(&data);
  std::copy(arr, &arr[4], std::back_inserter(*v));
  return 4;
}

void write(std::vector<char> *v, short data, unsigned int index) {
  const char *arr = reinterpret_cast<const char *>(&data);
  (*v)[index] = arr[0];
  (*v)[index + 1] = arr[1];
}

void write(std::vector<char> *v, unsigned short data, unsigned int index) {
  const char *arr = reinterpret_cast<const char *>(&data);
  (*v)[index] = arr[0];
  (*v)[index + 1] = arr[1];
}

void write(std::vector<char> *v, int data, unsigned int index) {
  const char *arr = reinterpret_cast<const char *>(&data);
  (*v)[index] = arr[0];
  (*v)[index + 1] = arr[1];
  (*v)[index + 2] = arr[2];
  (*v)[index + 3] = arr[3];
}

void write(std::vector<char> *v, unsigned int data, unsigned int index) {
  const char *arr = reinterpret_cast<const char *>(&data);
  (*v)[index] = arr[0];
  (*v)[index + 1] = arr[1];
  (*v)[index + 2] = arr[2];
  (*v)[index + 3] = arr[3];
}

int write(std::vector<char> *v, short data) {
  const char *arr = reinterpret_cast<const char *>(&data);
  std::copy(arr, &arr[2], std::back_inserter(*v));
  return 2;
}

int write(std::vector<char> *v, unsigned short data) {
  const char *arr = reinterpret_cast<const char *>(&data);
  std::copy(arr, &arr[2], std::back_inserter(*v));
  return 2;
}

int write(std::vector<char> *v, char data) {
  const char *arr = reinterpret_cast<const char *>(&data);
  v->push_back(data);
  return 1;
}

void ConstTableEntry::writeToClassFile(std::ofstream &out) {
  out.write(reinterpret_cast<const char *>(&type), 1);
  if (this->type == constTableEntryType::integer) {
    long tmp4 = reverse4s(intVal1);
    out.write(reinterpret_cast<const char *>(&tmp4), 4);
    return;
  }
  if (this->type == constTableEntryType::floating) {
    float tmp4 = reverse4f(flVal);
    out.write(reinterpret_cast<const char *>(&tmp4), 4);
    return;
  }
  if (this->type == constTableEntryType::utf8) {
    unsigned short tmp2 = reverse2(this->strValue->size());
    out.write(reinterpret_cast<const char *>(&tmp2), 2);
    out.write(strValue->c_str(), strValue->size());
  }
  if (intVal1 > -1) {
    unsigned short tmp2 = reverse2(intVal1);
    out.write(reinterpret_cast<const char *>(&tmp2), 2);
  }
  if (intVal2 > -1) {
    unsigned short tmp2 = reverse2(intVal2);
    out.write(reinterpret_cast<const char *>(&tmp2), 2);
  }
}

void Block::generateBreakAddress(std::vector<char> *code) {
  for (int i = 0; i < this->stlist->size(); ++i) {
    if (this->stlist->at(i)->type == NT_Stat::Block) {
      this->stlist->at(i)->block->generateBreakAddress(code);
    } else if (this->stlist->at(i)->type == NT_Stat::IfElse) {
      this->stlist->at(i)->ifelse->ifBlock->generateBreakAddress(code);
      if (this->stlist->at(i)->ifelse->type == NT_IfElse::IfElseif ||
          this->stlist->at(i)->ifelse->type == NT_IfElse::IfElseifElse) {
        this->stlist->at(i)->ifelse->elseif->generateBreakAddress(code);
      }
      if (this->stlist->at(i)->ifelse->type == NT_IfElse::IfElse ||
          this->stlist->at(i)->ifelse->type == NT_IfElse::IfElseifElse) {
        this->stlist->at(i)->ifelse->elseBlock->generateBreakAddress(code);
      }

    } else if (this->stlist->at(i)->type == NT_Stat::Break) {
      write(code,
            reverse2(code->size() - this->stlist->at(i)->breakAddress + 1),
            this->stlist->at(i)->breakAddress);  // rewrite ifeq offset
    }
  }
}

void ElseIf::generateBreakAddress(std::vector<char> *code) {
  if (this->type == NT_ElseIf::Several) {
    this->block->generateBreakAddress(code);
    this->elseif->generateBreakAddress(code);
  } else {
    this->block->generateBreakAddress(code);
  }
}

int Block::generateCode(std::vector<char> *code) {
  int len = 0;
  if (this->stlist != nullptr) {
    for (int i = 0; i < this->stlist->size(); i++) {
      len += this->stlist->at(i)->generateCode(code, this->localConstTable,
                                               this->localVarTable);
    }
  }
  if (this->retstat != nullptr) {
    if (this->inMainChunk) {
      len += write(code, JBC_RETURN);
    } else {
      len += this->retstat->generateCode(code, this->localConstTable,
                                         this->localVarTable);
    }
  }
  return len;
}

int TableConstructor::generateCode(std::vector<char> *code, ConstTable *table,
                                   VarTable *varTable) {
  int len = 0;
  len += write(code, JBC_NEW);
  len += write(code, reverse2(findByClassName(std::string("PolyVar"), table)));
  len += write(code, DUP);
  len += write(code, ICONST_0);
  len += write(code, ICONST_0);
  len += write(code, INVOKESPECIAL);
  len += write(code, reverse2(findMethodref("<init>II", table)));
  for (int i = 0; i < this->fieldlist->size(); i++) {
    switch (this->fieldlist->at(i)->type) {
      case NT_Field::IndexSet:
        len += write(code, DUP);
        this->fieldlist->at(i)->lExp->generateCode(code, table, varTable);
        this->fieldlist->at(i)->rExp->generateCode(code, table, varTable);
        len += write(code, INVOKEVIRTUAL);
        len += write(code, reverse2(findMethodref("setTableField", table)));
        break;
      case NT_Field::IdSet:
        len += write(code, DUP);
        len += write(code, JBC_NEW);
        len += write(code, reverse2(findByClassName("PolyVar", table)));
        len += write(code, DUP);
        len += write(code, LDC_W);
        len += write(code, reverse2(this->fieldlist->at(i)->indexInTable));
        len += write(code, INVOKESPECIAL);
        len += write(code, reverse2(findMethodref("<init>S", table)));
        this->fieldlist->at(i)->rExp->generateCode(code, table, varTable);
        len += write(code, INVOKEVIRTUAL);
        len += write(code, reverse2(findMethodref("setTableField", table)));
        break;
      case NT_Field::ExpSet:
        len += write(code, DUP);
        len += write(code, JBC_NEW);
        len += write(code, reverse2(findByClassName("PolyVar", table)));
        len += write(code, DUP);
        len += write(code, SIPUSH);
        len +=
            write(code,
                  reverse2s(i + 1));  // WARNING a={0,1,2...255,256} delaet sas
        len += write(code, INVOKESPECIAL);
        len += write(code, reverse2(findMethodref("<init>I", table)));
        this->fieldlist->at(i)->lExp->generateCode(code, table, varTable);
        len += write(code, INVOKEVIRTUAL);
        len += write(code, reverse2(findMethodref("setTableField", table)));
        break;
    }
  }
  return len;
}

int PrefixExp::generateCode(std::vector<char> *code, ConstTable *table,
                            VarTable *varTable) {
  int len = 0;
  switch (this->type) {
    case NT_PrefixExp::Var:
      len += this->var->generateCode(code, table, varTable);
      break;
    case NT_PrefixExp::FuncCall:
      len += this->fCall->generateCode(code, table, varTable);
      break;
    case NT_PrefixExp::Exp:
      len += this->exp->generateCode(code, table, varTable);
      break;
  }
  return len;
}

int Var::generateCode(std::vector<char> *code, ConstTable *table,
                      VarTable *varTable) {
  int len = 0;
  if (this->type == NT_Var::Id) {
    int indexInLocalTable = findVar(this->id, varTable);
    if (indexInLocalTable != -1) {
      len += write(code, ALOAD);
      len += write(code, (char)(indexInLocalTable));
    } else {
      len += write(code, GETSTATIC);
      len += write(code, reverse2(findByVarName(*(this->id), table)));
    }
  } else {
    len += this->prefixexp->generateCode(code, table, varTable);
    len += this->exp->generateCode(code, table, varTable);
    len += write(code, INVOKEVIRTUAL);
    len += write(code, reverse2(findMethodref("getTableField", table)));
  }
  return len;
}

int ElseIf::generateCode(std::vector<char> *code, ConstTable *table,
                         std::vector<int> &gotos, VarTable *varTable) {
  int len = 0;

  len += this->exp->generateCode(code, table, varTable);

  len += write(code, INVOKEVIRTUAL);
  len += write(code, reverse2(findMethodref("evaluate", table)));

  len += write(code, IFEQ);
  int elseIfIndex = code->size();
  len += write(code, reverse2s(0));  // set fictive ifeq offset

  len += this->block->generateCode(code);

  len += write(code, JBC_GOTO);
  gotos.push_back(code->size());
  len += write(code, reverse2s(0));  // set fictive goto offset

  write(code, reverse2s(code->size() - elseIfIndex + 1),
        elseIfIndex);  // rewrite ifeq offset

  switch (this->type) {
    case NT_ElseIf::Single:
      break;
    case NT_ElseIf::Several:
      len += this->elseif->generateCode(code, table, gotos, varTable);
      break;
  }

  return len;
}

int IfElse::generateCode(std::vector<char> *code, ConstTable *table,
                         VarTable *varTable) {
  int len = 0;
  std::vector<int> gotos = std::vector<int>();

  len += this->exp->generateCode(code, table, varTable);

  len += write(code, INVOKEVIRTUAL);
  len += write(code, reverse2(findMethodref("evaluate", table)));

  len += write(code, IFEQ);
  int ifIndex = code->size();
  len += write(code, reverse2s(0));  // set fictive ifeq offset

  len += this->ifBlock->generateCode(code);

  len += write(code, JBC_GOTO);
  if (this->type == NT_IfElse::IfOnly &&
      this->ifBlock->type == NT_Block::Empty) {
    len += write(code, reverse2(3));  // HACK if only one if and it has empty
    // block -> jump hard 3 bytes down (goto x1 byte and offset x2 bytes)

  } else {
    gotos.push_back(code->size());
    len += write(code, reverse2(0));  // set fictive goto offset
  }

  write(code, reverse2s(code->size() - ifIndex + 1),
        ifIndex);  // rewrite ifeq offset

  switch (this->type) {
    case NT_IfElse::IfOnly:

      for (int i = 0; i < gotos.size(); ++i) {
        write(code, reverse2s(code->size() - gotos.at(i) + 1),
              gotos.at(i));  // rewrite goto offset
      }

      return len;
    case NT_IfElse::IfElseif:
      len += this->elseif->generateCode(code, table, gotos, varTable);
      break;
    case NT_IfElse::IfElse:
      len += this->elseBlock->generateCode(code);
      break;
    case NT_IfElse::IfElseifElse:
      len += this->elseif->generateCode(code, table, gotos, varTable);
      len += this->elseBlock->generateCode(code);
      break;
  }

  for (int i = 0; i < gotos.size(); ++i) {
    write(code, reverse2s(code->size() - gotos.at(i) + 1),
          gotos.at(i));  // rewrite goto offset
  }

  return len;
}

int ForLoop::generateCode(std::vector<char> *code, ConstTable *constTable,
                          VarTable *varTable) {
  int len = 0;
  int gotoIndex;
  int blockIndex;
  int indexInLocalTable;
  indexInLocalTable = findVar(this->id, this->block->localVarTable);

  len += write(code, ALOAD);  // put init var on stack
  len += write(code, (char)indexInLocalTable);

  len += this->initExp->generateCode(
      code, constTable,
      this->block->localVarTable);  // generate init exp

  len += write(code, INVOKESPECIAL);
  len += write(code, reverse2(findMethodref("<init>PolyVar", constTable)));

  len += write(code, JBC_GOTO);  // jump to validation
  gotoIndex = code->size();
  len += write(code, reverse2s(0));

  blockIndex = code->size();  // remember block index
  len += this->block->generateCode(code);

  len += write(code, ALOAD);  // i = i + step
  len += write(code, (char)(indexInLocalTable));

  len +=
      this->stepExp->generateCode(code, constTable, this->block->localVarTable);

  len += write(code, INVOKEVIRTUAL);
  len += write(code, reverse2(findMethodref("plus", constTable)));

  len += write(code, ASTORE);
  len += write(code, (char)(indexInLocalTable));

  write(code, reverse2(code->size() - gotoIndex + 1),
        gotoIndex);  // rewrite fictive goto offfset

  len += write(code, ALOAD);
  len += write(code, (char)(indexInLocalTable));
  len +=
      this->endExp->generateCode(code, constTable, this->block->localVarTable);
  len +=
      this->stepExp->generateCode(code, constTable, this->block->localVarTable);
  len += write(code, INVOKEVIRTUAL);
  len += write(code, reverse2(findMethodref("forCheck", constTable)));

  len += write(code, IFNE);
  len += write(code, reverse2s(blockIndex - code->size() + 1));

  this->block->generateBreakAddress(code);
  return len;
}

int FunctionCall::generateCode(std::vector<char> *code, ConstTable *table,
                               VarTable *varTable) {
  int len = 0;
  if (this->prefixexp->type == NT_PrefixExp::Var &&
      this->prefixexp->var->type == NT_Var::Id &&
      *(this->prefixexp->var->id) == ("print")) {
    len += this->args->generateCode(code, table, varTable);
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("print", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("read")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("read", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isNil")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isNil", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isBool")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isBool", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isInt")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isInt", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isFloat")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isFloat", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isString")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isString", table)));
  } else if (this->prefixexp->type == NT_PrefixExp::Var &&
             this->prefixexp->var->type == NT_Var::Id &&
             *(this->prefixexp->var->id) == ("isTable")) {
    len += write(code, INVOKESTATIC);
    len += write(code, reverse2(findMethodref("isTable", table)));
  } else {
    len += this->prefixexp->generateCode(code, table, varTable);
    len += this->args->generateCode(code, table, varTable);
    len += write(code, INVOKEVIRTUAL);
    len += write(code, reverse2(findMethodref("invoke", table)));
  }
  return len;
}

int Args::generateCode(std::vector<char> *code, ConstTable *table,
                       VarTable *varTable) {
  int len = 0;
  len += write(code, SIPUSH);
  if (this->explist != nullptr) {
    len += write(code, reverse2(this->explist->size()));  // WARNING max args
    // count is FF(255) due
    // to sipush instruction
    // constraints. It's
    // enough IMHO
  } else {
    len += write(code, reverse2s(0));
  }
  len += write(code, ANEWARRAY);
  len += write(code, reverse2(findByClassName("PolyVar", table)));
  if (this->explist != nullptr) {
    for (int i = 0; i < this->explist->size(); i++) {
      len += write(code, DUP);
      len += write(code, SIPUSH);
      len += write(code, reverse2(i));
      len += this->explist->at(i)->generateCode(code, table, varTable);
      len += write(code, AASTORE);
    }
  }
  return len;
}

int FuncBody::generateCode(std::vector<char> *code, ConstTable *table,
                           VarTable *varTable) {
  std::string filepath = std::string("bin/" + *(this->className) + ".class");
  Block *root = this->block;
  root->localConstTable = this->newConstTable;
  std::ofstream out(filepath.c_str(), std::ios::binary);
  unsigned long tmp4 = reverse4(CAFEBABE);
  out.write(reinterpret_cast<const char *>(&tmp4), 4);  // CAFEBABE
  tmp4 = reverse4(SDK_VERSION);
  out.write(reinterpret_cast<const char *>(&tmp4),
            4);  // SDK version (00 00 00 34)
  unsigned short tmp2 = reverse2(root->localConstTable->size());
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // constTableSize
  // constTable
  for (int i = 1; i < root->localConstTable->size(); i++) {
    root->localConstTable->at(i)->writeToClassFile(out);
  }
  tmp2 = reverse2(3);  // access flags
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(findByClassName(*(this->className),
                                  root->localConstTable));  // current class
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(findByClassName("Function", root->localConstTable));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // parent class(object)
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // interface count

  tmp2 = reverse2(0);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // fields count

  tmp2 = reverse2(2);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method count
  // invoke method
  tmp2 = reverse2(0x0001);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // access flags
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("invoke"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method name
  tmp2 = reverse2(findUtf(root->localConstTable,
                          *(new std::string("([LPolyVar;)LPolyVar;"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method descriptor
  tmp2 = reverse2(1);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // attribute count
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("Code"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // Code attr

  int len = 0;
  std::vector<char> *ncode = new std::vector<char>();
  for (int i = 2;
       i < root->localVarTable->size() + RESERVE_LOCAL_VARS_FOR_ASSIGNMENT;
       i++) {
    len += write(ncode, JBC_NEW);
    len += write(ncode,
                 reverse2(findByClassName("PolyVar", root->localConstTable)));
    len += write(ncode, DUP);
    len += write(ncode, INVOKESPECIAL);
    len +=
        write(ncode, reverse2(findMethodref("<init>", root->localConstTable)));
    len += write(ncode, ASTORE);
    len += write(ncode, (char)(i));
  }

  len += write(ncode, ALOAD);
  len += write(ncode, (char)1);
  for (int i = 0; i < this->parlist->namelist->size(); i++) {
    // put from arr on stack
    len += write(ncode, DUP);
    len += write(ncode, SIPUSH);
    len += write(ncode, reverse2s(i));
    len += write(ncode, AALOAD);
    // put from stack to localVar
    len += write(ncode, ASTORE);
    len += write(ncode, (char)(i + 2));
  }
  // pop arr from stack
  len += write(ncode, POP);

  len += root->generateCode(ncode);
  len += write(ncode, JBC_NEW);
  len +=
      write(ncode, reverse2(findByClassName("PolyVar", root->localConstTable)));
  len += write(ncode, DUP);
  len += write(ncode, INVOKESPECIAL);
  len += write(ncode, reverse2(findMethodref("<init>", root->localConstTable)));
  len += write(ncode, ARETURN);

  tmp4 = reverse4(ncode->size() + 2 + 2 + 4 + 2 + 2);  // Atribute len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  tmp2 = reverse2(2310);  // Stack size
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(root->localVarTable->size() +
                  RESERVE_LOCAL_VARS_FOR_ASSIGNMENT);  // Local vars count + 20
                                                       // reserved for multiple
                                                       // assignment
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp4 = reverse4(ncode->size());  // Bytecode len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  out.write((const char *)ncode->data(), ncode->size());  // Bytecode
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // exception count
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method attr count
  //<init> method
  tmp2 = reverse2(0x0001);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // access flags
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("<init>"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method name
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("()V"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method descriptor
  tmp2 = reverse2(1);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // attribute count
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("Code"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // Code attr

  len = 0;
  std::vector<char> *initcode = new std::vector<char>();
  len += write(initcode, ALOAD_0);
  len += write(initcode, INVOKESPECIAL);
  len +=
      write(initcode,
            reverse2(findMethodref("Function<init>", root->localConstTable)));
  len += write(initcode, JBC_RETURN);

  tmp4 = reverse4(initcode->size() + 2 + 2 + 4 + 2 + 2);  // Atribute len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  tmp2 = reverse2(2310);  // Stack size
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(root->localVarTable->size() +
                  RESERVE_LOCAL_VARS_FOR_ASSIGNMENT);  // Local vars count + 20
                                                       // reserved for multiple
                                                       // assignment
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp4 = reverse4(initcode->size());  // Bytecode len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  out.write((const char *)initcode->data(), initcode->size());  // Bytecode
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // exception count
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method attr count
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // class attr count
  out.close();

  len += write(code, JBC_NEW);
  len += write(code, reverse2(findByClassName("PolyVar", table)));
  len += write(code, DUP);
  len += write(code, JBC_NEW);
  len += write(code, reverse2(findByClassName(*(this->className), table)));
  len += write(code, DUP);
  len += write(code, INVOKESPECIAL);
  len += write(code, reverse2(findMethodref(
                         std::string(*(this->className) + "<init>"), table)));
  len += write(code, INVOKESPECIAL);
  len += write(code, reverse2(findMethodref("<init>Fn", table)));
  return len;
}

int VarDecl::generateCode(std::vector<char> *code, ConstTable *table,
                          VarTable *varTable) {
  int len = 0;
  if (this->type == NT_VarDecl::DeclDef) {
    int firstIndex = varTable->size() + 3;
    for (int i = 0; i < this->namelist->size(); i++) {
      len += write(code, JBC_NEW);
      len += write(code, reverse2(findByClassName("PolyVar", table)));
      len += write(code, DUP);
      len += this->explist->at(i)->generateCode(code, table, varTable);
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>PolyVar", table)));
      len += write(code, ASTORE);
      len += write(code, (char)(firstIndex + i));
    }
    for (int i = 0; i < this->namelist->size(); i++) {
      len += write(code, ALOAD);
      len += write(code, (char)(firstIndex + i));
      len += write(code, ASTORE);
      len += write(code, (char)(findVar(this->namelist->at(i), varTable)));
    }
  }
  return len;
}

int RetStat::generateCode(std::vector<char> *code, ConstTable *constTable,
                          VarTable *varTable) {
  int len = 0;
  switch (this->type) {
    case NT_RetStat::Empty:
      len += write(code, JBC_NEW);
      len += write(
          code, reverse2(findByClassName(std::string("PolyVar"), constTable)));
      len += write(code, DUP);
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>()V", constTable)));
      break;
    case NT_RetStat::NonEmpty:
      len += this->explist->at(0)->generateCode(
          code, constTable, varTable);  // WARNING only one value is returned
      break;
  }

  len += write(code, ARETURN);

  return len;
}

int Stat::generateCode(std::vector<char> *code, ConstTable *table,
                       VarTable *varTable) {
  int len = 0;
  int gotoIndex = -1;
  int blockIndex = -1;
  int localIndex = -1;
  int firstIndex = -1;

  switch (this->type) {
    case NT_Stat::VarDef:
      firstIndex = varTable->size() + 3;
      for (int i = 0; i < this->varlist->size(); i++) {
        len += write(code, JBC_NEW);
        len += write(code, reverse2(findByClassName("PolyVar", table)));
        len += write(code, DUP);
        len += this->explist->at(i)->generateCode(code, table, varTable);
        len += write(code, INVOKESPECIAL);
        len += write(code, reverse2(findMethodref("<init>PolyVar", table)));
        len += write(code, ASTORE);
        len += write(code, (char)(firstIndex + i));
      }
      for (int i = 0; i < this->varlist->size(); i++) {
        // a
        if (this->varlist->at(i)->type == NT_Var::Id) {
          localIndex = findVar(this->varlist->at(i)->id, varTable);
          if (localIndex > -1) {
            len += write(code, ALOAD);
            len += write(code, (char)(firstIndex + i));
            len += write(code, ASTORE);
            len += write(code, (char)localIndex);
          } else {
            len += write(code, ALOAD);
            len += write(code, (char)(firstIndex + i));
            len += write(code, PUTSTATIC);
            len += write(code, reverse2(this->varlist->at(i)->indexInTable));
          }
        } else {
          // a[i]
          len += this->varlist->at(i)->prefixexp->generateCode(
              code, table, varTable);  // Put a on stack
          len += this->varlist->at(i)->exp->generateCode(
              code, table, varTable);  // put i on stack
          len += write(code, ALOAD);
          len += write(code, (char)(firstIndex + i));
          len += write(code, INVOKEVIRTUAL);
          len += write(code, reverse2(findMethodref("setTableField", table)));
        }
      }
      break;
    case NT_Stat::FuncCallOnly:
      if (this->prefixexp->type != NT_PrefixExp::FuncCall) {
        // NOTE should be handled on parsing
      } else {
        len += this->prefixexp->generateCode(code, table, varTable);
      }
      break;
    case NT_Stat::Label:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::Break:
      // NOTE should be handled on parsing
      len += write(code, JBC_GOTO);
      this->breakAddress = code->size();
      len += write(code, reverse2(0));  // set fictive goto offset
      break;
    case NT_Stat::Goto:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::Block:
      len += this->block->generateCode(code);
      break;
    case NT_Stat::WhileLoop:
      len += write(code, JBC_GOTO);
      gotoIndex = code->size();
      len += write(code, reverse2s(0));

      blockIndex = code->size();
      len += this->block->generateCode(code);
      write(code, reverse2s(code->size() - gotoIndex + 1), gotoIndex);

      len += this->exp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("evaluate", table)));

      len += write(code, IFNE);
      len += write(code, reverse2s(blockIndex - code->size() + 1));

      this->block->generateBreakAddress(code);
      break;
    case NT_Stat::RepUntilLoop:
      blockIndex = code->size();
      len += this->block->generateCode(code);

      len += this->exp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("evaluate", table)));

      len += write(code, IFEQ);
      len += write(code, reverse2s(blockIndex - code->size() + 1));
      this->block->generateBreakAddress(code);
      break;
    case NT_Stat::IfElse:
      len += this->ifelse->generateCode(code, table, varTable);
      break;
    case NT_Stat::ForLoop:
      len += this->forloop->generateCode(code, table, varTable);
      break;
    case NT_Stat::ForEach:
      // NOTE should be handled on parsing
      break;
    case NT_Stat::FuncDecl:
      len += this->funcbody->generateCode(code, table, varTable);
      len += write(code, PUTSTATIC);
      len += write(code, reverse2(this->indexInTable));
      break;
    case NT_Stat::LocalFuncDecl:
      len += write(code, ALOAD);
      len += write(code, (char)(0));
      len += this->funcbody->generateCode(code, table, varTable);
      len += write(code, ASTORE);
      len += write(code, (char)(this->indexInTable));
      break;
    case NT_Stat::VarDecl:
      len += this->vardecl->generateCode(code, table, varTable);
      break;
  }
  return len;
}

int Exp::generateCode(std::vector<char> *code, ConstTable *table,
                      VarTable *varTable) {
  int len = 0;
  switch (this->type) {
    case NT_Exp::Nil:
      len += write(code, JBC_NEW);
      len +=
          write(code, reverse2(findByClassName(std::string("PolyVar"), table)));
      len += write(code, DUP);
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>", table)));
      break;
    case NT_Exp::Bool:
      len += write(code, JBC_NEW);
      len +=
          write(code, reverse2(findByClassName(std::string("PolyVar"), table)));
      len += write(code, DUP);
      if (this->b) {
        len += write(code, ICONST_1);
      } else {
        len += write(code, ICONST_0);
      }
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>Z", table)));
      break;
    case NT_Exp::Int:
      len += write(code, JBC_NEW);
      len +=
          write(code, reverse2(findByClassName(std::string("PolyVar"), table)));
      len += write(code, DUP);
      len += write(code, LDC_W);
      len += write(code, reverse2(this->indexInTable));
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>I", table)));
      break;
    case NT_Exp::Float:
      len += write(code, JBC_NEW);
      len += write(code, reverse2(findByClassName("PolyVar", table)));
      len += write(code, DUP);
      len += write(code, LDC_W);
      len += write(code, reverse2(this->indexInTable));
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>F", table)));
      break;
    case NT_Exp::String:
      len += write(code, JBC_NEW);
      len += write(code, reverse2(findByClassName("PolyVar", table)));
      len += write(code, DUP);
      len += write(code, LDC_W);
      len += write(code, reverse2(this->indexInTable));
      len += write(code, INVOKESPECIAL);
      len += write(code, reverse2(findMethodref("<init>S", table)));
      break;
    case NT_Exp::Funcdef:
      this->fDef->funcbody->generateCode(code, table, varTable);
      break;
    case NT_Exp::Prefexp:
      len += this->prefExp->generateCode(code, table, varTable);
      break;
    case NT_Exp::Tconstr:
      len += this->tConstr->generateCode(code, table, varTable);
      break;
    case NT_Exp::Plus:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("plus", table)));
      break;
    case NT_Exp::Minus:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("minus", table)));
      break;
    case NT_Exp::Multiply:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("mult", table)));
      break;
    case NT_Exp::Div:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("div", table)));
      break;
    case NT_Exp::Divint:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("floorDiv", table)));
      break;
    case NT_Exp::Pow:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("exp", table)));
      break;
    case NT_Exp::Mod:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("mod", table)));
      break;
    case NT_Exp::Band:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("band", table)));
      break;
    case NT_Exp::Xor:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("bxor", table)));
      break;
    case NT_Exp::Bor:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("or", table)));
      break;
    case NT_Exp::Shr:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("rsh", table)));
      break;
    case NT_Exp::Shl:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("lsh", table)));
      break;
    case NT_Exp::Concat:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("concat", table)));
      break;
    case NT_Exp::Lt:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("lt", table)));
      break;
    case NT_Exp::Lte:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("le", table)));
      break;
    case NT_Exp::Gt:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("gt", table)));
      break;
    case NT_Exp::Gte:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("ge", table)));
      break;
    case NT_Exp::Eq:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("eq", table)));
      break;
    case NT_Exp::Neq:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("neq", table)));
      break;
    case NT_Exp::And:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("and", table)));
      break;
    case NT_Exp::Or:
      this->lExp->generateCode(code, table, varTable);
      this->rExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("or", table)));
      break;
    case NT_Exp::Uminus:
      this->lExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("unm", table)));
      break;
    case NT_Exp::Not:
      this->lExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("not", table)));
      break;
    case NT_Exp::Len:
      this->lExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("len", table)));
      break;
    case NT_Exp::Ubnot:
      this->lExp->generateCode(code, table, varTable);
      len += write(code, INVOKEVIRTUAL);
      len += write(code, reverse2(findMethodref("ubnot", table)));
      break;
  }
  return len;
}
