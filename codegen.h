#ifndef CODEGEN_H
#define CODEGEN_H

#include <algorithm>
#include <fstream>
#include <ios>
#include <iostream>
#include <iterator>
#include "ast.h"
#include "commands.h"

unsigned short reverse2(unsigned short num);
short reverse2s(short num);

unsigned long reverse4(unsigned long num);
long reverse4s(long num);

float reverse4f(float num);

#endif  // CODEGEN_H
