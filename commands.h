#ifndef DICTIONARY_H
#define DICTIONARY_H

#define CLASS_ACC_PUBLIC \
  0x01  // Declared public; may be accessed from outside its package.
#define CLASS_ACC_FINAL 0x0010  // Declared final; no subclasses allowed.
#define CLASS_ACC_SUPER \
  0x0020  // Treat superclass methods specially when invoked by the
          // invokespecial instruction.
#define CLASS_ACC_INTERFACE 0x0200  // Is an interface, not a class.
#define CLASS_ACC_ABSTRACT \
  0x0400  // Declared abstract; must not be instantiated.
#define CLASS_ACC_SYNTHETIC \
  0x1000  // Declared synthetic; not present in the source code.
#define CLASS_ACC_ANNOTATION 0x2000  // Declared as an annotation type.
#define CLASS_ACC_ENUM 0x4000        // Declared as an enum type.

#define FIELD_ACC_PUBLIC \
  0x0001  // Declared public; may be accessed from outside its package.
#define FIELD_ACC_PRIVATE \
  0x0002  // Declared private; usable only within the defining class.
#define FIELD_ACC_PROTECTED \
  0x0004  // Declared protected; may be accessed within subclasses.
#define FIELD_ACC_STATIC 0x0008  // Declared static.
#define FIELD_ACC_FINAL \
  0x0010  // Declared final; never directly assigned to after object
          // construction (JLS §17.5).
#define FIELD_ACC_VOLATILE 0x0040  // Declared volatile; cannot be cached.
#define FIELD_ACC_TRANSIENT \
  0x0080  // Declared transient; not written or read by a persistent object
          // manager.
#define FIELD_ACC_SYNTHETIC \
  0x1000  // Declared synthetic; not present in the source code.
#define FIELD_ACC_ENUM 0x4000  // Declared as an element of an enum.

#define METHOD_ACC_PUBLIC \
  0x0001  // Declared public; may be accessed from outside its package.
#define METHOD_ACC_PRIVATE \
  0x0002  // Declared private; accessible only within the defining class.
#define METHOD_ACC_PROTECTED \
  0x0004  // Declared protected; may be accessed within subclasses.
#define METHOD_ACC_STATIC 0x0008  // Declared static.
#define METHOD_ACC_FINAL \
  0x0010  // Declared final; must not be overridden (§5.4.5).
#define METHOD_ACC_SYNCHRONIZED \
  0x0020  // Declared synchronized; invocation is wrapped by a monitor use.
#define METHOD_ACC_BRIDGE 0x0040  // A bridge method, generated by the compiler.
#define METHOD_ACC_VARARGS \
  0x0080  // Declared with variable number of arguments.
#define METHOD_ACC_NATIVE \
  0x0100  // Declared native; implemented in a language other than Java.
#define METHOD_ACC_ABSTRACT \
  0x0400  // Declared abstract; no implementation is provided.
#define METHOD_ACC_STRICT \
  0x0800  // Declared strictfp; floating-point mode is FP-strict.
#define METHOD_ACC_SYNTHETIC \
  0x1000  // Declared synthetic; not present in the source code.

#define NESTED_ACC_PUBLIC 0x0001     // Marked or implicitly public in source.
#define NESTED_ACC_PRIVATE 0x0002    // Marked private in source.
#define NESTED_ACC_PROTECTED 0x0004  // Marked protected in source.
#define NESTED_ACC_STATIC 0x0008     // Marked or implicitly static in source.
#define NESTED_ACC_FINAL 0x0010      // Marked final in source.
#define NESTED_ACC_INTERFACE 0x0200  // Was an interface in source.
#define NESTED_ACC_ABSTRACT 0x0400   // Marked or implicitly abstract in source.
#define NESTED_ACC_SYNTHETIC \
  0x1000  // Declared synthetic; not present in the source code.
#define NESTED_ACC_ANNOTATION 0x2000  // Declared as an annotation type.
#define NESTED_ACC_ENUM 0x4000        // Declared as an enum type.

#define CAFEBABE \
  0xCAFEBABE  // "magic" number from which every class-file starts of

#define SDK_VERSION 0x00000034  // SDK version

/* Load reference from array
 * FORMAT:
 *   aaload
*/
#define AALOAD (char)0x32

/* Store into reference array
 * FORMAT:
 *   aastore
*/
#define AASTORE (char)0x53

/* Push null
 * FORMAT:
 *   aconst_null
*/
#define ACONST_NULL (char)0x1

/* Load reference from local variable
 * FORMAT:
 *   aload
 *   index
*/
#define ALOAD (char)0x19

/* Load reference from local variable
 * FORMAT:
 *   aload_0
*/
#define ALOAD_0 (char)0x2a

/* Load reference from local variable
 * FORMAT:
 *   aload_1
*/
#define ALOAD_1 (char)0x2b

/* Load reference from local variable
 * FORMAT:
 *   aload_2
*/
#define ALOAD_2 (char)0x2c

/* Load reference from local variable
 * FORMAT:
 *  aload_3
*/
#define ALOAD_3 (char)0x2d

/* Create new array of reference
 * FORMAT:
 *   anewarray
 *   indexbyte1
 *   indexbyte2
*/
#define ANEWARRAY (char)0xbd

/* Return reference from method
 * FORMAT:
 *   areturn
*/
#define ARETURN (char)0xb0

/* Get length of array
 * FORMAT:
 *   arraylength
*/
#define ARRAYLENGTH (char)0xbe

/* Store reference into local variable
 * FORMAT:
 *   astore
 *   index
*/
#define ASTORE (char)0x3a

/* Store reference into local variable
 * FORMAT:
 *   astore_0
*/
#define ASTORE_0 (char)0x4b

/* Store reference into local variable
 * FORMAT:
 *   astore_1
*/
#define ASTORE_1 (char)0x4c

/* Store reference into local variable
 * FORMAT:
 *   astore_2
*/
#define ASTORE_2 (char)0x4d

/* Store reference into local variable
 * FORMAT:
 *   astore_3
*/
#define ASTORE_3 (char)0x4e

/* Throw exception or error
 * FORMAT:
 *   athrow
*/
#define ATHROW (char)0xbf

/* Load byte or boolean from array
 * FORMAT:
 *   baload
*/
#define BALOAD (char)0x33

/* Store into byte or boolean array
 * FORMAT:
 *   bastore
*/
#define BASTORE (char)0x54

/* Push byte
 * FORMAT:
 *   bipush
 *   byte
*/
#define BIPUSH (char)0x10

/* Load char from array
 * FORMAT:
 *   caload
*/
#define CALOAD (char)0x34

/* Store into char array
 * FORMAT:
 *   castore
*/
#define CASTORE (char)0x55

/* Check whether object of given type
 * FORMAT:
 *   checkcast
 *   indexbyte1
 *   indexbyte2
*/
#define CHECKCAST (char)0xc0

/* Convert double to float
 * FORMAT:
 *   d2f
*/
#define D2F (char)0x90

/* Convert double to integer
 * FORMAT:
 *   d2i
*/
#define D2I (char)0x8e

/* Convert double to long
 * FORMAT:
 *   d2l
*/
#define D2L (char)0x8f

/* Add double
 * FORMAT:
 *   dadd
*/
#define DADD (char)0x63

/* Load double from array
 * FORMAT:
 *   daload
*/
#define DALOAD (char)0x31

/* Store into double array
 * FORMAT:
 *   dastore
*/
#define DASTORE (char)0x52

/* Compare double
 * FORMAT:
 *   dcmpg
*/
#define DCMPG (char)0x98

/* Compare double
 * FORMAT:
 *   dcmpl
*/
#define DCMPL (char)0x97

/* Push double
 * FORMAT:
 *   dconst_0
*/
#define DCONST_0 (char)0xe

/* Push double
 * FORMAT:
 *   dconst_1
*/
#define DCONST_1 (char)0xf

/* Divide double
 * FORMAT:
 *   ddiv
*/
#define DDIV (char)0x6f

/* Load double from local variable
 * FORMAT:
 *   dload
 *   index
*/
#define DLOAD (char)0x18

/* Load double from local variable
 * FORMAT:
 *   dload_0
*/
#define DLOAD_0 (char)0x26

/* Load double from local variable
 * FORMAT:
 *   dload_1
*/
#define DLOAD_1 (char)0x27

/* Load double from local variable
 * FORMAT:
 *   dload_2
*/
#define DLOAD_2 (char)0x28

/* Load double from local variable
 * FORMAT:
 *   dload_3
*/
#define DLOAD_3 (char)0x29

/* Multiply double
 * FORMAT:
 *   dmul
*/
#define DMUL (char)0x6b

/* Negate double
 * FORMAT:
 *   dneg
*/
#define DNEG (char)0x77

/* Remainder double
 * FORMAT:
 *   drem
*/
#define DREM (char)0x73

/* Return double from method
 * FORMAT:
 *   dreturn
*/
#define DRETURN (char)0xaf

/* Store double into local variable
 * FORMAT:
 *   dstore
 *   index
*/
#define DSTORE (char)0x39

/* Store double into local variable
 * FORMAT:
 *   dstore_0
*/
#define DSTORE_0 (char)0x47

/* Store double into local variable
 * FORMAT:
 *   dstore_1
*/
#define DSTORE_1 (char)0x48

/* Store double into local variable
 * FORMAT:
 *   dstore_2
*/
#define DSTORE_2 (char)0x49

/* Store double into local variable
 * FORMAT:
 *   dstore_3
*/
#define DSTORE_3 (char)0x4a

/* Subtract double
 * FORMAT:
 *   dsub
*/
#define DSUB (char)0x67

/* Duplicate the top operand stack value
 * FORMAT:
 *   dup
*/
#define DUP (char)0x59

/* Duplicate the top operand stack value and insert two values down
 * FORMAT:
 *   dup_x1
*/
#define DUP_X1 (char)0x5a

/* Duplicate the top operand stack value and insert two or three values down
 * FORMAT:
 *   dup_x2
*/
#define DUP_X2 (char)0x5b

/* Duplicate the top one or two operand stack value
 * FORMAT:
 *   dup2
*/
#define DUP2 (char)0x5c

/* Duplicate the top one or two operand stack value and insert two or three
 * values down
 * FORMAT:
 *   dup2_x1
*/
#define DUP2_X1 (char)0x5d

/* Duplicate the top one or two operand stack value and insert two, three or
 * four values down
 * FORMAT:
 *   dup2_x2
*/
#define DUP2_X2 (char)0x5e

/* Convert float to double
 * FORMAT:
 *   f2d
*/
#define F2D (char)0x8d

/* Convert float to int
 * FORMAT:
 *   f2i
*/
#define F2I (char)0x8b

/* Convert float to long
 * FORMAT:
 *   f2l
*/
#define F2L (char)0x8c

/* Add float
 * FORMAT:
 *  fadd
*/
#define FADD (char)0x62

/* Load float from array
 * FORMAT:
 *   faload
*/
#define FALOAD (char)0x30

/* Store into float array
 * FORMAT:
 *   fastore
*/
#define FASTORE (char)0x51

/* Compare float
 * FORMAT:
 *   fcmpg
*/
#define FCMPG (char)0x96

/* Compare float
 * FORMAT:
 *   fcmpl
*/
#define FCMPL (char)0x95

/* Push int constant
 * FORMAT:
 *   fconst_0
*/
#define FCONST_0 (char)0xb

/* Push int constant
 * FORMAT:
 *   fconst_1
*/
#define FCONST_1 (char)0xc

/* Push int constant
 * FORMAT:
 *   fconst_1
*/
#define FCONST_2 (char)0xd

/* Divide float
 * FORMAT:
 *   fdiv
*/
#define FDIV (char)0x6e

/* Load float from local variable
 * FORMAT:
 *   fload
 *   index
*/
#define FLOAD (char)0x17

/* Load float from local variable
 * FORMAT:
 *   fload_0
*/
#define FLOAD_0 (char)0x22

/* Load float from local variable
 * FORMAT:
 *   fload_1
*/
#define FLOAD_1 (char)0x23

/* Load float from local variable
 * FORMAT:
 *   fload_2
*/
#define FLOAD_2 (char)0x24

/* Load float from local variable
 * FORMAT:
 *   fload_3
*/
#define FLOAD_3 (char)0x25

/* Multiply float
 * FORMAT:
 *   fmul
*/
#define FMUL (char)0x6a

/* Negate float
 * FORMAT:
 *   fneg
*/
#define FNEG (char)0x76

/* Remainder float
 * FORMAT:
 *   frem
*/
#define FREM (char)0x72

/* Return float from method
 * FORMAT:
 *   freturn
*/
#define FRETURN (char)0xae

/* Store float into local variable
 * FORMAT:
 *   fstore
 *   index
*/
#define FSTORE (char)0x38

/* Store float into local variable
 * FORMAT:
 *   fstore_0
*/
#define FSTORE_0 (char)0x43

/* Store float into local variable
 * FORMAT:
 *   fstore_1
*/
#define FSTORE_1 (char)0x44

/* Store float into local variable
 * FORMAT:
 *   fstore_2
*/
#define FSTORE_2 (char)0x45

/* Store float into local variable
 * FORMAT:
 *   fstore_3
*/
#define FSTORE_3 (char)0x46

/* Subtract float
 * FORMAT:
 *   fsub
*/
#define FSUB (char)0x66

/* Fetch field from object
 * FORMAT:
 *   getfield
 *  indexbyte1
 *  indexbyte2
*/
#define GETFIELD (char)0xb4

/* Get static field from class
 * FORMAT:
 *   getstatic
 *  indexbyte1
 *  indexbyte2
*/
#define GETSTATIC (char)0xb2

/* Branch always
 * FORMAT:
 *   goto
 *   branchbyte1
 *  branchbyte2
*/
#define JBC_GOTO (char)0xa7

/* Branch always (wide index)
 * FORMAT:
 *   goto_w
 *   branchbyte1
 *   branchbyte2
 *   branchbyte3
 *   branchbyte4
*/
#define GOTO_W (char)0xc8

/* Convert int to byte
 * FORMAT:
 *   i2b
*/
#define I2B (char)0x91

/* Convert int to char
 * FORMAT:
 *   i2c
*/
#define I2C (char)0x92

/* Convert int to double
 * FORMAT:
 *   i2d
*/
#define I2D (char)0x87

/* Convert int to float
 * FORMAT:
 *   i2f
*/
#define I2F (char)0x86

/* Convert int to long
 * FORMAT:
 *   i2l
*/
#define I2L (char)0x85

/* Convert int to short
 * FORMAT:
 *   i2s
*/
#define I2S (char)0x93

/* Add int
 * FORMAT:
 *   iadd
*/
#define IADD (char)0x60

/* Load int from array
 * FORMAT:
 *   iaload
*/
#define IALOAD (char)0x2e

/* Boolean AND int
 * FORMAT
 *   iand
*/
#define IAND (char)0x7e

/* Store into int array
 * FORMAT:
 *   iastore
*/
#define IASTORE (char)0x4f

/* Push int constant (-1)
 * FORMAT:
 iconst_m1
*/
#define ICONST_M1 (char)0x2

/* Push int constant (0)
 * FORMAT:
 iconst_0
*/
#define ICONST_0 (char)0x3

/* Push int constant (1)
 * FORMAT:
 *   iconst_1
*/
#define ICONST_1 (char)0x4

/* Push int constant (2)
 * FORMAT:
 *   iconst_2
*/
#define ICONST_2 (char)0x5

/* Push int constant (3)
 * FORMAT:
 *   iconst_3
*/
#define ICONST_3 (char)0x6

/* Push int constant (4)
 * FORMAT:
 *   iconst_4
*/
#define ICONST_4 (char)0x7

/* Push int constant (5)
 * FORMAT:
 *   iconst_5
*/
#define ICONST_5 (char)0x8

/* Divide int
 * FORMAT:
 *   idiv
*/
#define IDIV (char)0x6c

/* Branch if reference comparison succeeds
 * FORMAT:
 *   if_acmpeq
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ACMPEQ (char)0xa5

/* Branch if reference comparison succeeds
 * FORMAT:
 *   if_acmpne
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ACMPNE (char)0xa6

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmpeq
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPEQ (char)0x9f

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmpne
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPNE (char)0xa0

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmplt
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPLT (char)0xa1

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmpge
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPGE (char)0xa2

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmpgt
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPGT (char)0xa3

/* Branch if int comparison succeeds
 * FORMAT:
 *   if_icmple
 *   branchbyte1
 *   branchbyte2
*/
#define IF_ICMPLE (char)0xa4

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   ifeq
 *   branchbyte1
 *   branchbyte2
*/
#define IFEQ (char)0x99

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   ifne
 *   branchbyte1
 *   branchbyte2
*/
#define IFNE (char)0x9a

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   iflt
 *   branchbyte1
 *   branchbyte2
*/
#define IFLT (char)0x9b

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   ifge
 *   branchbyte1
 *   branchbyte2
*/
#define IFGE (char)0x9c

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   ifgt
 *   branchbyte1
 *   branchbyte2
*/
#define IFGT (char)0x9d

/* Branch if int comparison with zero succeeds
 * FORMAT:
 *   ifle
 *   branchbyte1
 *   branchbyte2
*/
#define IFLE (char)0x9e

/* Branch if reference not null
 * FORMAT:
 *   ifnonnull
 *   branchbyte1
 *   branchbyte2
*/
#define IFNONNULL (char)0xc7

/* Branch if reference is null
 * FORMAT:
 *   ifnull
 *   branchbyte1
 *   branchbyte2
*/
#define IFNULL (char)0xc6

/* Increment local variable by constant
 * FORMAT:
 *   iinc
 *   index
 *   const
*/
#define IINC (char)0x84

/* Load int from local variable
 * FORMAT:
 *   iload
 *   index
*/
#define ILOAD (char)0x15

/* Load int from local variable
 * FORMAT:
 *   iload_0
*/
#define ILOAD_0 (char)0x1a

/* Load int from local variable
 * FORMAT:
 *   iload_1
*/
#define ILOAD_1 (char)0x1b

/* Load int from local variable
 * FORMAT:
 *   iload_2
*/
#define ILOAD_2 (char)0x1c

/* Load int from local variable
 * FORMAT:
 *   iload_3
*/
#define ILOAD_3 (char)0x1d

/* Multiply int
 * FORMAT:
 *   imul
*/
#define IMUL (char)0x68

/* Negate int
 * FORMAT:
 *   ineg
*/
#define INEG (char)0x74

/* Determine if object of given type
 * FORMAT:
 *   instanceof
 *   indexbyte1
 *   indexbyte2
*/
#define INSTANCEOF (char)0xc1

/* Invoke dynamic method
 * FORMAT:
 *   invokedynamic
 *   indexbyte1
 *   indexbyte2
 *   0
 *   0
*/
#define INVOKEDYNAMIC (char)0xba

/* Invoke interface method
 * FORMAT:
 *   invokeinterface
 *   indexbyte1
 *   indexbyte2
 *   count
 *   0
*/
#define INVOKEINTERFACE (char)0xb9

/* Invoke instance method; special handling for superclass, private, and
 * instance initialization method invocations
 * FORMAT:
 *   invokespecial
 *   indexbyte1
 *   indexbyte2
*/
#define INVOKESPECIAL (char)0xb7

/* Invoke a class (static) method
 * FORMAT:
 *   invokestatic
 *   indexbyte1
 *   indexbyte2
*/
#define INVOKESTATIC (char)0xb8

/* Invoke instance method; dispatch based on class
 * FORMAT:
 *   invokevirtual
 *   indexbyte1
 *   indexbyte2
*/
#define INVOKEVIRTUAL (char)0xb6

/* Boolean OR int
 * FORMAT:
 *   ior
*/
#define IOR (char)0x80

/* Remainder int
 * FORMAT:
 *   irem
*/
#define IREM (char)0x70

/* Return int from method
 * FORMAT:
 *   ireturn
*/
#define IRETURN (char)0xac

/* Shift left int
 * FORMAT:
 *   ishl
*/
#define ISHL (char)0x78

/* Shift right int
 * FORMAT:
 *   ishr
*/
#define ISHR (char)0x7a

/* Store int into local variable
 * FORMAT:
 *   istore
 *   index
*/
#define ISTORE (char)0x36

/* Store int into local variable
 * FORMAT:
 *   istore_0
*/
#define ISTORE_0 (char)0x3b

/* Store int into local variable
 * FORMAT:
 *   istore_1
*/
#define ISTORE_1 (char)0x3c

/* Store int into local variable
 * FORMAT:
 *   istore_2
*/
#define ISTORE_2 (char)0x3d

/* Store int into local variable
 * FORMAT:
 *   istore_3
*/
#define ISTORE_3 (char)0x3e

/* Subtract int
 * FORMAT:
 *   isub
*/
#define ISUB (char)0x64

/* Logical shift right int
 * FORMAT:
 *   iushr
*/
#define IUSHR (char)0x7c

/* Boolean xor int
 * FORMAT:
 *   ixor
*/
#define IXOR (char)0x82

/* Jump subroutine
 * FORMAT:
 *   jsr
 *   branchbyte1
 *   branchbyte2
*/
#define JSR (char)0xa8

/* Jump subroutine (wide index)
 * FORMAT:
 *   jsr
 *   branchbyte1
 *   branchbyte2
 *   branchbyte3
 *   branchbyte4
*/
#define JSR_W (char)0xc9

/* Convert long to double
 * FORMAT:
 *   l2d
*/
#define L2D (char)0x8a

/* Convert long to float
 * FORMAT:
 *   l2f
*/
#define L2F (char)0x89

/* Convert long to int
 * FORMAT:
 *   l2i
*/
#define L2I (char)0x88

/* Add long
 * FORMAT:
 *   ladd
*/
#define LADD (char)0x61

/* Load long from array
 * FORMAT:
 *   laload
*/
#define LALOAD (char)0x2f

/* Boolean AND long
 * FORMAT:
 *   land
*/
#define LAND (char)0x7f

/* Store into long array
 * FORMAT:
 *   lastore
*/
#define LASTORE (char)0x50

/* Compare long
 * FORMAT:
 *   lcmp
*/
#define LCMP (char)0x94

/* Push long constant
 * FORMAT:
 *   lconst_0
*/
#define LCONST_0 (char)0x9

/* Push long constant
 * FORMAT:
 *   lconst_0
*/
#define LCONST_1 (char)0xa

/* Push item from run-time constant pool
 * FORMAT:
 *   ldc
 *   index
*/
#define LDC (char)0x12

/* Push item from run-time constant pool (wide index)
 * FORMAT:
 *   ldc_w
 *   indexbyte1
 *   indexbyte2
*/
#define LDC_W (char)0x13

/* Push long or double from run-time constant pool (wide index)
 * FORMAT:
 *   ldc2_w
 *   indexbyte1
 *   indexbyte2
*/
#define LDC2_W (char)0x14

/* Divide long
 * FORMAT:
 *   ldiv
*/
#define LDIV (char)0x6d

/* Load long from local variable
 * FORMAT:
 *   lload
*/
#define LLOAD (char)0x16

/* Load long from local variable
 * FORMAT:
 *   lload_0
*/
#define LLOAD_0 (char)0x1e

/* Load long from local variable
 * FORMAT:
 *   lload_1
*/
#define LLOAD_1 (char)0x1f

/* Load long from local variable
 * FORMAT:
 *   lload_2
*/
#define LLOAD_2 (char)0x20

/* Load long from local variable
 * FORMAT:
 *   lload_3
*/
#define LLOAD_3 (char)0x21

/* Multiply long
 * FORMAT:
 *   lmul
*/
#define LMUL (char)0x69

/* Negate long
 * FORMAT:
 *   lneg
*/
#define LNEG (char)0x75

/* Access jump table by key match and jump
 * FORMAT:
 *   lookupswitch
 *   <0-3 byte pad>
 *   defaultbyte1
 *   defaultbyte2
 *   defaultbyte3
 *   defaultbyte4
 *   npairs1
 *   npairs2
 *   npairs3
 *   npairs4
 *   match-offset pairs...
*/
#define LOOKUPSWITCH (char)0xab

/* Boolean OR long
 * FORMAT:
 *   lor
*/
#define LOR (char)0x81

/* Remainder long
 * FORMAT:
 *   lrem
*/
#define LREM (char)0x71

/* Return long from method
 * FORMAT:
 *   lreturn
*/
#define LRETURN (char)0xad

/* Shift left long
 * FORMAT:
 *   lshl
*/
#define LSHL (char)0x79

/* Arithmetic shift right long
 * FORMAT:
 *   lshr
*/
#define LSHR (char)0x7b

/* Store long into local variable
 * FORMAT:
 *   lstore
 *   index
*/
#define LSTORE (char)0x37

/* Store long into local variable
 * FORMAT:
 *   lstore_0
*/
#define LSTORE_0 (char)0x3f

/* Store long into local variable
* FORMAT:
*   lstore_1
*/
#define LSTORE_1 (char)0x40

/* Store long into local variable
* FORMAT:
*   lstore_2
*/
#define LSTORE_2 (char)0x41

/* Store long into local variable
* FORMAT:
*   lstore_3
*/
#define LSTORE_3 (char)0x42

/* Subtract long
 * FORMAT:
 *   lsub
*/
#define LSUB (char)0x65

/* Logical shift right long
 * FORMAT:
 *   lushr
*/
#define LUSHR (char)0x7d

/* Boolean XOR long
 * FORMAT:
 *   lxor
*/
#define LXOR (char)0x83

/* Enter monitor for object
 * FORMAT:
 *   monitorenter
*/
#define MONITORENTER (char)0xc2

/* Exit monitor for object
 * FORMAT:
 *   monitorexit
*/
#define MONITOREXIT (char)0xc3

/* Create new multidimensional array
 * FORMAT:
 *   multianewarray
 *   indexbyte1
 *   indexbyte2
 *   dimensions
*/
#define MULTIANEWARRAY (char)0xc5

/* Create new object
 * FORMAT:
 *   new
 *   indexbyte1
 *   indexbyte2
*/
#define JBC_NEW (char)0xbb

/* Create new array
 * FORMAT:
 *   newarray
 *   atype
*/
#define NEWARRAY (char)0xbc

/* Do nothing
 * FORMAT:
 *   nop
*/
#define NOP (char)0x0

/* Pop the top operand stack value
 * FORMAT:
 *   pop
*/
#define POP (char)0x57

/* Pop the top one or two operand stack values
 * FORMAT:
 *   pop2
*/
#define POP2 (char)0x58

/* Set field in object
 * FORMAT:
 *   putfield
 *   indexbyte1
 *   indexbyte2
*/
#define PUTFIELD (char)0xb5

/* Set static field in class
 * FORMAT:
 *   putstatic
 *   indexbyte1
 *   indexbyte2
*/
#define PUTSTATIC (char)0xb3

/* Return from subroutine
 * FORMAT:
 *   ret
 *   index
*/
#define RET (char)0xa9

/* Return void from method
 * FORMATL
 *   return
*/
#define JBC_RETURN (char)0xb1

/* Load short from array
 * FORMAT:
 *   saload
*/
#define SALOAD (char)0x35

/* Store into short array
 * FORMAT:
 *   sastore
*/
#define SASTORE (char)0x56

/* Push short
 * FORMAT:
 *   sipush
 *   byte1
 *   byte2
*/
#define SIPUSH (char)0x11

/* Swap the top two operand stack values
 * FORMAT:
 *   swap
*/
#define SWAP (char)0x5f

/* Access jump table by index and jump
 * FORMAT:
 *   tableswitch
 *   <0-3 byte pad>
 *   defaultbyte1
 *   defaultbyte2
 *   defaultbyte3
 *   defaultbyte4
 *   lowbyte1
 *   lowbyte2
 *   lowbyte3
 *   lowbyte4
 *   highbyte1
 *   highbyte2
 *   highbyte3
 *   highbyte4
 *   jump offsets...
*/
#define TABLESWITCH (char)0xaa

/* Extend local variable index by additional bytes
 * FORMAT1:
 *   wide
 *   <opcode>
 *   indexbyte1
 *   indexbyte2
 *
 * FORMAT2:
 *   wide
 *   iinc
 *   indexbyte1
 *   indexbyte2
 *   constbyte1
 *   constbyte2
*/
#define WIDE (char)0xc4

#endif  // DICTIONARY_H
