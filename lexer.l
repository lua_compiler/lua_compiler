%{
  #include <cstdio>
  #include <cstdlib>
  #include <string>
  #include <cmath>

  #include "ast.h"
  #include "parser.tab.hpp"

  #define EPS 0.00001
  #define SAVE_STRING(sval)     yylval.string = new std::string(sval, strlen(sval))
  #define SAVE_INT(inum)        yylval.ival = inum
  #define SAVE_DOUBLE(dnum)     yylval.dval = dnum

  int yycolno = 1;
  extern "C" int yylex();

  extern char filename[100];

  void smart_realloc(char** str, int len);
  double smart_strtof(char* str, char** dummy);
  long smart_strtol(char* str, char** dummy, int base);
%}

%option noyywrap
%option never-interactive
%option yylineno

%x ESCAPE
%x LONG_STRING
%x MULTICOMMENT
%x SINGLECOMMENT
%x SHEBANG
%x SHORT_STRING

DIGIT                       [0-9]
DIGITS                      {DIGIT}+
INT                         {DIGITS}({EXP}|{POW})?
DOUBLE                      ({DIGITS}\.{DIGITS}|\.{DIGITS}|{DIGITS}\.)({EXP}|{POW})?
EXP                         [eE](\+|\-)?({DIGITS})?
POW                         [pP](\+|\-)?({DIGITS})?

HEXDIGIT                    [0-9a-fA-F]
HEXDIGITS                   {HEXDIGIT}+
HEXINT                      0[xX]{HEXDIGITS}({HEXPOW})?
HEXDOUBLE                   0[xX]({HEXDIGITS}?\.{HEXDIGITS}{HEXPOW}?|{HEXDIGITS}\.)
HEXPOW                      [pP](\+|\-)?({DIGITS})?

XINT                        {DIGIT}{1,3}
XHEX                        x{HEXDIGIT}{2}
UHEX                        u\{{HEXDIGITS}\}

INT_NUMBER                  {INT}|{HEXINT}
DOUBLE_NUMBER               {DOUBLE}|{HEXDOUBLE}
STRING_LITERAL              {SHORT_STRING}|{LONG_STRING}

%%

%{
  static char str[1001];
  str[0] = 0;

  char quote_symbol[2];
  unsigned int startline = 0;
  unsigned int startcol = 0;
  unsigned int endline = 0;
  unsigned int endcol = 0;

  unsigned int token_count = 0;
  int str_len=0;
%}

                            /* NUMBERS */

{INT}                       {
                              yycolno += yyleng;
                              double d_number = smart_strtof(yytext, 0);
                              if (std::abs(roundf(d_number) - d_number) < EPS) {
                                long i_number = round(d_number);
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %ld\n", yylineno, yycolno, "constant (integer)", i_number);
                                SAVE_INT(i_number);
                                return INT_NUMBER;
                              } else {
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %lf\n", yylineno, yycolno, "constant (floating-point)", d_number);
                                SAVE_DOUBLE(d_number);
                                return DOUBLE_NUMBER;
                              }
                            }
{HEXINT}                    {
                              yycolno += yyleng;
                              double d_number = smart_strtof(yytext, 0);
                              if (std::abs(roundf(d_number) - d_number) < EPS) {
                                long i_number = round(d_number);
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %ld\n", yylineno, yycolno, "constant (integer)", i_number);
                                SAVE_INT(i_number);
                                return INT_NUMBER;
                              } else {
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %lf\n", yylineno, yycolno, "constant (floating-point)", d_number);
                                SAVE_DOUBLE(d_number);
                                return DOUBLE_NUMBER;
                              }
                            }
{DOUBLE}                    {
                              yycolno += yyleng;
                              double d_number = smart_strtof(yytext, 0);
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %lf\n", yylineno, yycolno, "constant (floating-point)", d_number);
                              SAVE_DOUBLE(d_number);
                              return DOUBLE_NUMBER;
                            }
{HEXDOUBLE}                 {
                              yycolno += yyleng;
                              double d_number = smart_strtof(yytext, 0);
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %lf\n", yylineno, yycolno, "constant (floating-point)", d_number);
                              SAVE_DOUBLE(d_number);
                              return DOUBLE_NUMBER;
                            }

                            /* STRINGS */

\"|\'                       {
                              BEGIN(SHORT_STRING);
                              startcol = yycolno;
                              yycolno += yyleng;
                              strcpy(quote_symbol, yytext);
                              strcpy(str,"");
                            }
<SHORT_STRING>"\\"          {
                              BEGIN(ESCAPE);
                              yycolno += yyleng;
                            }
<ESCAPE>"a"                 {
                              strcat(str, "\a");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\a");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"b"                 {
                              strcat(str, "\b");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\b");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"f"                 {
                              strcat(str, "\f");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\f");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"n"                 {
                              strcat(str, "\n");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\n");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"r"                 {
                              strcat(str, "\r");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\r");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"t"                 {
                              strcat(str, "\t");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\t");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"v"                 {
                              strcat(str, "\v");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\v");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>z[ \f\r\t\v]*       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\z");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>z\n*                {
                              yycolno = 1;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>"\\"                {
                              strcat(str, "\\");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\\\");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>\"                  {
                              strcat(str, "\"");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\\"");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>\'                  {
                              strcat(str, "\'");
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "escaping sequence", "\\\'");
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>{UHEX}              {
                              strcat(str, yytext);
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: \\%s\n", yylineno, yycolno, "escaping sequence", yytext);
                              yycolno += yyleng;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>{XHEX}              {
                              yycolno += yyleng;
                              char buf[5] = "";
                              strcpy(buf + 1, yytext);
                              buf[0] = '0';
                              char ch = strtol(buf, 0, 16);
                              int sLen = strlen(str);
                              str[sLen] = ch;
                              str[sLen+1] = 0;
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: \\%s (%c)\n", yylineno, yycolno, "escaping sequence", yytext, ch);
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>{XINT}              {
                              yycolno += yyleng;
                              if (atoi(yytext) > 255) {
                                fprintf(stderr, "[%s:%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", filename, yylineno, yycolno, "escaping sequence", "Wrong value > 255");
                              } else {
                                str_len=strlen(str);
                                str[str_len] = atoi(yytext);
                                str[str_len + 1] = 0;
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: \\%s (%c)\n", yylineno, yycolno, "escaping sequence", yytext, atoi(yytext));
                              }
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>\n                  {
                              yycolno = 1;
                              BEGIN(SHORT_STRING);
                            }
<ESCAPE>.                   {
                              fprintf(stderr, "[%s:%d]: invalid escape sequence near %s\n", filename, yylineno, str);
                              return 1;
                            }
<SHORT_STRING>\"|\'         {
                              yycolno += yyleng;
                              if (strcmp(yytext, quote_symbol) == 0) {
                                endcol = yycolno;
                                //fprintf(stdout, "[%d:%d-%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, startcol, endcol, "constant (short string)", str);
                                BEGIN(INITIAL);
                                SAVE_STRING(str);
                                strcpy(str, "");
                                return STRING_LITERAL;
                              } else {
                                strcat(str, yytext);
                              }
                            }
<SHORT_STRING>[^\n\"\'\\]+  {
                              strcat(str, yytext);
                              yycolno += yyleng;
                            }
<SHORT_STRING>\n            {
                              fprintf(stderr, "[%s:%d]: unfinished string near %s\n", filename, yylineno, str);
                              return 1;
                            }
<SHORT_STRING><<EOF>>       {
                              fprintf(stderr, "[%s:%d]: unfinished string near %s\n", filename, yylineno, str);
                              return 1;
                            }

\[=*\[                      {
                              BEGIN(LONG_STRING);
                              strcpy(str, "");
                              startline = yylineno;
                              startcol = yycolno;
                              token_count = 0;
                              for (int i = 0; yytext[i]; token_count += (yytext[i] == '='), ++i);
                            }
<LONG_STRING>\]=*\]         {
                              unsigned int token_count_end = 0;
                              for (int i = 0; yytext[i]; token_count_end += (yytext[i] == '='), ++i);
                              if (token_count == token_count_end) {
                                endline = yylineno;
                                endcol = yycolno;
                                //fprintf(stdout, "[%d:%d-%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", startline, startcol, endline, endcol, "constant (long string)", str);
                                SAVE_STRING(str);
                                strcpy(str, "");
                                BEGIN(INITIAL);
                                return STRING_LITERAL;
                              } else {
                                strcat(str, yytext);
                              }
                            }
<LONG_STRING>[^\n\]]+       {
                              strcat(str, yytext);
                              yycolno += yyleng;
                            }
<LONG_STRING>]              {
                              strcat(str, yytext);
                              yycolno += yyleng;
                            }

<LONG_STRING>\n             {
                              strcat(str, yytext);
                              yycolno = 1;
                            }
<LONG_STRING><<EOF>>        {
                              fprintf(stderr, "[%s:%d] unfinished long string (starting at line %d) near <eof>\n", filename, yylineno, startline);
                              BEGIN(INITIAL);
                            }

                            /* COMMENTS */

"--"                        {
                              BEGIN(SINGLECOMMENT);
                              startcol = yycolno;
                              yycolno += yyleng;
                            }
<SINGLECOMMENT>\n           {
                              BEGIN(INITIAL);
                              endcol = yycolno;
                              yycolno = 1;
                              //fprintf(stdout, "[%d:%d-%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, startcol, endcol, "single-line comment", "<IGNORED>");
                            }
<SINGLECOMMENT>[^\n]+       { yycolno += yyleng; }

\-\-\[=*\[                  {
                              BEGIN(MULTICOMMENT);
                              token_count = 0;
                              for (int i = 0; yytext[i]; token_count += (yytext[i] == '='), ++i);
                              startline = yylineno;
                              startcol = yycolno;
                            }
<MULTICOMMENT>\]=*\]        {
                              unsigned int token_count_end = 0;
                              for (int i = 0; yytext[i]; token_count_end += (yytext[i] == '='), ++i);
                              if (token_count_end == token_count) {
                                BEGIN(INITIAL);
                                endline = yylineno;
                                endcol = yycolno;
                                //fprintf(stdout, "[%d:%d-%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", startline, startcol, endline, endcol, "multi-line comment", "<IGNORED>");
                              } else { yycolno += yyleng; }
                            }
<MULTICOMMENT>[^\n\]]+      { yycolno += yyleng; }
<MULTICOMMENT>\n            { yycolno = 1; }
<MULTICOMMENT>\]            { yycolno++; }
<MULTICOMMENT><<EOF>>       {
                              fprintf(stderr, "%s: unfinished long comment (starting at line %d) near <eof>\n", filename, yylineno);
                              BEGIN(INITIAL);
                            }

                            /* KEYWORDS */

"and"                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return AND;
                            }
"break"                     {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return BREAK;
                            }
"function"                  {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return FUNCTION;
                            }
"do"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return DO;
                            }
"else"                      {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return ELSE;
                            }
"elseif"                    {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return ELSEIF;
                            }
"end"                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return END;
                            }
"false"                     {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return FALSE;
                            }
"for"                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return FOR;
                            }
"goto"                      {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return GOTO;
                            }
"if"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return IF;
                            }
"in"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return IN;
                            }
"local"                     {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return LOCAL;
                            }
"nil"                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return NIL;
                            }
"not"                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return NOT;
                            }
"or"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return OR;
                            }
"repeat"                    {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return REPEAT;
                            }
"return"                    {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return RETURN;
                            }
"then"                      {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return THEN;
                            }
"true"                      {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return TRUE;
                            }
"until"                     {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return UNTIL;
                            }
"while"                     {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "keyword", yytext);
                              yycolno += yyleng;
                              return WHILE;
                            }

                            /* OPERATORS */

"+"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'+';
                            }
"-"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'-';
                            }
"*"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'*';
                            }
"/"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'/';
                            }
"%"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'%';
                            }
"^"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'^';
                            }
"//"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DSLASH;
                            }
"#"                         {
                              if (yylineno == 1 && yycolno == 1) {
                                BEGIN(SHEBANG);
                              } else {
                                //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                                yycolno += yyleng;
                                return (unsigned char)'#';
                              }
                            }
<SHEBANG>.+                 {
                              yycolno = 1;
                              BEGIN(INITIAL);
                            }
"&"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'&';
                            }
"~"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'~';
                            }
"|"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'|';
                            }
"<<"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DLCHEVRON;
                            }
">>"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DRCHEVRON;
                            }
"="                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'=';
                            }
"=="                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DEQ;
                            }
"~="                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return TILDEEQ;
                            }
"<"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'<';
                            }
">"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'>';
                            }
"<="                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return LCHEVRONEQ;
                            }
">="                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return RCHEVRONEQ;
                            }
"("                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'(';
                            }
")"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)')';
                            }
"{"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'{';
                            }
"}"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'}';
                            }
"["                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'[';
                            }
"]"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)']';
                            }
":"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)':';
                            }
"::"                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DCOLON;
                            }
";"                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)';';
                            }
","                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)',';
                            }
"."                         {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return (unsigned char)'.';
                            }
".."                        {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return DDOT;
                            }
"..."                       {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "operator", yytext);
                              yycolno += yyleng;
                              return TDOT;
                            }


                            /* OTHER */

[a-zA-Z_][a-zA-Z0-9_]*      {
                              //fprintf(stdout, "[%d:%d]\tTYPE: <%s>:\tVALUE: %s\n", yylineno, yycolno, "id", yytext);
                              yycolno += yyleng;
                              SAVE_STRING(yytext);
                              return ID;
                            }
\n                          { yycolno = 1; }
\t                          { yycolno += 4; }
" "                         { yycolno++; }
[ \a\b\f\n\r\t\v]           ;
.                           {
                              fprintf(stderr, "[%s:%d:%d]\tUNIDENTIFIED SYMBOL <%s>\n", filename, yylineno, yycolno, yytext);
                              yycolno += yyleng;
                            }

%%

void smart_realloc(char** str, int len) {
  char *tmp = (char*)malloc(strlen(*str) + 1);
  strcpy(tmp, *str);
  *str = (char*)realloc(*str, strlen(*str) + 1 + len);
  strcpy(*str, tmp);
}

double smart_strtof(char* str, char** dummy) {
  if (strchr(str, 'p') == 0 && strchr(str, 'P') == 0) {
    return strtof(str, dummy);
  }
  char* powc = strchr(str, 'p');
  if (powc == 0) {
    powc = strchr(str, 'P');
  }
  powc[0] = 0;
  powc++;
  return strtof(str, dummy) * pow(2, strtol(powc, dummy, 10));
}

long smart_strtol(char* str, char** dummy, int base) {
  if (strchr(str, 'p') == 0 && strchr(str,'P') == 0) {
    return strtol(str, dummy, base);
  }
  char* powc = strchr(str,'p');
  if (powc == 0) {
    powc = strchr(str, 'P');
  }
  powc[0] = 0;
  powc++;
  return strtol(str, dummy, base) * pow(2, strtol(powc, dummy, 10));
}
