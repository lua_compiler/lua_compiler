TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    build-dot.cpp \
    lex.yy.cpp \
    parser.tab.cpp \
    build-html.cpp \
    codegen.cpp \
    build-tables.cpp

HEADERS += \
    parser.y \
    ast.h \
    node-types.h \
    lexer.l \
    parser.tab.hpp \
    codegen.h \
    commands.h

DISTFILES += \
    tests/*.lua \
    .gitignore
