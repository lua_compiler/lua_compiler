#include <cstring>

#include "ast.h"
#include "codegen.h"
#include "parser.tab.hpp"

extern int yydebug;

extern FILE *yyin;
extern int yyparse();

extern Block *root;
extern VarTable *superGlobalVarTable;
char filename[100];

struct options {
  bool isHtml = false;
  bool isDot = false;
  bool isDebug = false;
  char filename[100];
};

struct options params;

void parseOptions(int argc, char **argv,
                  struct options &opts) {  // TODO improve
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "--html") == 0) {
      opts.isHtml = true;
    } else if (strcmp(argv[i], "--dot") == 0) {
      opts.isDot = true;
    } else if (strcmp(argv[i], "--debug") == 0) {
      opts.isDebug = true;
    } else {
      strcpy(opts.filename, argv[i]);
    }
  }
}

void buildDot(Block *root) {
  unsigned int id = 0;
  printf("digraph {");
  root->buildDot(&id);
  printf("}");
}

void buildHtml(Block *root) {
  printf("<head>\n");
  printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\">\n");
  printf("</head>\n");
  printf("<body>\n");
  printf("<h3> Глобальная область видимости: </h3>\n");
  printf("<h4>Таблица констант:</h4>\n");
  buildHtml(root->localConstTable);
  root->buildHtml();
  printf("</body>\n");
}

void initializeMainTable(ConstTable *table) {
  table->push_back(
      new ConstTableEntry(constTableEntryType::dummy, new std::string("")));
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string(MAIN_CLASS_NAME)));
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string(MAIN_CLASS_NAME)));
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("java/lang/Object")));
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string("java/lang/Object")));
  int nameIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("main")));
  int typeIndex = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::utf8, new std::string("([Ljava/lang/String;)V")));
  int nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""),
      findByClassName(*(new std::string(MAIN_CLASS_NAME)), table),
      nameAndTypeIndex, new std::string("main")));

  // RTL
  fillPolyVarMethods(table, generatePolyVarMethods());
  fillPolyVarFields(table, generatePolyVarFields());

  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("Print")));
  int printClassIndex = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::const_class, new std::string(), table->size() - 1,
      -1, new std::string(""), -1, new std::string("Print")));
  // print()
  nameIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("print")));
  typeIndex = table->size();
  table->push_back(new ConstTableEntry(
      constTableEntryType::utf8, new std::string("([LPolyVar;)LPolyVar;")));
  nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""), printClassIndex,
      nameAndTypeIndex, new std::string("print")));
  // read()
  nameIndex = table->size();
  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("read")));
  typeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::utf8,
                                       new std::string("()LPolyVar;")));
  nameAndTypeIndex = table->size();
  table->push_back(new ConstTableEntry(constTableEntryType::nameAndType,
                                       new std::string(""), nameIndex,
                                       typeIndex));
  table->push_back(new ConstTableEntry(
      constTableEntryType::methodref, new std::string(""), printClassIndex,
      nameAndTypeIndex, new std::string("read")));

  table->push_back(
      new ConstTableEntry(constTableEntryType::utf8, new std::string("Code")));
}

void generateCodeForMain(Block *root) {
  std::ofstream out("bin/__mainClass.class", std::ios::binary);
  unsigned long tmp4 = reverse4(0xCAFEBABE);
  out.write(reinterpret_cast<const char *>(&tmp4), 4);  // CAFEBABE
  tmp4 = 872415232;
  out.write(reinterpret_cast<const char *>(&tmp4),
            4);  // SDK version (00 00 00 34)
  unsigned short tmp2 = reverse2(root->localConstTable->size());
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // constTableSize
  // constTable
  for (int i = 1; i < root->localConstTable->size(); i++) {
    root->localConstTable->at(i)->writeToClassFile(out);
  }
  tmp2 = reverse2(3);  // access flags
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(findByClassName(*(new std::string(MAIN_CLASS_NAME)),
                                  root->localConstTable));  // current class
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(findByClassName("java/lang/Object", root->localConstTable));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // parent class(object)
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // interface count

  unsigned int fieldsCount = 0;
  for (int i = 1; i < root->localConstTable->size(); i++) {
    if (root->localConstTable->at(i)->type == constTableEntryType::fieldref &&
        root->localConstTable->at(i)->localField) {
      fieldsCount++;
    }
  }

  tmp2 = reverse2(fieldsCount);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // fields count
  // field table
  for (int i = 1; i < root->localConstTable->size(); i++) {
    if (root->localConstTable->at(i)->type == constTableEntryType::fieldref &&
        root->localConstTable->at(i)->localField) {
      tmp2 = reverse2(0x0001 | 0x0008);  // public static
      out.write(reinterpret_cast<const char *>(&tmp2), 2);
      int nameAndTypeIndex = root->localConstTable->at(i)->intVal2;
      tmp2 = reverse2(root->localConstTable->at(nameAndTypeIndex)->intVal1);
      out.write(reinterpret_cast<const char *>(&tmp2), 2);
      tmp2 = reverse2(root->localConstTable->at(nameAndTypeIndex)->intVal2);
      out.write(reinterpret_cast<const char *>(&tmp2), 2);
      tmp2 = reverse2(0);
      out.write(reinterpret_cast<const char *>(&tmp2), 2);
    }
  }

  tmp2 = reverse2(1);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method count
  tmp2 = reverse2(0x0001 | 0x0008);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // access flags
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("main"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method name
  tmp2 = reverse2(findUtf(root->localConstTable,
                          *(new std::string("([Ljava/lang/String;)V"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method descriptor
  tmp2 = reverse2(1);
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // attribute count
  tmp2 = reverse2(findUtf(root->localConstTable, *(new std::string("Code"))));
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // Code attr

  std::vector<char> *code = new std::vector<char>();  // TODO check unsigned ?
  int len = 0;
  for (int i = 2;
       i < root->localVarTable->size() + RESERVE_LOCAL_VARS_FOR_ASSIGNMENT;
       i++) {
    len += write(code, JBC_NEW);
    len += write(code,
                 reverse2(findByClassName("PolyVar", root->localConstTable)));
    len += write(code, DUP);
    len += write(code, INVOKESPECIAL);
    len +=
        write(code, reverse2(findMethodref("<init>", root->localConstTable)));
    len += write(code, ASTORE);
    len += write(code, (char)(i));
  }
  for (unsigned int i = 0; i < root->localConstTable->size(); i++) {
    if (root->localConstTable->at(i)->type == constTableEntryType::fieldref &&
        root->localConstTable->at(i)->localField) {
      len += write(code, JBC_NEW);
      len += write(code,
                   reverse2(findByClassName("PolyVar", root->localConstTable)));
      len += write(code, DUP);
      len += write(code, INVOKESPECIAL);
      len +=
          write(code, reverse2(findMethodref("<init>", root->localConstTable)));
      len += write(code, PUTSTATIC);
      len += write(code, reverse2(i));
    }
  }

  len += root->generateCode(code);
  len += write(code, JBC_RETURN);

  tmp4 = reverse4(code->size() + 2 + 2 + 4 + 2 + 2);  // Atribute len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  tmp2 = reverse2(MAX_STACK_SIZE);  // Stack size
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp2 = reverse2(root->localVarTable->size() +
                  RESERVE_LOCAL_VARS_FOR_ASSIGNMENT);  // Local vars count + 20
                                                       // reserved for multiple
                                                       // assignment
  out.write(reinterpret_cast<const char *>(&tmp2), 2);
  tmp4 = reverse4(code->size());  // Bytecode len
  out.write(reinterpret_cast<const char *>(&tmp4), 4);
  out.write((const char *)code->data(), code->size());  // Bytecode
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // exception count
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // method attr count
  tmp2 = 0;
  out.write(reinterpret_cast<const char *>(&tmp2), 2);  // class attr count
  out.close();
}

int main(int argc, char **argv) {
  parseOptions(argc, argv, params);

  yyin = (strcmp(params.filename, "") == 0) ? fopen("test.lua", "rb")
                                            : fopen(params.filename, "rb");

  if (yyin) {
    if (params.isDebug) {
      yydebug = 1;
    }

    strcpy(filename, params.filename);

    do {
      yyparse();
    } while (!feof(yyin));

    ConstTable *table = new ConstTable();

    initializeMainTable(table);
    (*superGlobalVarTable)[std::string("self")] = 0;
    (*superGlobalVarTable)[std::string("<reserved for PolyVar array>")] = 1;

    root->buildTables(table, table, superGlobalVarTable);

    generateCodeForMain(root);

    if (params.isDot) {
      buildDot(root);
    }

    if (params.isHtml) {
      buildHtml(root);
    }

  } else {
    fprintf(stderr, "Error: no such file");
    return 1;
  }
}
