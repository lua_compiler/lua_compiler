#ifndef NODE_TYPES_H
#define NODE_TYPES_H

enum class NT_Field { IndexSet, ExpSet, IdSet };

enum class NT_RetStat { Empty, NonEmpty };

enum class NT_Block { Empty, Stats, Return, StatsReturn };

enum class NT_TableConstructor { Empty, Single, List };

enum class NT_Args { Empty, ExpList };

enum class NT_PrefixExp { Var, FuncCall, Exp };

enum class NT_FunctionCall { FuncCall, MethodCall };

enum class NT_Var { Id, TableField };

enum class NT_ElseIf { Single, Several };

enum class NT_IfElse { IfOnly, IfElseif, IfElse, IfElseifElse };

enum class NT_ForLoop { WithStep, WithoutStep };

enum class NT_VarDecl { DeclOnly, DeclDef };

enum class NT_ParList { Usual };

enum class NT_FuncBody { WithParams, WithoutParams };

enum class NT_Stat {
  Empty,
  VarDef,
  FuncCallOnly,
  Label,
  Break,
  Goto,
  Block,
  WhileLoop,
  RepUntilLoop,
  IfElse,
  ForLoop,
  ForEach,
  FuncDecl,
  LocalFuncDecl,
  VarDecl
};

enum class NT_Exp {
  Nil,
  Bool,
  Int,
  Float,
  String,
  Funcdef,
  Prefexp,
  Tconstr,
  Plus,
  Minus,
  Multiply,
  Div,
  Divint,
  Pow,
  Mod,
  Band,
  Bor,
  Xor,
  Shl,
  Shr,
  Concat,
  Lt,
  Lte,
  Gt,
  Gte,
  Eq,
  Neq,
  And,
  Or,
  Uminus,
  Not,
  Len,
  Ubnot
};

#endif  // NODE_TYPES_H
