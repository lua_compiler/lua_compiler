%{
  #include <stdio.h>
  #include <stdlib.h>

  #include "ast.h"

  Block * root;
  extern char filename[100];

  VarTable *superGlobalVarTable = new VarTable();

  #define YYDEBUG 1

  extern "C" int yylineno;
  extern "C" int yycolno;
  extern "C" int yylex();

  void yyerror(const char* error) {
    fprintf(stderr, "[%s:%d:%d]: %s\n", filename, yylineno, yycolno, error);
    exit(-1);
  }
%}

%union {
  int                 ival;
  double              dval;
  std::string*        string;
  Block*              block;
  Stat*               stat;
  Var*                var;
  Exp*                exp;
  StatList*           statList;
  VarList*            varList;
  ExpList*            expList;
  NameList*           nameList;
  RetStat*            retStat;
  FunctionCall*       functionCall;
  IfElse*             ifElse;
  ForLoop*            forLoop;
  FuncBody*           funcBody;
  VarDecl*            varDecl;
  ElseIf*             elseIf;
  PrefixExp*          prefixExp;
  Args*               args;
  FunctionDef*        functionDef;
  ParList*            parList;
  TableConstructor*   tableConstructor;
  Field*              field;
  FieldList*          fieldList;
}

%token  <string>  ID STRING_LITERAL
%token  <ival>    INT_NUMBER
%token  <dval>    DOUBLE_NUMBER

%token            AND BREAK FUNCTION DO ELSE ELSEIF END FALSE FOR GOTO IF
%token            LOCAL NIL NOT OR REPEAT RETURN THEN TRUE UNTIL WHILE IN

%token            DSLASH DLCHEVRON DRCHEVRON DEQ TILDEEQ
%token            LCHEVRONEQ RCHEVRONEQ DCOLON DDOT TDOT

%type   <args>              args
%type   <block>             block chunk
%type   <elseIf>            elseif
%type   <exp>               exp
%type   <expList>           explist
%type   <field>             field
%type   <fieldList>         fieldlist fieldsep_field_list
%type   <forLoop>           forloop
%type   <funcBody>          funcbody
%type   <functionCall>      functioncall
%type   <functionDef>       functiondef
%type   <ifElse>            ifelse
%type   <nameList>          namelist
%type   <parList>           parlist
%type   <prefixExp>         prefixexp
%type   <retStat>           retstat
%type   <stat>              stat
%type   <statList>          statements
%type   <string>            label qualified_name
%type   <tableConstructor>  tableconstructor
%type   <var>               var
%type   <varDecl>           vardecl
%type   <varList>           varlist

%left OR
%left AND
%left '<' '>' LCHEVRONEQ RCHEVRONEQ TILDEEQ DEQ
%left '|'
%left '~'
%left '&'
%left DLCHEVRON DRCHEVRON
%right DDOT
%left '+' '-'
%left '*' '/' DSLASH '%'
%nonassoc NOT UMINUS '#' UTILDE
%right '^'

%start chunk
%debug
%error-verbose

%%

chunk
    : block                                           { root = $1; }
    ;

block
    : %empty                                          { $$ = new Block(NT_Block::Empty); }
    | statements                                      { $$ = new Block($1); }
    | retstat                                         { $$ = new Block($1); }
    | statements retstat                              { $$ = new Block($1, $2); }
    ;

statements
    : stat                                            {
                                                        $$ = new StatList();
                                                        $$->push_back($1);
                                                      }
    | statements stat                                 { $$->push_back($2); }
    ;

stat
    : ';'                                             { $$ = new Stat(); }
    | varlist '=' explist                             { $$ = new Stat($1, $3); }
    | prefixexp                                       {
                                                        switch ($1->type) {
                                                          case NT_PrefixExp::FuncCall:
                                                            $$ = new Stat($1);
                                                            break;
                                                          case NT_PrefixExp::Var:
                                                          case NT_PrefixExp::Exp:
                                                            yyerror("syntax error: function call expected");
                                                            break;
                                                        }
                                                      }
    | label                                           { yyerror("syntax error: labels are not supported"); }
    | BREAK                                           { $$ = new Stat(NT_Stat::Break); }
    | GOTO ID                                         { yyerror("syntax error: gotos are not supported"); }
    | DO block END                                    { $$ = new Stat($2); }
    | WHILE exp DO block END                          { $$ = new Stat($2, $4, NT_Stat::WhileLoop); }
    | REPEAT block UNTIL exp                          { $$ = new Stat($4, $2, NT_Stat::RepUntilLoop); }
    | ifelse                                          { $$ = new Stat($1); }
    | forloop                                         { $$ = new Stat($1); }
    | FOR namelist IN explist DO block END            { yyerror("syntax error: generic for loop is not supported"); }
    | FUNCTION qualified_name funcbody                { $$ = new Stat($2, $3, NT_Stat::FuncDecl); }
    | FUNCTION qualified_name ':' ID funcbody         {
                                                        $5->parlist->namelist->insert(
                                                          $5->parlist->namelist->begin(),
                                                          new std::string("self")
                                                        );
                                                        $2->append("." + *$4);
                                                        $$ = new Stat($2, $5, NT_Stat::FuncDecl);
                                                      }
    | LOCAL FUNCTION ID funcbody                      { $$ = new Stat($3, $4, NT_Stat::LocalFuncDecl); }
    | vardecl                                         { $$ = new Stat($1); }
    ;

vardecl
    : LOCAL namelist                                  { $$ = new VarDecl($2); }
    | LOCAL namelist '=' explist                      { $$ = new VarDecl($2, $4); }
    ;

ifelse
    : IF exp THEN block END                           { $$ = new IfElse($2, $4); }
    | IF exp THEN block elseif END                    { $$ = new IfElse($2, $4, $5); }
    | IF exp THEN block ELSE block END                { $$ = new IfElse($2, $4, $6); }
    | IF exp THEN block elseif ELSE block END         { $$ = new IfElse($2, $4, $5, $7); }
    ;

elseif
    : ELSEIF exp THEN block                           { $$ = new ElseIf($2, $4); }
    | elseif ELSEIF exp THEN block                    { $$ = new ElseIf($1, $3, $5); }
    ;

forloop
    : FOR ID '=' exp ',' exp DO block END             { $$ = new ForLoop($2, $4, $6, new Exp(1), $8); }
    | FOR ID '=' exp ',' exp ',' exp DO block END     { $$ = new ForLoop($2, $4, $6, $8, $10); }
    ;

retstat
    : RETURN                                          { $$ = new RetStat(); }
    | RETURN ';'                                      { $$ = new RetStat(); }
    | RETURN explist                                  { $$ = new RetStat($2); }
    | RETURN explist ';'                              { $$ = new RetStat($2); }
    ;

label
    : DCOLON ID DCOLON                                { $$ = new std::string(*$2); }
    ;

qualified_name
    : ID                                              { $$ = $1; }
    | qualified_name '.' ID                           { $1->append("." + *$3); }
    ;

varlist
    : var                                             {
                                                        $$ = new VarList();
                                                        $$->push_back($1);
                                                      }
    | varlist ',' var                                 { $1->push_back($3); }
    ;

var
    : ID                                              { $$ = new Var($1); }
    | prefixexp '[' exp ']'                           { $$ = new Var($1, $3); }
    | prefixexp '.' ID                                { $$ = new Var($1, new Exp($3)); }
    ;

namelist
    : ID                                              {
                                                        $$ = new NameList();
                                                        $$->push_back($1);
                                                      }
    | namelist ',' ID                                 { $1->push_back($3); }
    ;

explist
    : exp                                             {
                                                        $$ = new ExpList();
                                                        $$->push_back($1);
                                                      }
    | explist ',' exp                                 { $1->push_back($3); }
    ;

exp
    : NIL                                             { $$ = new Exp(); }
    | FALSE                                           { $$ = new Exp(false); }
    | TRUE                                            { $$ = new Exp(true); }
    | INT_NUMBER                                      { $$ = new Exp($1); }
    | DOUBLE_NUMBER                                   { $$ = new Exp($1); }
    | STRING_LITERAL                                  { $$ = new Exp($1); }
    | TDOT                                            { yyerror("syntax error: variadics are not supported"); }
    | functiondef                                     { $$ = new Exp($1); }
    | prefixexp                                       { $$ = new Exp($1); }
    | tableconstructor                                { $$ = new Exp($1); }
    | exp '+' exp                                     { $$ = new Exp(NT_Exp::Plus ,     $1, $3); }
    | exp '-' exp                                     { $$ = new Exp(NT_Exp::Minus ,    $1, $3); }
    | exp '*' exp                                     { $$ = new Exp(NT_Exp::Multiply , $1, $3); }
    | exp '/' exp                                     { $$ = new Exp(NT_Exp::Div ,      $1, $3); }
    | exp DSLASH exp                                  { $$ = new Exp(NT_Exp::Divint ,   $1, $3); }
    | exp '^' exp                                     { $$ = new Exp(NT_Exp::Pow ,      $1, $3); }
    | exp '%' exp                                     { $$ = new Exp(NT_Exp::Mod ,      $1, $3); }
    | exp '&' exp                                     { $$ = new Exp(NT_Exp::Band ,     $1, $3); }
    | exp '~' exp                                     { $$ = new Exp(NT_Exp::Xor ,      $1, $3); }
    | exp '|' exp                                     { $$ = new Exp(NT_Exp::Bor ,      $1, $3); }
    | exp DRCHEVRON exp                               { $$ = new Exp(NT_Exp::Shr ,      $1, $3); }
    | exp DLCHEVRON exp                               { $$ = new Exp(NT_Exp::Shl ,      $1, $3); }
    | exp DDOT exp                                    { $$ = new Exp(NT_Exp::Concat ,   $1, $3); }
    | exp '<' exp                                     { $$ = new Exp(NT_Exp::Lt ,       $1, $3); }
    | exp LCHEVRONEQ exp                              { $$ = new Exp(NT_Exp::Lte ,      $1, $3); }
    | exp '>' exp                                     { $$ = new Exp(NT_Exp::Gt ,       $1, $3); }
    | exp RCHEVRONEQ exp                              { $$ = new Exp(NT_Exp::Gte ,      $1, $3); }
    | exp DEQ exp                                     { $$ = new Exp(NT_Exp::Eq ,       $1, $3); }
    | exp TILDEEQ exp                                 { $$ = new Exp(NT_Exp::Neq ,      $1, $3); }
    | exp AND exp                                     { $$ = new Exp(NT_Exp::And ,      $1, $3); }
    | exp OR exp                                      { $$ = new Exp(NT_Exp::Or ,       $1, $3); }
    | '-' exp %prec UMINUS                            { $$ = new Exp(NT_Exp::Uminus ,       $2); }
    | NOT exp                                         { $$ = new Exp(NT_Exp::Not ,          $2); }
    | '#' exp                                         { $$ = new Exp(NT_Exp::Len ,          $2); }
    | '~' exp %prec UTILDE                            { $$ = new Exp(NT_Exp::Ubnot ,        $2); }
    ;

prefixexp
    : var                                             { $$ = new PrefixExp($1); }
    | functioncall                                    { $$ = new PrefixExp($1); }
    | '(' exp ')'                                     { $$ = new PrefixExp($2); }
    ;

functioncall
    : prefixexp args                                  { $$ = new FunctionCall($1, $2); }
    | prefixexp ':' ID args                           { $$ = new FunctionCall($1, $3, $4); }
    ;

args
    : '(' ')'                                         { $$ = new Args(); }
    | '(' explist ')'                                 { $$ = new Args($2); }
    | tableconstructor                                { $$ = new Args(new ExpList(1, new Exp($1))); }
    | STRING_LITERAL                                  { $$ = new Args(new ExpList(1, new Exp($1))); }
    ;

functiondef
    : FUNCTION funcbody                               { $$ = new FunctionDef($2); }
    ;

funcbody
    : '(' ')' block END                               { $$ = new FuncBody($3); }
    | '(' parlist ')' block END                       { $$ = new FuncBody($2, $4); }
    ;

parlist
    : TDOT                                            { yyerror("syntax error: variadic params are not supported"); }
    | namelist                                        { $$ = new ParList($1); }
    | namelist ',' TDOT                               { yyerror("syntax error: variadic params are not supported"); }
    ;

tableconstructor
    : '{' '}'                                         { $$ = new TableConstructor(); }
    | '{' field '}'                                   { $$ = new TableConstructor($2); }
    | '{' fieldlist '}'                               { $$ = new TableConstructor($2); }
    ;

fieldlist
    : field fieldsep_field_list                       {
                                                        $$ = new FieldList();
                                                        $$->push_back($1);
                                                        $$->insert(std::end(*$$), std::begin(*$2), std::end(*$2));
                                                      }
    | field fieldsep_field_list fieldsep              {
                                                        $$ = new FieldList();
                                                        $$->push_back($1);
                                                        $$->insert(std::end(*$$), std::begin(*$2), std::end(*$2));
                                                      }
    ;

fieldsep_field_list
    : fieldsep field                                  {
                                                        $$ = new FieldList();
                                                        $$->push_back($2);
                                                      }
    | fieldsep_field_list fieldsep field              { $1->push_back($3); }
    ;

field
    : '[' exp ']' '=' exp                             { $$ = new Field($2, $5); }
    | ID '=' exp                                      { $$ = new Field($1, $3); }
    | exp                                             { $$ = new Field($1); }
    ;

fieldsep
    : ','
    | ';'
    ;

%%
