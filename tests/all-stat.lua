a,b,c,d = {
  0,
  (function() return 1 end)(),
  1,
  [30] = 777,
  "keke"
}, 1, print("sas"), 1234

print(a, b, c, d)

do
  local i = 0
  while i > 10 do
    print("while_sas#"..i)
  end

  repeat
    print("ru_sas#"..i)
  until i < 10
end

--print("local i = "..i)

print("Enter number 0..100:")
n = read()
if n > 50 then
  print("number is greater than 50")
elseif n < 50 then
  print("number is less than 50")
else
  print("number is equal to 50")
end
print("The End")

+, -, *, /, %, ^, #, &, ~, |, <<, >>, //, ==,~=,<=,>=,<, >, =, (), {}, [], ;, :, ,, ., ..
