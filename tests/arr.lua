function findMin(arr)
    local min=arr[#arr]
    for i=1,#arr+1 do
        if arr[i]<min then
            min=arr[i]
        end
    end
    return min
end

function findMax(arr)
    local max=arr[#arr]
    for i=1,#arr+1 do
        if arr[i]>max then
            max=arr[i]
        end
    end
    return max
end

function getCol(m,j)
local arr={}
    for i=1,#m+1 do
        arr[i]=m[i][j]
    end
return arr
end

m={{999,999,900,999,999},{1,2,3,4,5},{0,0,0,0,0},{1,2,3,4,5},{1,2,3,4,5}}

for i=1,#m+1 do
    for j=1, #m[i]+1 do
        if m[i][j]==findMin(m[i]) and m[i][j]==findMax(getCol(m,j)) then
            print(m[i][1],m[i][2],m[i][3],m[i][4],m[i][5])
            print(m[i][j])
        end
    end
end
