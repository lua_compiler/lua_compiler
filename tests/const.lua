a=3
print(a)
a=345
print(a)
a=0xff
print(a)
a=0xBEBADA
print(a)
a=3.0
print(a)
a=3.1416
print(a)
a=314.16e-2
print(a)
a=0.31416E1
print(a)
a=34e1
print(a)
a=0x0.1E
print(a)
a=0xA23p-4
print(a)
a=0X1.921FB54442D18P+1
print(a)


a= 'single quote'
print(a)
a = "double quote"
print(a)
a = 'escape sequences\a\b\f\n\r\t\v\\\"\''
print(a)
a = [==[
mult]i]]
	line]=]
		st]===]ring
constant]==]
print(a)
a = true and false or nil
print(a)
a = 'ab\067\x43c\\d'
print(a)
