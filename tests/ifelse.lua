-- 0.

if true then
  print("123")
end
print("12345")

cond = true

-- 1. Test empty if block with only one if
if cond then
-- empty
end
print("after_1_1")

if not cond then
-- empty
end
print("after_1_2")

-- 2. Test empty if block with non-empty else
if cond then
  -- empty
else
  print("in_else_2_1")
end

if not cond then
  -- empty
else
  print("in_else_2_2")
end

-- 3. Test empty if block with one non-empty elseif
if not cond then
  -- empty
elseif cond then
  print("in_elseif_3_1")
end

if cond then
  -- empty
elseif not cond then
  print("in_elseif_3_2")
end

if not cond then
  -- empty
elseif not cond then
  print("in_elseif_3_3")
end

if cond then
  -- empty
elseif cond then
  print("in_elseif_3_4")
end

-- 4.Test empty if block with one empty elseif
if not cond then
  -- empty
elseif cond then
  -- empty
end
print("after_4_1")

if cond then
  -- empty
elseif not cond then
  -- empty
end
print("after_4_2")

if not cond then
  -- empty
elseif not cond then
  -- empty
end
print("after_4_3")

if cond then
  -- empty
elseif cond then
  -- empty
end
print("after_4_4")

-- 5. Test empty if block with several empty elseifs
if not cond then
  -- empty
elseif cond == false then
  -- empty
elseif cond then
  -- empty
end
print("after_5")
