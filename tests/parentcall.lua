function parent()
  table = {}
  table.f1 = function()
    print("parent#1")
  end
  table.f2 = function()
    print("parent#2")
  end
  return table
end

function child1()
  tab = {}
  tab.parent = parent()
  tab.f1 = function()
    tab.parent.f1()
    print("child1")
    tab.parent.f2()
  end
  return tab
end

function child2()
  tab = {}
  tab.parent = parent()
  tab.f1 = function()
    tab.parent.f1()
    print("child2")
    tab.parent.f2()
  end
  return tab
end

childs = { child1(), child2(), parent()}

for i=1, #childs+1 do
  childs[i].f1()
end
