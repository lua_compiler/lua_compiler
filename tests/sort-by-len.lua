arr = {
  {true, (function() return 5 end), {}},
  {1e4, 2+7, nil, nil},
  {0, "siofhj"},
  {1},
  {},
}

for i=1, #arr+1 do
  print(arr[i][1], arr[i][2], arr[i][3], arr[i][4])
end

for i=1, #arr do
  local i1 = i
  for j=i1, #arr+1 do
    if #arr[i] > #arr[j] then
      arr[i], arr[j] = arr[j], arr[i]
    end
  end
end

print("")

for i=1, #arr+1 do
  print(arr[i][1], arr[i][2], arr[i][3], arr[i][4])
end
