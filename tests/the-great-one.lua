function fact(n)
  if n == 1 then
    return 1
  else
    return n * fact(n-1)
  end
end

function fib(n)
  if n == 0 or n == 1 then
    return n
  else
    return fib(n-1) + fib(n-2)
  end
end

function sq (n)
  return n * n
end

print("Enter number 0..10:")
num = read()

if num < 5 then
  a = fact
elseif num > 5 then
  a = fib
else
  a = sq
end

print(a(num))
